﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Util;

using Wikitude.Architect;
using Wikitude.Tools.Device.Features;
using Wikitude.Common.Util;
using Wikitude.Common.Camera;
using Android.Content.PM;
using Org.Json;

namespace Com.Wikitude.Samples
{
	[Activity (Label = "WikitudeActivity", ConfigurationChanges = ConfigChanges.Orientation | ConfigChanges.ScreenSize | ConfigChanges.KeyboardHidden)]		
	public class WikitudeActivity : Activity, IArchitectJavaScriptInterfaceListener
	{
		protected ArchitectView architectView;

		private const string SAMPLE_WORLD_URL = "http://test.netwintec.com/donzella/index.html";

		private const string TITLE = "Image on Target";

		protected override void OnCreate (Bundle bundle)
		{
			base.OnCreate (bundle);

			SetContentView(Resource.Layout.sample_cam);

			Title = TITLE;

			architectView = FindViewById<ArchitectView>(Resource.Id.architectView);
			ArchitectStartupConfiguration startupConfiguration = new ArchitectStartupConfiguration();
			startupConfiguration.setLicenseKey(Constants.WIKITUDE_SDK_KEY);
			startupConfiguration.setFeatures(ArchitectStartupConfiguration.Features.ImageTracking);
			startupConfiguration.setCameraResolution(CameraSettings.CameraResolution.Auto);

			/* use  
			   int requiredFeatures = StartupConfiguration.Features.Tracking2D | StartupConfiguration.Features.Geo;
			   if you need both 2d Tracking and Geo
			*/
			int requiredFeatures = ArchitectStartupConfiguration.Features.ImageTracking;
			MissingDeviceFeatures missingDeviceFeatures = ArchitectView.isDeviceSupported(this, requiredFeatures);
			

			if ((ArchitectView.getSupportedFeaturesForDevice (Android.App.Application.Context) & requiredFeatures) == requiredFeatures) {
				architectView.OnCreate (startupConfiguration);
				architectView.AddArchitectJavaScriptInterfaceListener(this);
			} else {
				architectView = null;
				Toast.MakeText (this, missingDeviceFeatures.getMissingFeatureMessage(), ToastLength.Long).Show();

				StartActivity (typeof(ErrorActivity));
			}	
		}

		public override bool OnPrepareOptionsMenu(IMenu menu)
		{
			MenuInflater.Inflate(Resource.Menu.menu_main, menu);
			return base.OnPrepareOptionsMenu(menu);
		}

		public override bool OnOptionsItemSelected(IMenuItem item)
		{
			switch (item.ItemId)
			{
				case Resource.Id.build_information_button:
					ISDKBuildInformation sdkBuildInformation = ArchitectView.SDKBuildInformation;
					new AlertDialog.Builder(this).SetTitle("SDK Build information")
						.SetMessage(
								"Build type:    " + sdkBuildInformation.BuildConfiguration + "\n" +
								"Build date:    " + sdkBuildInformation.BuildDate + "\n" +
								"Build number:  " + sdkBuildInformation.BuildNumber + "\n" +
								"Build version: " + ArchitectView.SDKVersion
						)
						.Show();
					return true;
			}
			return base.OnOptionsItemSelected(item);
		}

		protected override void OnResume ()
		{
			base.OnResume ();

			if (architectView != null)
				architectView.OnResume ();
		}

		protected override void OnPause ()
		{
			base.OnPause ();

			if (architectView != null)
				architectView.OnPause ();
		}

		protected override void OnStop ()
		{
			base.OnStop ();
		}

		protected override void OnDestroy ()
		{
			base.OnDestroy ();

			if (architectView != null)
			{
				architectView.OnDestroy ();
			}
		}

		public override void OnLowMemory ()
		{
			base.OnLowMemory ();

			if (architectView != null)
				architectView.OnLowMemory ();
		}

		protected override void OnPostCreate (Bundle savedInstanceState)
		{
			base.OnPostCreate (savedInstanceState);

			if (architectView != null) {
				architectView.OnPostCreate ();

				try {
					architectView.Load (SAMPLE_WORLD_URL);
				} catch (Exception ex) {
					Log.Error ("WIKITUDE_SAMPLE", ex.ToString ());
				}
			}
		}

		public void OnJSONObjectReceived(JSONObject jSONObject)
		{
			/* This is a example implementation of the OnJSONObjectReceived method */
			/* This is called whenever "AR.platform.sendJSONObject()" is used in JS */
			Console.WriteLine("architect view jsonObject received: " + jSONObject.ToString());
		}
	}
}

