package md5cf70acd2110bc5116a4cd18f670fdcde;


public class ErrorActivity
	extends android.app.Activity
	implements
		mono.android.IGCUserPeer
{
/** @hide */
	public static final String __md_methods;
	static {
		__md_methods = 
			"n_onCreate:(Landroid/os/Bundle;)V:GetOnCreate_Landroid_os_Bundle_Handler\n" +
			"";
		mono.android.Runtime.register ("Com.Wikitude.Samples.ErrorActivity, Example, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null", ErrorActivity.class, __md_methods);
	}


	public ErrorActivity ()
	{
		super ();
		if (getClass () == ErrorActivity.class)
			mono.android.TypeManager.Activate ("Com.Wikitude.Samples.ErrorActivity, Example, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null", "", this, new java.lang.Object[] {  });
	}


	public void onCreate (android.os.Bundle p0)
	{
		n_onCreate (p0);
	}

	private native void n_onCreate (android.os.Bundle p0);

	private java.util.ArrayList refList;
	public void monodroidAddReference (java.lang.Object obj)
	{
		if (refList == null)
			refList = new java.util.ArrayList ();
		refList.add (obj);
	}

	public void monodroidClearReferences ()
	{
		if (refList != null)
			refList.clear ();
	}
}
