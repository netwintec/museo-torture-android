package md5cf70acd2110bc5116a4cd18f670fdcde;


public class MainActivity
	extends android.app.Activity
	implements
		mono.android.IGCUserPeer,
		com.wikitude.common.permission.PermissionManager.PermissionManagerCallback
{
/** @hide */
	public static final String __md_methods;
	static {
		__md_methods = 
			"n_onCreate:(Landroid/os/Bundle;)V:GetOnCreate_Landroid_os_Bundle_Handler\n" +
			"n_onRequestPermissionsResult:(I[Ljava/lang/String;[I)V:GetOnRequestPermissionsResult_IarrayLjava_lang_String_arrayIHandler\n" +
			"n_permissionsDenied:([Ljava/lang/String;)V:GetPermissionsDenied_arrayLjava_lang_String_Handler:Wikitude.Common.Permission.IPermissionManagerPermissionManagerCallbackInvoker, Wikitude.SDK\n" +
			"n_permissionsGranted:(I)V:GetPermissionsGranted_IHandler:Wikitude.Common.Permission.IPermissionManagerPermissionManagerCallbackInvoker, Wikitude.SDK\n" +
			"n_showPermissionRationale:(I[Ljava/lang/String;)V:GetShowPermissionRationale_IarrayLjava_lang_String_Handler:Wikitude.Common.Permission.IPermissionManagerPermissionManagerCallbackInvoker, Wikitude.SDK\n" +
			"";
		mono.android.Runtime.register ("Com.Wikitude.Samples.MainActivity, Example, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null", MainActivity.class, __md_methods);
	}


	public MainActivity ()
	{
		super ();
		if (getClass () == MainActivity.class)
			mono.android.TypeManager.Activate ("Com.Wikitude.Samples.MainActivity, Example, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null", "", this, new java.lang.Object[] {  });
	}


	public void onCreate (android.os.Bundle p0)
	{
		n_onCreate (p0);
	}

	private native void n_onCreate (android.os.Bundle p0);


	public void onRequestPermissionsResult (int p0, java.lang.String[] p1, int[] p2)
	{
		n_onRequestPermissionsResult (p0, p1, p2);
	}

	private native void n_onRequestPermissionsResult (int p0, java.lang.String[] p1, int[] p2);


	public void permissionsDenied (java.lang.String[] p0)
	{
		n_permissionsDenied (p0);
	}

	private native void n_permissionsDenied (java.lang.String[] p0);


	public void permissionsGranted (int p0)
	{
		n_permissionsGranted (p0);
	}

	private native void n_permissionsGranted (int p0);


	public void showPermissionRationale (int p0, java.lang.String[] p1)
	{
		n_showPermissionRationale (p0, p1);
	}

	private native void n_showPermissionRationale (int p0, java.lang.String[] p1);

	private java.util.ArrayList refList;
	public void monodroidAddReference (java.lang.Object obj)
	{
		if (refList == null)
			refList = new java.util.ArrayList ();
		refList.add (obj);
	}

	public void monodroidClearReferences ()
	{
		if (refList != null)
			refList.clear ();
	}
}
