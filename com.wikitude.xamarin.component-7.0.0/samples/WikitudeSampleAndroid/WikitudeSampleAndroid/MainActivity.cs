using System;
using Android.App;
using Android.Content;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.OS;
using System.Collections.Generic;
using Android.Util;

using System.Threading.Tasks;
using Android.Content.PM;
using Android.Support.V4.App;

using Wikitude.Architect;
using Wikitude.Common.Permission;

namespace Com.Wikitude.Samples
{
	[Activity (Label = "Component Example", MainLauncher = true)]
	public class MainActivity : Activity, IPermissionManagerPermissionManagerCallback
	{
		protected override void OnCreate (Bundle bundle)
		{
			base.OnCreate(bundle);
			SetContentView(Resource.Layout.activity_main);

			ArchitectView.PermissionManager.CheckPermissions(this, new String[] { Android.Manifest.Permission.Camera }, PermissionManager.WikitudePermissionRequest, this);
		}

		public override void OnRequestPermissionsResult(int requestCode, string[] permissions, Permission[] grantResults)
		{
			int[] result = new int[grantResults.Length];
			for (int i = 0; i < grantResults.Length; i++)
			{
				result[i] = grantResults[i] == Permission.Granted ? 0 : -1;
			}
			ArchitectView.PermissionManager.OnRequestPermissionsResult(requestCode, permissions, result);
		}

		public void PermissionsDenied(string[] permissions)
		{
			StartActivity(typeof(ErrorActivity));
			Finish();
		}

		public void PermissionsGranted(int requestCode)
		{
			StartActivity(typeof(WikitudeActivity));
			Finish();
		}

		public void ShowPermissionRationale(int requestCode, string[] permissions)
		{
			ArchitectView.PermissionManager.PositiveRationaleResult(requestCode, permissions);
		}
	}
}
