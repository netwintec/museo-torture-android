﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.Graphics;
using Android.OS;
using Android.Runtime;
using Android.Util;
using Android.Views;
using Android.Widget;
using ZXing.Mobile;
using Fragment = Android.Support.V4.App.Fragment;
using FragmentManager = Android.Support.V4.App.FragmentManager;

namespace MuseoDelleTorture
{
    public class FragmentHome : Fragment
    {

        ImageView MostraButton, MuseiButton, MostreTempButton, GiocaButton, CittaButton;
        RelativeLayout Loading;
        RelativeLayout ModalChooseSanGimBackground;

        MobileBarcodeScanner scanner;

        bool scanOpen = false;
        bool scanFineOpen = false;

        public override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);


            // Create your fragment here
        }

        public static FragmentHome Self { get; private set; }

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            // Use this to return your custom view for this Fragment
            View view = inflater.Inflate(Resource.Layout.FragmentHomeLayout, container, false);

            FragmentHome.Self = this;

            Loading = view.FindViewById<RelativeLayout>(Resource.Id.Loading);
            Loading.Visibility = ViewStates.Gone;

            Typeface tf = Typeface.CreateFromAsset(MainActivity.Instance.Assets, "fonts/Myriad_Pro.ttf");
            Typeface tfB = Typeface.CreateFromAsset(MainActivity.Instance.Assets, "fonts/Myriad_Pro_Bold.ttf");

            ModalChooseSanGimBackground = view.FindViewById<RelativeLayout>(Resource.Id.ModalChooseSanGimBackground);
            ModalChooseSanGimBackground.Visibility = ViewStates.Gone;
            ModalChooseSanGimBackground.Clickable = true;
            ModalChooseSanGimBackground.Click += (object sender, EventArgs e) => { ModalChooseSanGimBackground.Visibility = ViewStates.Gone; };

            view.FindViewById<TextView>(Resource.Id.ModalChooseSanGimTitle).Typeface = tfB;

            Button SanGimPena = view.FindViewById<Button>(Resource.Id.ModalChooseSanGim1);
            SanGimPena.Typeface = tf;
            SanGimPena.Click += (object sender, EventArgs e) =>
            {

                ModalChooseSanGimBackground.Visibility = ViewStates.Gone;
                ClickListener(2, "SanGimignano");

            };

            Button SanGimStr = view.FindViewById<Button>(Resource.Id.ModalChooseSanGim2);
            SanGimStr.Typeface = tf;
            SanGimStr.Click += (object sender, EventArgs e) =>
            {

                ModalChooseSanGimBackground.Visibility = ViewStates.Gone;
                ClickListener(2, "SanGimignanoTorture");

            };


            MostraButton = view.FindViewById<ImageView>(Resource.Id.MostraButton);
            MostraButton.Click += delegate
            {
                ClickListener(0);
            };

            MuseiButton = view.FindViewById<ImageView>(Resource.Id.MuseiButton);
            MuseiButton.Click += delegate
            {
                ClickListener(1);
            };

            MostreTempButton = view.FindViewById<ImageView>(Resource.Id.MostraTempButton);
            MostreTempButton.Click += delegate
            {
                if (MainActivity.Instance.Download && !MainActivity.Instance.ErrorDownload)
                {
                    string MuseumName = MainActivity.Instance.prefs.GetString("Museum", "");

                    if (MuseumName.Contains("SanGimignano"))
                    {
                        ModalChooseSanGimBackground.Visibility = ViewStates.Visible;
                    }
                    else
                    {
                        ClickListener(2);
                    }
                }
                else
                {
                    if (!MainActivity.Instance.LockFirstScan)
                    {
                        StartQrCodeReader();
                    }
                }
            };

            GiocaButton = view.FindViewById<ImageView>(Resource.Id.GiocaButton);
            GiocaButton.Click += delegate
            {
                ClickListener(3);
            };

            CittaButton = view.FindViewById<ImageView>(Resource.Id.CittaButton);
            CittaButton.Click += delegate
            {
                ClickListener(4);
            };

            if (MainActivity.Instance.Download && !MainActivity.Instance.ErrorDownload)
            {
                MostreTempButton.SetImageResource(Resource.Drawable.visita_on_icon);
            }


            return view;
        }

        public async void StartQrCodeReader()
        {
            var options = new ZXing.Mobile.MobileBarcodeScanningOptions();
            options.PossibleFormats = new List<ZXing.BarcodeFormat>() {
                ZXing.BarcodeFormat.QR_CODE
                };
            options.AutoRotate = false;
            options.TryHarder = true;

            scanOpen = true;

            scanner = new ZXing.Mobile.MobileBarcodeScanner();
            var result = await scanner.Scan(options);

            if (result != null)

                HandleScanResult(result, 1);
            else
            {
                scanOpen = false;
            }
        }

        void HandleScanResult(ZXing.Result result, int Indice)
        {

            scanOpen = false;

            string msg = "Wrong QRCode scan";

            if (result != null && !string.IsNullOrEmpty(result.Text))
            {

                Loading.Visibility = ViewStates.Visible;
                string id = result.Text;
                MainActivity.Instance.RunOnUiThread(() =>
                {

                    MainActivity.Instance.GetMuseumData(id,
                     (object sender, EventArgs e) =>
                     {
                         Loading.Visibility = ViewStates.Gone;

                         var prefs = MainActivity.Instance.prefs;
                         var prefEditor = prefs.Edit();
                         prefEditor.PutString("Museum", id);
                         prefEditor.PutLong("EndVisit", Utility.FromDateTime(DateTime.Now.AddHours(1)));
                         prefEditor.PutString("EndVisit_v2", DateTime.Now.AddHours(1).ToString("yyyy-MM-dd HH:mm:ss"));
                         prefEditor.Commit();

                         if (id.Contains("SanGimignano"))
                         {
                             ModalChooseSanGimBackground.Visibility = ViewStates.Visible;
                         }
                         else
                         {
                             ClickListener(2);
                         }
                     },
                     (object sender, EventArgs e) =>
                     {
                         Loading.Visibility = ViewStates.Gone;
                     });



                });
            }
            else
            {
                MainActivity.Instance.RunOnUiThread(() => Toast.MakeText(MainActivity.Instance, msg, ToastLength.Short).Show());
            }

        }


        public void DownloadBeacon(bool enter)
        {
            if (enter)
                MostreTempButton.SetImageResource(Resource.Drawable.visita_on_icon);
            else
                MostreTempButton.SetImageResource(Resource.Drawable.visita_on_icon);
        }

        public void ClickListener(int button, string id = null)
        {
            Fragment fragment = null;
            bool isFragment = false;

            switch (button)
            {
                case 0:
                    fragment = new FragmentMostra();
                    isFragment = true;
                    break;
                case 1:
                    fragment = new FragmentMusei();
                    isFragment = true;
                    break;
                case 2:
                    fragment = new FragmentStanzaHome();
                    HomePage.Instance.MuseoID = id;
                    isFragment = true;
                    break;
                case 3:
                    fragment = new FragmentGiocaHome();
                    isFragment = true;
                    break;
                case 4:
                    var intent = new Intent(this.Activity, typeof(CosaVedereActivity));
                    StartActivity(intent);
                    break;
                default:
                    break;
            }
            if (isFragment)
            {
                FragmentManager frgManager = this.FragmentManager;
                frgManager.BeginTransaction().Replace(Resource.Id.content_frame, fragment)
                    .AddToBackStack(null)
                    .Commit();
            }
        }
    }
}

