﻿using System;

using Android.App;
using Android.Content.PM;

using Android.OS;

using Android.Content;

namespace MuseoDelleTorture
{
	[Activity(MainLauncher = true, NoHistory = true, Theme = "@style/Theme.Splash", ScreenOrientation = ScreenOrientation.Portrait)]
	public class SplashScreen : Activity
	{
		bool notification;
		string desc,img;

		protected override void OnCreate(Bundle bundle)
		{
			base.OnCreate (bundle);

			notification = Intent.GetBooleanExtra ("Notification", false);
			desc = Intent.GetStringExtra ("descrizione");
			img = Intent.GetStringExtra ("image");
			Console.WriteLine ("NOT:"+notification+"|"+desc+"|"+img);
			var intent = new Intent (this, typeof(MainActivity));
			if (notification){
				intent.PutExtra ("image", img);
				intent.PutExtra ("descrizione", desc);
				intent.PutExtra ("Notification", true);
			}
			StartActivity(intent);
			Finish();
		}
	}
}

