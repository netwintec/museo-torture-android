﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Graphics;
using Android.Views;
using Android.Widget;

using Fragment = Android.Support.V4.App.Fragment;
using FragmentManager = Android.Support.V4.App.FragmentManager;

namespace MuseoDelleTorture
{
	public class FragmentMostreTemp : Fragment,View.IOnTouchListener,GestureDetector.IOnGestureListener
	{
		int attivo = 1;
		ImageView pallino1,pallino2,pallino3;
		ViewFlipper immagineSwipe;
		protected GestureDetector gestureScanner;
		private static int SWIPE_MIN_DISTANCE = 120;
		private static int SWIPE_MAX_OFF_PATH = 250;
		private static int SWIPE_THRESHOLD_VELOCITY = 200;

		public override void OnCreate (Bundle savedInstanceState)
		{
			base.OnCreate (savedInstanceState);

			// Create your fragment here
		}

		public override View OnCreateView (LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
		{
			// Use this to return your custom view for this Fragment
			// return inflater.Inflate(Resource.Layout.YourFragment, container, false);

			View view = inflater.Inflate(Resource.Layout.FragmentMostreTemp,container,false);

			Typeface tf = Typeface.CreateFromAsset(Activity.Assets,"fonts/Myriad_Pro.ttf");
			Typeface tfB = Typeface.CreateFromAsset(Activity.Assets,"fonts/Myriad_Pro_Bold.ttf");
			Typeface tfG = Typeface.CreateFromAsset(Activity.Assets,"fonts/daunpenh.ttf");

			view.FindViewById<TextView>(Resource.Id.Descrizione).Text="Survivor of Torture International  - Joan B.Kroc\nInstitute for Peace and Justice - California\nWestern School of Law";
			view.FindViewById<TextView>(Resource.Id.Descrizione).Typeface=tf;
			immagineSwipe = view.FindViewById<ViewFlipper> (Resource.Id.immagineSwipe);
			immagineSwipe.SetOnTouchListener (this);

			pallino1 = view.FindViewById<ImageView> (Resource.Id.pallino1);
			pallino2 = view.FindViewById<ImageView> (Resource.Id.pallino2);
			pallino3 = view.FindViewById<ImageView> (Resource.Id.pallino3);

			gestureScanner = new GestureDetector(this);


			view.FindViewById<TextView>(Resource.Id.textView1).Typeface=tfG;
			view.FindViewById<TextView>(Resource.Id.textView2).Typeface=tfG;
			view.FindViewById<TextView>(Resource.Id.textView3).Typeface=tfB;
			view.FindViewById<TextView>(Resource.Id.textView5).Typeface=tfB;
			view.FindViewById<TextView>(Resource.Id.textView6).Typeface=tfB;
			view.FindViewById<TextView>(Resource.Id.textView7).Typeface=tfB;
			view.FindViewById<TextView>(Resource.Id.AddressText).Typeface=tf;
			view.FindViewById<TextView>(Resource.Id.PriceText).Typeface=tf;
			view.FindViewById<TextView>(Resource.Id.InfoText).Typeface=tf;
			view.FindViewById<TextView>(Resource.Id.OpeningText).Typeface=tf;
			view.FindViewById<TextView>(Resource.Id.AddressText).Typeface=tf;
			view.FindViewById<TextView>(Resource.Id.Titolo).Typeface=tfG;
			view.FindViewById<TextView>(Resource.Id.Data).Typeface=tfG;
			view.FindViewById<TextView>(Resource.Id.Descrizione).Typeface=tf;


			return view;
		}

		public bool OnTouch (View v, MotionEvent e)
		{
			return gestureScanner.OnTouchEvent(e);
		}

		public bool OnDown (MotionEvent e)
		{
			return true;
		}

		public bool OnFling (MotionEvent e1, MotionEvent e2, float velocityX, float velocityY)
		{
			Console.WriteLine(e1.GetX()+"  "+e2.GetX()+"  "+ Math.Abs(e1.GetX() - e2.GetX())+"  "+velocityX+"  "+Math.Abs(velocityX));
			try {
				if(e1.GetX() > e2.GetX() && Math.Abs(e1.GetX() - e2.GetX()) > SWIPE_MIN_DISTANCE && Math.Abs(velocityX) > SWIPE_THRESHOLD_VELOCITY) {
					Console.WriteLine("L");
					//Toast.makeText(this.getApplicationContext(), "Left", Toast.LENGTH_SHORT).show();
					attivo++;
					if(attivo==4)attivo=1;
					cambiaPallino(attivo);
					immagineSwipe.ShowPrevious();
				}else if (e1.GetX() < e2.GetX() && e2.GetX() - e1.GetX() > SWIPE_MIN_DISTANCE && Math.Abs(velocityX) > SWIPE_THRESHOLD_VELOCITY) {
					Console.WriteLine("R");
					//Toast.makeText(this.getApplicationContext(), "Right", Toast.LENGTH_SHORT).show();
					immagineSwipe.ShowNext();
					attivo--;
					if(attivo==0)attivo=3;
					cambiaPallino(attivo);
				}
			} catch (Exception e) {
				// nothing
			}
			return true;

		}

		public void OnLongPress (MotionEvent e)
		{
		}

		public bool OnScroll (MotionEvent e1, MotionEvent e2, float distanceX, float distanceY)
		{
			return true;
		}

		public void OnShowPress (MotionEvent e)
		{
		}

		public bool OnSingleTapUp (MotionEvent e)
		{
			return true;
		}

		public void cambiaPallino(int posizione){

			if (posizione == 1) {
				pallino1.SetImageResource (Resource.Drawable.pallinorosso);
				pallino2.SetImageResource (Resource.Drawable.pallinobianco);
				pallino3.SetImageResource (Resource.Drawable.pallinobianco);
			}
			if (posizione == 2) {
				pallino1.SetImageResource (Resource.Drawable.pallinobianco);
				pallino2.SetImageResource (Resource.Drawable.pallinorosso);
				pallino3.SetImageResource (Resource.Drawable.pallinobianco);
			}
			if (posizione == 3) {
				pallino1.SetImageResource (Resource.Drawable.pallinobianco);
				pallino2.SetImageResource (Resource.Drawable.pallinobianco);
				pallino3.SetImageResource (Resource.Drawable.pallinorosso);
			}
		}
	}

	/*
	class SwipeGestureDetector : Gest {
		@Override
		public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {
			try {
				// right to left swipe
				if (e1.getX() - e2.getX() > SWIPE_MIN_DISTANCE && Math.abs(velocityX) > SWIPE_THRESHOLD_VELOCITY) {
					mViewFlipper.setInAnimation(AnimationUtils.loadAnimation(mContext, R.anim.left_in));
					mViewFlipper.setOutAnimation(AnimationUtils.loadAnimation(mContext, R.anim.left_out));					
					mViewFlipper.showNext();
					return true;
				} else if (e2.getX() - e1.getX() > SWIPE_MIN_DISTANCE && Math.abs(velocityX) > SWIPE_THRESHOLD_VELOCITY) {
					mViewFlipper.setInAnimation(AnimationUtils.loadAnimation(mContext, R.anim.right_in));
					mViewFlipper.setOutAnimation(AnimationUtils.loadAnimation(mContext,R.anim.right_out));
					mViewFlipper.showPrevious();
					return true;
				}

			} catch (Exception e) {
				e.printStackTrace();
			}

			return false;
		}
	}*/
}