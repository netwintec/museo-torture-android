﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Graphics;
using Android.Util;
using Android.Views;
using Android.Widget;

using Fragment = Android.Support.V4.App.Fragment;
//using FragmentManager = Android.Support.V4.App.FragmentManager;

using Android.Gms.Maps;
using Android.Gms.Maps.Model;

namespace MuseoDelleTorture
{
	public class FragmentMusei : Fragment,IOnMapReadyCallback
	{
		TextView PrimaTextClick,SecondaTextClick,TerzaTextClick,QuartaTextClick,QuintaTextClick;
		ImageView PrimaFreccia,SecondaFreccia,TerzaFreccia,QuartaFreccia,QuintaFreccia;
		int indice1=0,indice2=0,indice3=0,indice4=0,indice5=0;
		RelativeLayout rl,rl1,rl2,rl3,rl4,rl5;
		View DetailView,DetailView2,DetailView3,DetailView4,DetailView5;

		MapFragment mapFrag;
		GoogleMap map;

		LatLng Coord1 = new LatLng(43.464941, 11.042679); //san giminiano 1
		LatLng Coord1_2 = new LatLng(43.465539, 11.042644); //san giminiano 2
		LatLng Coord2 = new LatLng(43.401470, 10.863198); //volterra
		LatLng Coord3 = new LatLng(43.317884, 11.330948); //siena
		LatLng Coord4 = new LatLng(43.846435, 10.506773); //lucca
		LatLng Coord5 = new LatLng(43.092201, 11.780358); //montepulciano


		public override void OnCreate (Bundle savedInstanceState)
		{
			base.OnCreate (savedInstanceState);
			// Create your fragment here

		}

		public override View OnCreateView (LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
		//protected override void OnCreate (Bundle bundle)
		{
			// Use this to return your custom view for this Fragment
			// return inflater.Inflate(Resource.Layout.YourFragment, container, false);
			container.RemoveAllViews();

			View view = inflater.Inflate(Resource.Layout.FragmentMuseiLayout,container,false);
			//SetContentView (Resource.Layout.FragmentMuseiLayout);

			//Creazione Mappa



			//mapFrag =fm.FindFragmentById<MapFragment>(Resource.Id.content_map_frame);// =FragmentManager.FindFragmentById<MapFragment>(Resource.Id.content_map_frame);
			mapFrag = Activity.FragmentManager.FindFragmentById<MapFragment>(Resource.Id.content_map_frame);//.GetMapAsync(this);
			mapFrag.GetMapAsync(this);

			Typeface tf = Typeface.CreateFromAsset(Activity.Assets,"fonts/Myriad_Pro.ttf");
			Typeface tfB = Typeface.CreateFromAsset(Activity.Assets,"fonts/Myriad_Pro_Bold.ttf");
			Typeface tfG = Typeface.CreateFromAsset(Activity.Assets,"fonts/daunpenh.ttf");

			TextView text = view.FindViewById<TextView>(Resource.Id.textView1);
			text.Typeface = tfG;

			DetailView = inflater.Inflate(Resource.Layout.SanGiminianoLayout,container,false);
			ChangeFont (DetailView, true);
			DetailView2 = inflater.Inflate(Resource.Layout.VolterraLayout,container,false);
			ChangeFont (DetailView2, false);
			DetailView3 = inflater.Inflate(Resource.Layout.SienaLayout,container,false);
			ChangeFont (DetailView3, false);
			DetailView4 = inflater.Inflate(Resource.Layout.LuccaLayout,container,false);
			ChangeFont (DetailView4, false);
			DetailView5 = inflater.Inflate(Resource.Layout.MontepulcianoLayout,container,false);
			ChangeFont (DetailView5, false);
			//Prima Location
			rl = view.FindViewById<RelativeLayout> (Resource.Id.PrimoTextComparsa);
			PrimaTextClick = view.FindViewById<TextView>(Resource.Id.PrimaTextClick);
			PrimaTextClick.Typeface = tfG;
			PrimaFreccia = view.FindViewById<ImageView> (Resource.Id.primaFreccia);

			//Seconda Location
			rl2 = view.FindViewById<RelativeLayout> (Resource.Id.SecondaTextComparsa);
			SecondaTextClick = view.FindViewById<TextView>(Resource.Id.SecondaTextClick);
			SecondaTextClick.Typeface = tfG;
			SecondaFreccia = view.FindViewById<ImageView> (Resource.Id.secondaFreccia);

			//Terza Location
			rl3 = view.FindViewById<RelativeLayout> (Resource.Id.TerzoTextComparsa);
			TerzaTextClick = view.FindViewById<TextView>(Resource.Id.TerzoTextClick);
			TerzaTextClick.Typeface = tfG;
			TerzaFreccia = view.FindViewById<ImageView> (Resource.Id.terzaFreccia);

			//Quarta Location
			rl4 = view.FindViewById<RelativeLayout> (Resource.Id.QuartaTextComparsa);
			QuartaTextClick = view.FindViewById<TextView>(Resource.Id.QuartaTextClick);
			QuartaTextClick.Typeface = tfG;
			QuartaFreccia = view.FindViewById<ImageView> (Resource.Id.quartaFreccia);

			//Prima Location
			rl5 = view.FindViewById<RelativeLayout> (Resource.Id.QuintaTextComparsa);
			QuintaTextClick = view.FindViewById<TextView>(Resource.Id.QuintaTextClick);
			QuintaTextClick.Typeface = tfG;
			QuintaFreccia = view.FindViewById<ImageView> (Resource.Id.quintaFreccia);



			// Tap Prima Location
			PrimaTextClick.Click += delegate {
				GestioneLayout(1);
			};
			PrimaFreccia.Click += delegate {
				GestioneLayout(1);
			};

			// Tap Seconda Location
			SecondaTextClick.Click += delegate {
				GestioneLayout(2);
			};
			SecondaFreccia.Click += delegate {
				GestioneLayout(2);
			};

			// Tap Terza Location
			TerzaTextClick.Click += delegate {
				GestioneLayout(3);
			};
			TerzaFreccia.Click += delegate {
				GestioneLayout(3);
			};

			// Tap Quarta Location
			QuartaTextClick.Click += delegate {
				GestioneLayout(4);
			};
			QuartaFreccia.Click += delegate {
				GestioneLayout(4);
			};

			// Tap Quinta Location
			QuintaTextClick.Click += delegate {
				GestioneLayout(5);
			};
			QuintaFreccia.Click += delegate {
				GestioneLayout(5);
			};
				

			return view;
		}

		public override void OnDestroyView ()
		{
			
			mapFrag.Dispose ();

			base.OnDestroyView();
			try
			{
				Android.App.Fragment fragment = (Activity.FragmentManager.FindFragmentById(Resource.Id.content_map_frame));
				FragmentTransaction ft = Activity.FragmentManager.BeginTransaction();
				ft.Remove(fragment);
				ft.Commit();
			}
			catch (Exception){ }
		}

		public void ChangeFont (View view,bool isGim){

			Typeface tf = Typeface.CreateFromAsset(Activity.Assets,"fonts/Myriad_Pro.ttf");
			Typeface tfB = Typeface.CreateFromAsset(Activity.Assets,"fonts/Myriad_Pro_Bold.ttf");

			TextView text = view.FindViewById<TextView> (Resource.Id.textView2);
			text.Typeface = tfB;
			TextView text1 = view.FindViewById<TextView> (Resource.Id.textView3);
			text1.Typeface = tfB;
			TextView text2 = view.FindViewById<TextView> (Resource.Id.textView4);
			text2.Typeface = tf;
			TextView text3 = view.FindViewById<TextView> (Resource.Id.textView5);
			text3.Typeface = tf;
			TextView text4 = view.FindViewById<TextView> (Resource.Id.textView6);
			text4.Typeface = tfB;
			TextView text5 = view.FindViewById<TextView> (Resource.Id.textView7);
			text5.Typeface = tf;
			TextView text6 = view.FindViewById<TextView> (Resource.Id.textView8);
			text6.Typeface = tfB;
			TextView text7 = view.FindViewById<TextView> (Resource.Id.textView9);
			text7.Typeface = tf;

			if (isGim)
			{
				TextView text8 = view.FindViewById<TextView>(Resource.Id.textView10);
				text8.Typeface = tf;
			}
		}

		public void GestioneLayout(int location){

			if(location != 1 && rl.ChildCount!=0){
				rl.RemoveAllViews();
				PrimaFreccia.SetImageResource(Resource.Drawable.freccia_giu);
				PrimaTextClick.SetTextColor (Color.White);
				indice1++;
			}
			if(location != 2 && rl2.ChildCount!=0){
				rl2.RemoveAllViews();
				SecondaFreccia.SetImageResource(Resource.Drawable.freccia_giu);
				SecondaTextClick.SetTextColor (Color.White);
				indice2++;
			}
			if(location != 3 && rl3.ChildCount!=0){
				rl3.RemoveAllViews();
				TerzaFreccia.SetImageResource(Resource.Drawable.freccia_giu);
				TerzaTextClick.SetTextColor (Color.White);
				indice3++;
			}
			if(location != 4 && rl4.ChildCount!=0){
				rl4.RemoveAllViews();
				QuartaFreccia.SetImageResource(Resource.Drawable.freccia_giu);
				QuartaTextClick.SetTextColor (Color.White);
				indice4++;
			}
			if(location != 5 && rl5.ChildCount!=0){
				rl5.RemoveAllViews();
				QuintaFreccia.SetImageResource(Resource.Drawable.freccia_giu);
				QuintaTextClick.SetTextColor (Color.White);
				indice5++;
			}

			switch (location) {
			case 1:
				if (indice1 % 2 == 0) {
					rl.AddView (DetailView);
					PrimaFreccia.SetImageResource(Resource.Drawable.freccia_su);
					PrimaTextClick.SetTextColor (Color.Rgb(152,20,27));

					CameraPosition.Builder builder = CameraPosition.InvokeBuilder ();
					builder.Target (Coord1);
					builder.Zoom (14);
					CameraPosition cameraPosition = builder.Build ();
					CameraUpdate cameraUpdate = CameraUpdateFactory.NewCameraPosition (cameraPosition);
					map.MoveCamera (cameraUpdate);


				} else {
					rl.RemoveAllViews();
					PrimaFreccia.SetImageResource(Resource.Drawable.freccia_giu);
					PrimaTextClick.SetTextColor (Color.White);

				}
				indice1++;
				break;
			case 2:
				if (indice2 % 2 == 0) {
					rl2.AddView (DetailView2);
					SecondaFreccia.SetImageResource(Resource.Drawable.freccia_su);
					SecondaTextClick.SetTextColor (Color.Rgb(152,20,27));

					CameraPosition.Builder builder = CameraPosition.InvokeBuilder ();
					builder.Target (Coord2);
					builder.Zoom (14);
					CameraPosition cameraPosition = builder.Build ();
					CameraUpdate cameraUpdate = CameraUpdateFactory.NewCameraPosition (cameraPosition);
					map.MoveCamera (cameraUpdate);

				} else {
					rl2.RemoveAllViews();
					SecondaFreccia.SetImageResource(Resource.Drawable.freccia_giu);
					SecondaTextClick.SetTextColor (Color.White);

				}
				indice2++;
				break;
			case 3:
				if (indice3 % 2 == 0) {
					rl3.AddView (DetailView3);
					TerzaFreccia.SetImageResource(Resource.Drawable.freccia_su);
					TerzaTextClick.SetTextColor (Color.Rgb(152,20,27));

					CameraPosition.Builder builder = CameraPosition.InvokeBuilder ();
					builder.Target (Coord3);
					builder.Zoom (14);
					CameraPosition cameraPosition = builder.Build ();
					CameraUpdate cameraUpdate = CameraUpdateFactory.NewCameraPosition (cameraPosition);
					map.MoveCamera (cameraUpdate);

				} else {
					rl3.RemoveAllViews();
					TerzaFreccia.SetImageResource(Resource.Drawable.freccia_giu);
					TerzaTextClick.SetTextColor (Color.White);

				}
				indice3++;
				break;
			case 4:
				if (indice4 % 2 == 0) {
					rl4.AddView (DetailView4);
					QuartaFreccia.SetImageResource(Resource.Drawable.freccia_su);
					QuartaTextClick.SetTextColor (Color.Rgb(152,20,27));

					CameraPosition.Builder builder = CameraPosition.InvokeBuilder ();
					builder.Target (Coord4);
					builder.Zoom (14);
					CameraPosition cameraPosition = builder.Build ();
					CameraUpdate cameraUpdate = CameraUpdateFactory.NewCameraPosition (cameraPosition);
					map.MoveCamera (cameraUpdate);

				} else {
					rl4.RemoveAllViews();
					QuartaFreccia.SetImageResource(Resource.Drawable.freccia_giu);
					QuartaTextClick.SetTextColor (Color.White);

				}
				indice4++;
				break;
			case 5:
				if (indice5 % 2 == 0) {
					rl5.AddView (DetailView5);
					QuintaFreccia.SetImageResource(Resource.Drawable.freccia_su);
					QuintaTextClick.SetTextColor (Color.Rgb(152,20,27));

					CameraPosition.Builder builder = CameraPosition.InvokeBuilder ();
					builder.Target (Coord5);
					builder.Zoom (14);
					CameraPosition cameraPosition = builder.Build ();
					CameraUpdate cameraUpdate = CameraUpdateFactory.NewCameraPosition (cameraPosition);
					map.MoveCamera (cameraUpdate);

				} else {
					rl5.RemoveAllViews();
					QuintaFreccia.SetImageResource(Resource.Drawable.freccia_giu);
					QuintaTextClick.SetTextColor (Color.White);

				}
				indice5++;
				break;
			}
		}

		public void AggiungiMarker(LatLng Coord,int indice){
			
			MarkerOptions markerOpt = new MarkerOptions();
			markerOpt.SetPosition(Coord);
			if (indice == 0)
				markerOpt.SetIcon (BitmapDescriptorFactory.FromResource (Resource.Drawable.marker_san_gimignano));
			if (indice == 1)
				markerOpt.SetIcon (BitmapDescriptorFactory.FromResource (Resource.Drawable.marker_volterra));
			if (indice == 2)
				markerOpt.SetIcon (BitmapDescriptorFactory.FromResource (Resource.Drawable.marker_siena));
			if (indice == 3)
				markerOpt.SetIcon (BitmapDescriptorFactory.FromResource (Resource.Drawable.marker_lucca));
			if (indice == 4)
				markerOpt.SetIcon (BitmapDescriptorFactory.FromResource (Resource.Drawable.marker_montepulciano));
			map.AddMarker (markerOpt);
		}

		public void OnMapReady(GoogleMap googleMap)
		{
   			LatLng location = new LatLng(43.472449, 11.250806);//CENTRO ITALIA :42.40433, 12.856337);
			CameraPosition.Builder builder = CameraPosition.InvokeBuilder();
			builder.Target(location);
			builder.Zoom(7);//5);
			CameraPosition cameraPosition = builder.Build();

			CameraUpdate cameraUpdate = CameraUpdateFactory.NewCameraPosition(cameraPosition);
		
			map = googleMap;

			if (map != null) {
				map.MapType = GoogleMap.MapTypeNormal;
				map.UiSettings.ZoomControlsEnabled = true;
				map.UiSettings.CompassEnabled = true;
				map.UiSettings.MyLocationButtonEnabled = false;
				map.MyLocationEnabled = false;
				map.MoveCamera (cameraUpdate);

				AggiungiMarker(Coord1,0);
				AggiungiMarker(Coord1_2, 0);
				AggiungiMarker(Coord2,1);
				AggiungiMarker(Coord3,2);
				AggiungiMarker(Coord4,3);
				AggiungiMarker(Coord5,4);
			}
		}
	}
}

