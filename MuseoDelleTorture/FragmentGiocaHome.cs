﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Util;
using Android.Views;
using Android.Widget;
using Android.Graphics;

using Fragment = Android.Support.V4.App.Fragment;
using FragmentManager = Android.Support.V4.App.FragmentManager;
using RestSharp;

namespace MuseoDelleTorture
{
	public class FragmentGiocaHome : Fragment
	{

		List<ImageView> GiocaImages= new List<ImageView>();
		List<TextView> GiocaTexts = new List<TextView>();

		public List<List<GiocaElement>> ListGioca = new List<List<GiocaElement>> ();
		public List<GiocaResult> ListResult = new List<GiocaResult> ();

		public int selected;
		int porta = 82;

		bool shouldPopBack = false;

		public override void OnResume()
		{
			base.OnResume();

			if (shouldPopBack)
			{
				try
				{
					FragmentManager.PopBackStack ();
				}
				catch (Exception e)
				{

				}

				shouldPopBack = false;


			}
		}

		public override void OnCreate (Bundle savedInstanceState)
		{
			base.OnCreate (savedInstanceState);


			// Create your fragment here
		}

		public static FragmentGiocaHome Self {get;private set;}

		public override View OnCreateView (LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
		{
			// Use this to return your custom view for this Fragment
			View view = inflater.Inflate(Resource.Layout.FragmentGiocaHome,container,false);

			FragmentGiocaHome.Self = this;

			Typeface tf = Typeface.CreateFromAsset(Activity.Assets,"fonts/daunpenh.ttf"); 

			GiocaImages.Clear ();
			GiocaTexts.Clear ();

			GiocaImages.Add (view.FindViewById<ImageView>(Resource.Id.ImageGioca1));
			GiocaImages.Add (view.FindViewById<ImageView>(Resource.Id.ImageGioca2));
			GiocaImages.Add (view.FindViewById<ImageView>(Resource.Id.ImageGioca3));
			GiocaImages.Add (view.FindViewById<ImageView>(Resource.Id.ImageGioca4));
			GiocaImages.Add (view.FindViewById<ImageView>(Resource.Id.ImageGioca5));
			GiocaImages.Add (view.FindViewById<ImageView>(Resource.Id.ImageGioca6));

			GiocaTexts.Add (view.FindViewById<TextView>(Resource.Id.textGioca1));
			GiocaTexts.Add (view.FindViewById<TextView>(Resource.Id.textGioca2));
			GiocaTexts.Add (view.FindViewById<TextView>(Resource.Id.textGioca3));
			GiocaTexts.Add (view.FindViewById<TextView>(Resource.Id.textGioca4));
			GiocaTexts.Add (view.FindViewById<TextView>(Resource.Id.textGioca5));
			GiocaTexts.Add (view.FindViewById<TextView>(Resource.Id.textGioca6));

			view.FindViewById<TextView> (Resource.Id.titolo).Typeface = tf;
			view.FindViewById<TextView>(Resource.Id.textView2).Typeface = tf;

			var lang = Context.Resources.Configuration.Locale;



			Console.WriteLine ("Riprova"+ListResult.Count);
			if (ListResult.Count == 0) {

				var client = new RestClient("http://api.netwintec.com:" + porta + "/");
				//client.Authenticator = new HttpBasicAuthenticator(username, password);

				var requestN4U = new RestRequest("play" , Method.GET);
				requestN4U.AddHeader("content-type", "application/json");
				requestN4U.AddHeader("Net4U-Company", "museotortura");
				requestN4U.AddHeader("Net4U-Token", MainActivity.Instance.prefs.GetString ("TokenN4U", ""));

				requestN4U.Timeout = 60000;

				IRestResponse response = client.Execute(requestN4U);

				Console.WriteLine("GIOCA:"+response.StatusCode+"|"+response.Content);

				if (response.StatusCode == System.Net.HttpStatusCode.OK) {

					JsonUtility ju = new JsonUtility ();
					StreamReader strm = new StreamReader (Activity.Assets.Open ("Gioca.json"));
					ju.SpacchettamentoJsonGioca (ListGioca, ListResult, response.Content, lang.Country);
				} else {
					try
					{
						FragmentManager.PopBackStack ();
					}
					catch (Exception e) {
						shouldPopBack = true;
					}

					//SupportFragmentManager.PopBackStack();
				}
			}


			//Console.WriteLine (ListResult.Count+"|"+ListGioca.Count+"|"+ListGioca[0].Count+"|"+ListGioca[1].Count);

			for (int i = 0; i < ListResult.Count; i++) {

				//GiocaTexts [i].Text = ListResult [i].Luogo;
				//GiocaTexts [i].Typeface = tf;
				Console.WriteLine (i+" "+ListResult[i].Luogo+"|"+ListResult [i].IndiceActive);

				GiocaImages[i].SetBackgroundColor(Color.Black);

				if (ListResult [i].Luogo == "lucca") {
					GiocaTexts [i].Text = "Lucca";
					GiocaTexts [i].Typeface = tf;
					if (ListResult [i].IndiceActive == 0)
						GiocaImages [i].SetImageResource (Resource.Drawable.GiocaL);
					else
						GiocaImages [i].SetImageResource (Resource.Drawable.GiocaN);
				}
				if (ListResult [i].Luogo == "siena") {
					GiocaTexts [i].Text = "Siena";
					GiocaTexts [i].Typeface = tf;
					if (ListResult [i].IndiceActive == 0)
						GiocaImages [i].SetImageResource (Resource.Drawable.GiocaS);
					else
						GiocaImages [i].SetImageResource (Resource.Drawable.GiocaN);
				}
				if (ListResult [i].Luogo == "volterra") {
					GiocaTexts [i].Text = "Volterra";
					GiocaTexts [i].Typeface = tf;
					if (ListResult [i].IndiceActive == 0)
						GiocaImages [i].SetImageResource (Resource.Drawable.GiocaV);
					else
						GiocaImages [i].SetImageResource (Resource.Drawable.GiocaN);
				}
				if (ListResult [i].Luogo == "montepulciano") {
					GiocaTexts [i].Text = "Montepulciano";
					GiocaTexts [i].Typeface = tf;
					if (ListResult [i].IndiceActive == 0)
						GiocaImages [i].SetImageResource (Resource.Drawable.GiocaM);
					else
						GiocaImages [i].SetImageResource (Resource.Drawable.GiocaN);
				}
				if (ListResult [i].Luogo == "san giminiano") {
					GiocaTexts [i].Text = "San Giminiano";
					GiocaTexts [i].Typeface = tf;
					if (ListResult [i].IndiceActive == 0)
						GiocaImages [i].SetImageResource (Resource.Drawable.GiocaSG);
					else
						GiocaImages [i].SetImageResource (Resource.Drawable.GiocaN);
				}
				if (ListResult [i].Luogo == "san marino") {
					GiocaTexts [i].Text = "San Marino";
					GiocaTexts [i].Typeface = tf;
					if (ListResult [i].IndiceActive == 0)
						GiocaImages [i].SetImageResource (Resource.Drawable.GiocaSM);
					else
						GiocaImages [i].SetImageResource (Resource.Drawable.GiocaSMN);
				}

			}

			view.FindViewById<ImageView>(Resource.Id.ImageGioca1).Click += delegate {

				selected=0;
				if(ListResult [selected].IndiceActive == 0){
					Fragment fragment = new FragmentGiocaMuseum();
					FragmentManager frgManager = FragmentManager;
					frgManager.BeginTransaction()
						.Replace(Resource.Id.content_frame, fragment)
						.AddToBackStack(null)
						.Commit();
				}
				if(ListResult [selected].IndiceActive == 1){
					AlertDialog.Builder alert = new AlertDialog.Builder (Context);
					alert.SetTitle("Gioca");
					alert.SetMessage ("Visita il museo per poter giocare");
					alert.SetPositiveButton ("OK", (senderAlert, args) => {} );
					alert.Show();
				}
				if(ListResult [selected].IndiceActive == 2){
					AlertDialog.Builder alert = new AlertDialog.Builder (Context);
					alert.SetTitle("Gioca");
					alert.SetMessage ("Gioca non disponibile per questo museo");
					alert.SetPositiveButton ("OK", (senderAlert, args) => {} );
					alert.Show();
				}

			};

			view.FindViewById<ImageView>(Resource.Id.ImageGioca2).Click += delegate {

				selected=1;
				if(ListResult [selected].IndiceActive == 0){
					Fragment fragment = new FragmentGiocaMuseum();
					FragmentManager frgManager = FragmentManager;
					frgManager.BeginTransaction()
						.Replace(Resource.Id.content_frame, fragment)
						.AddToBackStack(null)
						.Commit();
				}
				if(ListResult [selected].IndiceActive == 1){
					AlertDialog.Builder alert = new AlertDialog.Builder (Context);
					alert.SetTitle("Gioca");
					alert.SetMessage ("Visita il museo per poter giocare");
					alert.SetPositiveButton ("OK", (senderAlert, args) => {} );
					alert.Show();
				}
				if(ListResult [selected].IndiceActive == 2){
					AlertDialog.Builder alert = new AlertDialog.Builder (Context);
					alert.SetTitle("Gioca");
					alert.SetMessage ("Gioca non disponibile per questo museo");
					alert.SetPositiveButton ("OK", (senderAlert, args) => {} );
					alert.Show();
				}

			};

			view.FindViewById<ImageView>(Resource.Id.ImageGioca3).Click += delegate {

				selected=2;
				if(ListResult [selected].IndiceActive == 0){
					Fragment fragment = new FragmentGiocaMuseum();
					FragmentManager frgManager = FragmentManager;
					frgManager.BeginTransaction()
						.Replace(Resource.Id.content_frame, fragment)
						.AddToBackStack(null)
						.Commit();
				}
				if(ListResult [selected].IndiceActive == 1){
					AlertDialog.Builder alert = new AlertDialog.Builder (Context);
					alert.SetTitle("Gioca");
					alert.SetMessage ("Visita il museo per poter giocare");
					alert.SetPositiveButton ("OK", (senderAlert, args) => {} );
					alert.Show();
				}
				if(ListResult [selected].IndiceActive == 2){
					AlertDialog.Builder alert = new AlertDialog.Builder (Context);
					alert.SetTitle("Gioca");
					alert.SetMessage ("Gioca non disponibile per questo museo");
					alert.SetPositiveButton ("OK", (senderAlert, args) => {} );
					alert.Show();
				}

			};

			view.FindViewById<ImageView>(Resource.Id.ImageGioca4).Click += delegate {

				selected=3;
				if(ListResult [selected].IndiceActive == 0){
					Fragment fragment = new FragmentGiocaMuseum();
					FragmentManager frgManager = FragmentManager;
					frgManager.BeginTransaction()
						.Replace(Resource.Id.content_frame, fragment)
						.AddToBackStack(null)
						.Commit();
				}
				if(ListResult [selected].IndiceActive == 1){
					AlertDialog.Builder alert = new AlertDialog.Builder (Context);
					alert.SetTitle("Gioca");
					alert.SetMessage ("Visita il museo per poter giocare");
					alert.SetPositiveButton ("OK", (senderAlert, args) => {} );
					alert.Show();
				}
				if(ListResult [selected].IndiceActive == 2){
					AlertDialog.Builder alert = new AlertDialog.Builder (Context);
					alert.SetTitle("Gioca");
					alert.SetMessage ("Gioca non disponibile per questo museo");
					alert.SetPositiveButton ("OK", (senderAlert, args) => {} );
					alert.Show();
				}

			};

			view.FindViewById<ImageView>(Resource.Id.ImageGioca5).Click += delegate {

				selected=4;
				if(ListResult [selected].IndiceActive == 0){
					Fragment fragment = new FragmentGiocaMuseum();
					FragmentManager frgManager = FragmentManager;
					frgManager.BeginTransaction()
						.Replace(Resource.Id.content_frame, fragment)
						.AddToBackStack(null)
						.Commit();
				}
				if(ListResult [selected].IndiceActive == 1){
					AlertDialog.Builder alert = new AlertDialog.Builder (Context);
					alert.SetTitle("Gioca");
					alert.SetMessage ("Visita il museo per poter giocare");
					alert.SetPositiveButton ("OK", (senderAlert, args) => {} );
					alert.Show();
				}
				if(ListResult [selected].IndiceActive == 2){
					AlertDialog.Builder alert = new AlertDialog.Builder (Context);
					alert.SetTitle("Gioca");
					alert.SetMessage ("Gioca non disponibile per questo museo");
					alert.SetPositiveButton ("OK", (senderAlert, args) => {} );
					alert.Show();
				}

			};

			view.FindViewById<ImageView>(Resource.Id.ImageGioca6).Click += delegate {

				selected=5;
				if(ListResult [selected].IndiceActive == 0){
					Fragment fragment = new FragmentGiocaMuseum();
					FragmentManager frgManager = FragmentManager;
					frgManager.BeginTransaction()
						.Replace(Resource.Id.content_frame, fragment)
						.AddToBackStack(null)
						.Commit();
				}
				if(ListResult [selected].IndiceActive == 1){
					AlertDialog.Builder alert = new AlertDialog.Builder (Context);
					alert.SetTitle("Gioca");
					alert.SetMessage ("Visita il museo per poter giocare");
					alert.SetPositiveButton ("OK", (senderAlert, args) => {} );
					alert.Show();
				}
				if(ListResult [selected].IndiceActive == 2){
					AlertDialog.Builder alert = new AlertDialog.Builder (Context);
					alert.SetTitle("Gioca");
					alert.SetMessage ("Gioca non disponibile per questo museo");
					alert.SetPositiveButton ("OK", (senderAlert, args) => {} );
					alert.Show();
				}

			};

			return view;
			//return base.OnCreateView (inflater, container, savedInstanceState);
		}
			
	}
}

