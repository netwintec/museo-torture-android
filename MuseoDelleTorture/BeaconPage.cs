﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.Content.PM;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

using Fragment = Android.Support.V4.App.Fragment;
using FragmentManager = Android.Support.V4.App.FragmentManager;

namespace MuseoDelleTorture
{
	[Activity (Label = "BeaconPage",Theme = "@style/AppTheme",ScreenOrientation = ScreenOrientation.Portrait,NoHistory = true)]		
	public class BeaconPage : BaseActivity
	{

		public ImageView backIcon,menuIcon;

		public FragmentManager.IOnBackStackChangedListener backStackListener;

		ISharedPreferences prefs;

		public static BeaconPage Instance { private set; get;}

		bool shouldPopBack = false;

		protected override void OnResume()
		{
			base.OnResume();

			if (shouldPopBack) {
				try
				{
					Console.WriteLine("POPPBACCKKKK");
					SupportFragmentManager.PopBackStack();
					if (FragmentBeaconImage.player != null)
					{
						FragmentBeaconImage.player.Release();
					}
				}
				catch (Exception e) { 
				
				}
				shouldPopBack = false;
					
		    }
		}

		protected override void OnCreate (Bundle savedInstanceState)
		{
			base.OnCreate (savedInstanceState);

			setActionBarIcon(Resource.Drawable.menu_icon,Resource.Drawable.back_icon);

			//Set default settings
			//PreferenceManager.setDefaultValues(this, R.xml.preferences, false);

			BeaconPage.Instance = this;

			prefs = MainActivity.Instance.prefs;
			MainActivity.Instance.TokenGood = true;
			MainActivity.Instance.ScanBeIn = true;
			HomePage.Instance.FromBeacon = true;

			menuIcon = (ImageView) getToolbar().FindViewById(Resource.Id.home_icon);
			backIcon = (ImageView) getToolbar().FindViewById(Resource.Id.back_icon);
			backIcon.Visibility = ViewStates.Visible;
			menuIcon.Visibility = ViewStates.Invisible;

			backIcon.Click += delegate {
				Console.WriteLine("INFO"+ (SupportFragmentManager.BackStackEntryCount).ToString());
				if(SupportFragmentManager.BackStackEntryCount == 0) {
					BeaconPage.Instance = null;
					Finish();
				} else {
					//int i=SupportFragmentManager.BackStackEntryCount;
					//while(i>0)
					try
					{
						SupportFragmentManager.PopBackStack();
						if (FragmentBeaconImage.player != null)
						{
							FragmentBeaconImage.player.Release();
						}
					}
					catch (Exception e) {
						shouldPopBack = true;
					}
				}
			};
				


			//Initialize fragment
			Fragment fragment;
			//Bundle args = new Bundle();
			fragment = new FragmentBeaconHomePage();
			//args.putString(FragmentHome.ITEM_NAME, dataList.get(1).getItemName());
			//fragment.setArguments(args);
			FragmentManager frgManager = SupportFragmentManager;
			frgManager.BeginTransaction().Replace(Resource.Id.content_frame, fragment)
				.Commit();

			SupportFragmentManager.BackStackChanged += delegate {
				Console.WriteLine ("INFO" + (SupportFragmentManager.BackStackEntryCount).ToString ());
				if (SupportFragmentManager.BackStackEntryCount == 0) {
					//backIcon.Visibility = ViewStates.Invisible;
				} else {
					//backIcon.Visibility = ViewStates.Visible;
					//backIcon.SetImageResource(Resource.Drawable.back_icon);
				}
			};
				

		}

		protected override int getLayoutResource() {
			return Resource.Layout.BeaconPageLayout;
		}
			

		public override void OnBackPressed ()
		{
			backIcon.Click += delegate
			{
				Console.WriteLine("INFO" + (SupportFragmentManager.BackStackEntryCount).ToString());
				if (SupportFragmentManager.BackStackEntryCount == 0)
				{
					BeaconPage.Instance = null;
					Finish();
				}
				else {
					//int i=SupportFragmentManager.BackStackEntryCount;
					//while(i>0)
					try
					{
						SupportFragmentManager.PopBackStack();
						if (FragmentBeaconImage.player != null)
						{
							FragmentBeaconImage.player.Release();
						}
					}
					catch (Exception e) {
						shouldPopBack = true;
					}
				}
			};
		}
	}
}

