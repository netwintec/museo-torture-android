﻿using System;
using System.Xml;
using Android.App;
using Android.Content.PM;
using System.Collections.Generic;
using Android.Graphics;
using Android.Bluetooth;
using Android.OS;
using System.Text.RegularExpressions;
using Android.Content;
using Android.Widget;


using RestSharp;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Xamarin.Facebook;
using Xamarin.Facebook.Login;
using Gcm.Client;
using System.Json;
using Android.Gms.Maps;
using System.Linq;
using Android;
using ZXing.Mobile;

using Microsoft.AppCenter;
using Microsoft.AppCenter.Analytics;
using Microsoft.AppCenter.Crashes;
using System.Timers;
using System.Globalization;

namespace MuseoDelleTorture
{
    [Activity(ScreenOrientation = ScreenOrientation.Portrait)]
    public class MainActivity : Activity
    {

        private const string UUID = "ACFD065E-C3C0-11E3-9BBE-1A514932AC01";
        private const string beaconId = "BlueBeacon";

        Timer Timer;

        Button Login, Registrati;
        public ISharedPreferences prefs;

        public bool regionEnter = false;
        public bool FirstScan = false;
        public bool LockFirstScan = false;
        public bool TokenGood = false;
        public bool ScanBeIn = false;
        public int StanzaFind = 0;
        public bool Download = false;
        public bool ErrorDownload = false;
        public string PushToken;

        public int ScanForBattery = 0;

        int porta = 82, countSleep = 51, countBeIn = 0;

        bool notification;
        string desc, img;

        public bool SpecialBeacon = false;
        public string SpecialBeaconValue = "";

        Dictionary<int, string> BeaconMusei = new Dictionary<int, string>();
        public Dictionary<int, Stanza> StanzeDictionary = new Dictionary<int, Stanza>();
        public Dictionary<int, Stanza> StanzeDictionaryComplete = new Dictionary<int, Stanza>();
        Dictionary<int, bool> StanzeFlagDictionary = new Dictionary<int, bool>();
        private static Dictionary<string, List<int>> ListBeaconBeIn = new Dictionary<string, List<int>>();
        public static List<string> BeaconNetworks = new List<string>();

        public static MainActivity Instance { get; private set; }

        public MainActivity()
        {

            BeaconMusei.Add(102, "lucca");
            BeaconMusei.Add(104, "san_marino");
            BeaconMusei.Add(105, "siena");
            BeaconMusei.Add(106, "SanGimignano");
            BeaconMusei.Add(1, "SanGimignanoTorture");
            BeaconMusei.Add(107, "volterra");
            BeaconMusei.Add(108, "montepulciano");
        }

        private readonly string[] Permissions =
        {
            Manifest.Permission.Bluetooth,
			//Manifest.Permission.BluetoothAdmin,
			//Manifest.Permission.BluetoothPrivileged,
			//Manifest.Permission.AccessCoarseLocation,
            Manifest.Permission.AccessFineLocation,
            Manifest.Permission.Camera,
        };

        private void CheckPermissions()
        {
            bool minimumPermissionsGranted = true;

            foreach (string permission in Permissions)
            {
                if (Android.Support.V4.Content.ContextCompat.CheckSelfPermission(this, permission) != Permission.Granted) minimumPermissionsGranted = false;
            }

            // If one of the minimum permissions aren't granted, we request them from the user
            if (!minimumPermissionsGranted) Android.Support.V4.App.ActivityCompat.RequestPermissions(this, Permissions, 0);
        }


        protected override void OnCreate(Bundle bundle)
        {

            base.OnCreate(bundle);

            MainActivity.Instance = this;

            MobileBarcodeScanner.Initialize(Application);

            AppCenter.Start("99f40d1f-ca77-40b0-8c07-44328ab38f76",
                               typeof(Analytics), typeof(Crashes));

            CheckPermissions();

            /*Console.WriteLine(CrossBluetoothLE.Current.IsOn + "|" + CrossBluetoothLE.Current.IsAvailable + "|" + CrossBluetoothLE.Current.State);


				var adapter = CrossBluetoothLE.Current.Adapter;
				adapter.StartScanningForDevicesAsync();//deviceFilter: dev => dev.Name.ToLower().Contains("blue"));

				adapter.DeviceDiscovered += (s, a) => {

					Console.WriteLine("Device:"+a.Device.Name);
					};
                    */


            //******

            prefs = Application.Context.GetSharedPreferences("MuseoDelleTorture", FileCreationMode.Private);

            ActionBar.Hide();

            FacebookSdk.SdkInitialize(this.ApplicationContext);

            try
            {
                MapsInitializer.Initialize(this);
            }
            catch (Exception e)
            {
                Console.WriteLine("Have GoogleMap but then error" + e);
                //return;
            }

            //Check to ensure everything's setup right
            /*GcmClient.CheckDevice(this);
			GcmClient.CheckManifest(this);
			GcmClient.Register(this, GcmBroadcastReceiver.SENDER_IDS);*/

            SetContentView(Resource.Layout.Main);

            notification = Intent.GetBooleanExtra("Notification", false);
            desc = Intent.GetStringExtra("descrizione");
            img = Intent.GetStringExtra("image");

            Console.WriteLine("NOT:" + notification + "|" + desc + "|" + img);
            var metrics = Resources.DisplayMetrics;

            Typeface tf = Typeface.CreateFromAsset(Assets, "fonts/Myriad_Pro.ttf");

            Login = FindViewById<Button>(Resource.Id.ButtonLogin);
            Login.Typeface = tf;

            Console.WriteLine("PRE:" + Login.Text);

            Login.Text = Resources.GetString(Resource.String.LoginButtonText);

            Console.WriteLine("AFT:" + Login.Text);

            Login.Click += delegate
            {
                //var intent = new Intent (this, typeof(LoginPage));
                //StartActivity (intent);

                //** LOGIN AUTOMATICO 
                UtilityLoginManager loginManager = new UtilityLoginManager();
                List<string> result = loginManager.Login("a@a.it", "a");
                Console.WriteLine(result[0]);

                if (result[0] == "SUCCESS")
                {
                    Console.WriteLine(result[0]);
                    var prefs = MainActivity.Instance.prefs;
                    var prefEditor = prefs.Edit();
                    prefEditor.PutString("TokenN4U", result[1]);
                    prefEditor.Commit();
                    var intent = new Intent(this, typeof(HomePage));
                    StartActivity(intent);

                }

                if (result[0] == "ERROR")
                {


                    AlertDialog.Builder alert = new AlertDialog.Builder(this);
                    alert.SetTitle("Login Non Riuscito");
                    alert.SetMessage("E-Mail o Password Errata");
                    alert.SetPositiveButton("OK", (senderAlert, args) => { });
                    alert.Show();

                }

                if (result[0] == "ERRORDATA")
                {

                    AlertDialog.Builder alert = new AlertDialog.Builder(this);
                    alert.SetTitle("Errore Dati");
                    alert.SetMessage("E-Mail o Password Non Inseriti o Formato errato");
                    alert.SetPositiveButton("OK", (senderAlert, args) => { });
                    alert.Show();

                }

                //** FINE LOGIN AUTOMATICO */ 

            };

            Console.WriteLine("TOKEN:" + prefs.GetString("TokenN4U", ""));


            if (prefs.GetString("TokenN4U", "") != "")
            {

                Console.WriteLine("TOKEN:" + prefs.GetString("TokenN4U", ""));

                var client = new RestClient("http://api.netwintec.com:" + porta + "/");
                //client.Authenticator = new HttpBasicAuthenticator(username, password);

                var requestN4U = new RestRequest("Profile", Method.GET);
                requestN4U.AddHeader("content-type", "application/json");
                requestN4U.AddHeader("Net4U-Company", "museotortura");
                requestN4U.AddHeader("Net4U-Token", prefs.GetString("TokenN4U", ""));

                IRestResponse response = client.Execute(requestN4U);

                Console.WriteLine("Result:" + response.StatusCode);

                if (response.StatusCode == System.Net.HttpStatusCode.OK)
                {

                    //AUTO DOWNLOAD

                    string MuseumName = prefs.GetString("Museum", "");
                    DateTime EndVisit = Utility.FromUnixTime(prefs.GetLong("EndVisit", 0));
                    DateTime EndVisit2;
                    try
                    {
                        EndVisit2 = DateTime.ParseExact(prefs.GetString("EndVisit_v2", ""), "yyyy-MM-dd HH:mm:ss", CultureInfo.InvariantCulture);
                    }
                    catch (Exception e)
                    {
                        EndVisit2 = DateTime.Now.AddYears(-1);
                    }

                    Console.WriteLine(MuseumName + "|" + EndVisit + "|" + EndVisit.CompareTo(DateTime.Now) + "|" + EndVisit2 + "|" + EndVisit2.CompareTo(DateTime.Now));


                    if (EndVisit2.CompareTo(DateTime.Now) > 0)
                    {
                        MainActivity.Instance.GetMuseumData(MuseumName, null, null, (long)(EndVisit2 - DateTime.Now).TotalMilliseconds);
                    }

                    var intent = new Intent(this, typeof(HomePage));
                    if (notification)
                    {
                        intent.PutExtra("image", img);
                        intent.PutExtra("descrizione", desc);
                        intent.PutExtra("Notification", true);
                    }
                    StartActivity(intent);

                }
                else
                {

                    LoginManager.Instance.LogOut();

                    if (notification)
                    {
                        var intent2 = new Intent(this, typeof(NotificationPage));
                        intent2.PutExtra("image", img);
                        intent2.PutExtra("descrizione", desc);
                        StartActivity(intent2);
                    }

                }

            }

            //FindViewById<Button> (Resource.Id.linearLayout1).SetBackgroundColor(Android.Graphics.Patte;

            //String URLString = "http://shop.roccogiocattoli.eu/xml/homepage.php";
            //	XmlTextReader reader = new XmlTextReader(URLString);
            //This make an error i dont know why
            //	reader.Read();
            //reader.

        }

        public void StartTimer(long intervalMS)
        {
            Timer = new Timer();
            Timer.Start();
            Timer.Interval = intervalMS;
            Timer.Enabled = true;
            Timer.Elapsed += (object sender, System.Timers.ElapsedEventArgs e) =>
            {
                Timer.Stop();
                RunOnUiThread(() =>
                {
                    TimerEnd();
                });
                //Delete time since it will no longer be used.
                Timer.Dispose();
            };
        }

        private void TimerEnd()
        {
            try
            {
                RunOnUiThread(() =>
                {
                    FragmentHome.Self.DownloadBeacon(false);

                    if (BeaconPage.Instance != null)
                    {
                        BeaconPage.Instance.Finish();
                        HomePage.Instance.ExitBeacon = true;
                    }
                    else
                    {
                        if (HomePage.Instance.isStanza)
                        {
                            try
                            {
                                HomePage.Instance.SupportFragmentManager.PopBackStackImmediate();
                            }
                            catch (Exception ee)
                            {
                                FragmentStanzaHome.Self.shouldPopBack = true;
                            }
                        }
                    }

                });
            }
            catch (Exception e2) { }

            ErrorDownload = false;
            Download = false;
        }

        public void GetMuseumData(string museumName, EventHandler onSuccess = null, EventHandler onError = null, long timerTime = -1)
        {
            if (!LockFirstScan)
            {
                LockFirstScan = true;
                Console.WriteLine("CHIEDERE DATI PER IL MUSEO :" + museumName);

                //******** CHIAMATA SALVARE DATI E POI METTERE UNA VARIABILE BOOL A TRUE PER INIZIARE SCAN DATI
                //102 --> lucca
                //104 --> san marino

                var client = new RestClient("http://api.netwintec.com:" + porta + "/");
                //client.Authenticator = new HttpBasicAuthenticator(username, password);

                var requestN4U = new RestRequest("active-messages", Method.GET);
                requestN4U.AddHeader("content-type", "application/json");
                requestN4U.AddHeader("Net4U-Company", "museotortura");
                requestN4U.AddHeader("Net4U-Token", prefs.GetString("TokenN4U", ""));

                client.ExecuteAsync(requestN4U, (response, arg2) =>
                 {

                     RunOnUiThread(() =>
                     {
                         Console.WriteLine("Result:" + response.StatusCode + "|" + response.Content);

                         if (response.StatusCode == System.Net.HttpStatusCode.OK)
                         {

                             var lang = MainActivity.Instance.ApplicationContext.Resources.Configuration.Locale;

                             Console.WriteLine("Lang:" + lang.Country);

                             StanzeDictionary.Clear();
                             StanzeDictionaryComplete.Clear();
                             StanzeFlagDictionary.Clear();

                             try
                             {
                                 JsonUtility ju = new JsonUtility();
                                 ju.SpacchettamentoJsonBeacon(museumName, StanzeDictionaryComplete, StanzeFlagDictionary, response.Content, lang.Country);
                             }
                             catch (Exception) { }

                             if (StanzeDictionaryComplete.Count != 0)
                             {

                                 RunOnUiThread(() =>
                                {
                                    if (timerTime != -1)
                                    {
                                        StartTimer(timerTime);
                                    }
                                    else
                                    {
                                        StartTimer(60 * 60 * 1000);
                                    }

                                    FragmentHome.Self.DownloadBeacon(true);

                                    if (onSuccess != null)
                                    {
                                        onSuccess(this, new EventArgs());
                                    }

                                });
                             }
                             else
                             {

                                 ErrorDownload = true;

                                 if (onError != null)
                                 {
                                     onError(this, new EventArgs());
                                 }

                                 Console.WriteLine("DIZIONARIO VUOTOOOOOO");
                             }

                             Download = true;
                         }
                         else
                         {

                             if (onError != null)
                             {
                                 onError(this, new EventArgs());
                             }

                             Download = false;

                         }

                         LockFirstScan = false;
                     });

                 });

            }
        }

        public void SetCorrectStanzaDictionary(string id)
        {
            StanzeDictionary.Clear();
            if (id != null)
            {
                foreach (var pair in StanzeDictionaryComplete)
                {
                    if (pair.Value.museo == id)
                    {
                        if (pair.Value.museo == "SanGimignanoTorture")
                        {
                            StanzeDictionary.Add(pair.Value.stanza, pair.Value);
                        }
                        else
                        {
                            StanzeDictionary.Add(pair.Value.stanza, pair.Value);
                        }

                    }
                }
            }
            else
            {

                StanzeDictionary = new Dictionary<int, Stanza>(StanzeDictionaryComplete);
            }
        }






    }
}


