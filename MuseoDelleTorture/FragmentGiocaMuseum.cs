﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Net;
using System.Threading.Tasks;
using System.Json;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Util;
using Android.Views;
using Android.Widget;
using Android.Graphics;

using Xamarin.Facebook;
using Xamarin.Facebook.Share.Widget;
using Xamarin.Facebook.Share.Model;
using Xamarin.Facebook.Share;
using Xamarin.Facebook.Login;

using Fragment = Android.Support.V4.App.Fragment;
using FragmentManager = Android.Support.V4.App.FragmentManager;
using RestSharp;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Square.Picasso;

namespace MuseoDelleTorture
{
	public class FragmentGiocaMuseum : Fragment
	{
		GiocaResult ListResult;
		public List<GiocaElement> ListGioca = new List<GiocaElement> ();
		public int i=0;
		int selected=0;
		int porta = 82;

		public TextView Domanda, Risposta1, Risposta2, Risposta3; 
		public ImageView Check1,Check2,Check3;
		public Button ConfermaButton;

		ImageView resultImage;

		public bool somethingCheck = false;

		WebClient webClient;

		Button Condividi;

		ISharedPreferences prefs;

		bool canPresentShareDialog;
		PendingAction pendingAction = PendingAction.NONE;
		ShareDialog shareDialog;
		FacebookCallback<SharerResult> shareCallback;
		ICallbackManager callbackManager;
		//static readonly string [] PERMISSIONS = new [] { "publish_actions" };

		enum PendingAction
		{
			NONE,
			POST_STATUS_UPDATE
		}

		public override void OnCreate (Bundle savedInstanceState)
		{
			base.OnCreate (savedInstanceState);


			// Create your fragment here
		}
			

		public override View OnCreateView (LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
		{
			// Use this to return your custom view for this Fragment
			View view = inflater.Inflate(Resource.Layout.FragmentGiocaMuseum,container,false);

			FacebookSdk.SdkInitialize (Context);

			callbackManager = CallbackManagerFactory.Create ();


			ListGioca = FragmentGiocaHome.Self.ListGioca [FragmentGiocaHome.Self.selected];
			ListResult = FragmentGiocaHome.Self.ListResult [FragmentGiocaHome.Self.selected];

			Typeface tf = Typeface.CreateFromAsset(Activity.Assets,"fonts/Myriad_Pro.ttf");
			Typeface tfB = Typeface.CreateFromAsset(Activity.Assets,"fonts/Myriad_Pro_Bold.ttf");
			Typeface tfG = Typeface.CreateFromAsset (Activity.Assets, "fonts/daunpenh.ttf");

			prefs = MainActivity.Instance.prefs;

			if (ListResult.Risultato != "") {

				view.FindViewById<TextView> (Resource.Id.Result).Typeface = tfG;
				view.FindViewById<TextView> (Resource.Id.youAre).Typeface = tf;

				view.FindViewById<TextView> (Resource.Id.RisultatoNome).Typeface = tfB;
				view.FindViewById<TextView> (Resource.Id.RisultatoDescr).Typeface = tf;

				view.FindViewById<TextView> (Resource.Id.RisultatoNome).Text = ListResult.Risultato;
				view.FindViewById<TextView> (Resource.Id.RisultatoDescr).Text = ListResult.Descrizione;

				resultImage = view.FindViewById<ImageView> (Resource.Id.RisultatoImage);
				//caricaImmagineAsync (ListResult.ImageUrl, resultImage);

                try
                {
                    Picasso.With(MainActivity.Instance.ApplicationContext)
                           .Load(ListResult.ImageUrl)
                           .Placeholder(Resource.Drawable.placeholder)
                           .Into(resultImage);
                }
                catch (Exception e)
                {
                    Picasso.With(MainActivity.Instance.ApplicationContext)
                           .Load(Resource.Drawable.placeholder)
                           .Placeholder(Resource.Drawable.placeholder)
                           .Into(resultImage);
                }

				view.FindViewById<LinearLayout> (Resource.Id.WaitView).Visibility = ViewStates.Gone;

				shareCallback = new FacebookCallback<SharerResult> {
					HandleSuccess = shareResult => {
						Console.WriteLine ("HelloFacebook: Success!");

						/*if (shareResult.PostId != null) {
							var title = Parent.GetString (Resource.String.error);
							var id = shareResult.PostId;
							var alertMsg = Parent.GetString (Resource.String.successfully_posted_post, id);

							ShowAlert (title, alertMsg);
						}*/
					},
					HandleCancel = () => {
						Console.WriteLine ("HelloFacebook: Canceled");
					},
					HandleError = shareError => {
						Console.WriteLine ("HelloFacebook: Error: {0}", shareError);

						/*var title = Parent.GetString (Resource.String.error);
						var alertMsg = shareError.Message;

						ShowAlert (title, alertMsg);*/
					}
				};

				shareDialog = new ShareDialog (this);
				shareDialog.RegisterCallback (callbackManager, shareCallback);

				canPresentShareDialog = ShareDialog.CanShow (Java.Lang.Class.FromType (typeof(ShareLinkContent)));

				/*if (prefs.GetString ("TokenFB", "") == "") {
					Condividi = view.FindViewById<Button> (Resource.Id.CondividiButton);
					Condividi.Visibility = ViewStates.Gone;
				}
				else{*/

				//LoginManager.Instance.LogOut();
					Condividi = view.FindViewById<Button> (Resource.Id.CondividiButton);
					Condividi.Typeface = tfG;
					Condividi.Click += delegate {

						PerformPublish (PendingAction.POST_STATUS_UPDATE, canPresentShareDialog);

					};
				//}

			} else {

				view.FindViewById<LinearLayout> (Resource.Id.RisultatoView).Visibility = ViewStates.Gone;
				view.FindViewById<LinearLayout> (Resource.Id.WaitView).Visibility = ViewStates.Gone;

				view.FindViewById<TextView> (Resource.Id.titolo).Typeface = tfG;
				view.FindViewById<TextView> (Resource.Id.Numero).Typeface = tfG;

				view.FindViewById<TextView> (Resource.Id.Numero).Text = Context.Resources.GetString (Resource.String.Gioca_Domanda) + " " + (i + 1) + "/" + ListGioca.Count;

				Domanda = view.FindViewById<TextView> (Resource.Id.Domanda);
				Risposta1 = view.FindViewById<TextView> (Resource.Id.Risposta1);
				Risposta2 = view.FindViewById<TextView> (Resource.Id.Risposta2);
				Risposta3 = view.FindViewById<TextView> (Resource.Id.Risposta3);

				Check1 = view.FindViewById<ImageView> (Resource.Id.Check1);
				Check2 = view.FindViewById<ImageView> (Resource.Id.Check2);
				Check3 = view.FindViewById<ImageView> (Resource.Id.Check3);

				ConfermaButton = view.FindViewById<Button> (Resource.Id.ConfermaButton);

				Domanda.Typeface = tf;
				Risposta1.Typeface = tf;
				Risposta2.Typeface = tf;
				Risposta3.Typeface = tf;

				Domanda.Text = ListGioca [i].Domanda;
				Risposta1.Text = ListGioca [i].Risposta1;
				Risposta2.Text = ListGioca [i].Risposta2;
				Risposta3.Text = ListGioca [i].Risposta3;

				ConfermaButton.Typeface = tfG;

				Check1.Click += delegate {

					if (selected != 1) {
					
						selected = 1; 
						somethingCheck = true;
						CheckAnswer (selected);

					}
				};

				Check2.Click += delegate {

					if (selected != 2) {

						selected = 2; 
						somethingCheck = true;
						CheckAnswer (selected);

					}
				};

				Check3.Click += delegate {

					if (selected != 3) {

						selected = 3; 
						somethingCheck = true;
						CheckAnswer (selected);

					}
				};

				ConfermaButton.Click += delegate {

					if (somethingCheck) {

						i++;
						if (i < ListGioca.Count) {
							view.FindViewById<TextView> (Resource.Id.Numero).Text = Context.Resources.GetString (Resource.String.Gioca_Domanda) + " " + (i + 1) + "/" + ListGioca.Count;
							Domanda.Text = ListGioca [i].Domanda;
							Risposta1.Text = ListGioca [i].Risposta1;
							Risposta2.Text = ListGioca [i].Risposta2;
							Risposta3.Text = ListGioca [i].Risposta3;
							somethingCheck = false;
							selected = 0; 
							CheckAnswer (selected);
						}
						if (i == ListGioca.Count) {
							view.FindViewById<TextView> (Resource.Id.WaitText).Typeface = tf;
							view.FindViewById<LinearLayout> (Resource.Id.WaitView).Visibility = ViewStates.Visible;
							Console.WriteLine("1");
							var client = new RestClient("http://api.netwintec.com:" + porta + "/");
							//client.Authenticator = new HttpBasicAuthenticator(username, password);
							Console.WriteLine("2");
							var requestN4U = new RestRequest("play" , Method.POST);
							requestN4U.AddHeader("content-type", "application/json");
							requestN4U.AddHeader("Net4U-Company", "museotortura");
							requestN4U.AddHeader("Net4U-Token", MainActivity.Instance.prefs.GetString ("TokenN4U", ""));
							requestN4U.Timeout = 60000;
							Console.WriteLine("3");
							JObject oJsonObject = new JObject();
							Console.WriteLine("4:"+ListResult.Luogo);
							oJsonObject.Add("museo", ListResult.Luogo);
							Console.WriteLine("5");
							requestN4U.AddParameter("application/json; charset=utf-8", oJsonObject, ParameterType.RequestBody);
							Console.WriteLine("6");
							//Console.WriteLine(requestN4U.Parameters[0].Value+"|"+requestN4U.Parameters[0].Type+"|"+requestN4U.Parameters[0].Name);

							client.ExecuteAsync(requestN4U,(s,e) => {

								//var s = client.Execute(requestN4U);
								Console.WriteLine("GIOCA2:"+s.StatusCode+"|"+s.Content);
								//if(status
								//if(s.StatusCode != 0){
									if(s.StatusCode == HttpStatusCode.OK){
										Activity.RunOnUiThread(() =>{
										
											var lang = Context.Resources.Configuration.Locale;
											JsonValue json = JsonValue.Parse (s.Content);

											JsonValue data = json ["elementi"];

											foreach (JsonValue dataItem in data) {

												string museo = dataItem["museo"];
												if(museo == ListResult.Luogo){
													Console.WriteLine(1);
													JsonValue data3 = dataItem ["risultato"];
												Console.WriteLine(2);
													ListResult.ImageUrl = data3["result_Image"];
												Console.WriteLine(3);
													if (lang.Country.CompareTo ("IT") == 0) {
														ListResult.Risultato = data3["result_it"];
														ListResult.Descrizione = data3["result_descrizione_it"];
													}else{
														ListResult.Risultato = data3["result_en"];
														ListResult.Descrizione = data3["result_descrizione_en"];
													}
												Console.WriteLine(4);
												}

											}
											
											Console.WriteLine(ListResult.Risultato+"|"+ListResult.Descrizione+"|"+ListResult.ImageUrl);


											view.FindViewById<LinearLayout> (Resource.Id.RisultatoView).Visibility = ViewStates.Visible;
											view.FindViewById<LinearLayout> (Resource.Id.WaitView).Visibility = ViewStates.Gone;

											view.FindViewById<TextView> (Resource.Id.Result).Typeface = tfG;
											view.FindViewById<TextView> (Resource.Id.youAre).Typeface = tf;

											view.FindViewById<TextView> (Resource.Id.RisultatoNome).Typeface = tfB;
											view.FindViewById<TextView> (Resource.Id.RisultatoDescr).Typeface = tf;

											view.FindViewById<TextView> (Resource.Id.RisultatoNome).Text = ListResult.Risultato;
											view.FindViewById<TextView> (Resource.Id.RisultatoDescr).Text = ListResult.Descrizione;

											resultImage = view.FindViewById<ImageView> (Resource.Id.RisultatoImage);
											//caricaImmagineAsync (ListResult.ImageUrl, resultImage);

                                            try
                                            {
                                                Picasso.With(MainActivity.Instance.ApplicationContext)
                                                       .Load(ListResult.ImageUrl)
                                                       .Placeholder(Resource.Drawable.placeholder)
                                                       .Into(resultImage);
                                            }
                                            catch (Exception ee)
                                            {
                                                Picasso.With(MainActivity.Instance.ApplicationContext)
                                                       .Load(Resource.Drawable.placeholder)
                                                       .Placeholder(Resource.Drawable.placeholder)
                                                       .Into(resultImage);
                                            }

											view.FindViewById<LinearLayout> (Resource.Id.WaitView).Visibility = ViewStates.Gone;

											shareCallback = new FacebookCallback<SharerResult> {
												HandleSuccess = shareResult => {
													Console.WriteLine ("HelloFacebook: Success!");

												},
												HandleCancel = () => {
													Console.WriteLine ("HelloFacebook: Canceled");
												},
												HandleError = shareError => {
													Console.WriteLine ("HelloFacebook: Error: {0}", shareError);

												}
											};

											shareDialog = new ShareDialog (this);
											shareDialog.RegisterCallback (callbackManager, shareCallback);

											canPresentShareDialog = ShareDialog.CanShow (Java.Lang.Class.FromType (typeof(ShareLinkContent)));

											/*if (prefs.GetString ("TokenFB", "") == "") {
												Condividi = view.FindViewById<Button> (Resource.Id.CondividiButton);
												Condividi.Visibility = ViewStates.Gone;
											}
											else{*/
												Condividi = view.FindViewById<Button> (Resource.Id.CondividiButton);
												Condividi.Typeface = tfG;
												Condividi.Click += delegate {

													PerformPublish (PendingAction.POST_STATUS_UPDATE, canPresentShareDialog);

												};
											//}
										});
									}else{
										Activity.RunOnUiThread(() =>{
											i--;
											view.FindViewById<LinearLayout> (Resource.Id.RisultatoView).Visibility = ViewStates.Gone;
											view.FindViewById<LinearLayout> (Resource.Id.WaitView).Visibility = ViewStates.Gone;
											Toast.MakeText (Context, "Errore Riprovare", ToastLength.Long).Show();
										});
									}
								//}

							});

						}
					}

				};
			}

			return view;
			//return base.OnCreateView (inflater, container, savedInstanceState);
		}

		public void CheckAnswer(int indice){

			if (indice == 0) {
				Check1.SetImageResource (Resource.Drawable.no_check);
				Check2.SetImageResource (Resource.Drawable.no_check);
				Check3.SetImageResource (Resource.Drawable.no_check);
			}

			if (indice == 1) {
				Check1.SetImageResource (Resource.Drawable.check);
				Check2.SetImageResource (Resource.Drawable.no_check);
				Check3.SetImageResource (Resource.Drawable.no_check);
			}

			if (indice == 2) {
				Check1.SetImageResource (Resource.Drawable.no_check);
				Check2.SetImageResource (Resource.Drawable.check);
				Check3.SetImageResource (Resource.Drawable.no_check);
			}

			if (indice == 3) {
				Check1.SetImageResource (Resource.Drawable.no_check);
				Check2.SetImageResource (Resource.Drawable.no_check);
				Check3.SetImageResource (Resource.Drawable.check);
			}

		}

		async void caricaImmagineAsync(String uri,ImageView immagine_view){
			webClient = new WebClient ();
			byte[] bytes = null;
			try{
				bytes = await webClient.DownloadDataTaskAsync(uri);
			}
			catch(TaskCanceledException){
				Console.WriteLine ("Task Canceled!**************************************");
				return;
			}
			catch(Exception e){
				Console.WriteLine (e.ToString());
				return;
			}

			//if (immagine_view.IsInLayout) {

			Bitmap immagine = await BitmapFactory.DecodeByteArrayAsync (bytes, 0, bytes.Length);

			immagine_view.SetImageBitmap (immagine);

			Console.WriteLine ("Immagine caricata!");
			/*} else {
				Console.WriteLine ("Immagine contatto non più esistente per cui non la carico!");
			}*/
		}

		private void HandlePendingAction ()
		{
			PendingAction previouslyPendingAction = pendingAction;
			// These actions may re-set pendingAction if they are still pending, but we assume they
			// will succeed.
			pendingAction = PendingAction.NONE;

			switch (previouslyPendingAction) {
			case PendingAction.POST_STATUS_UPDATE:
				PostStatusUpdate ();
				break;
			}
		}


		void PostStatusUpdate ()
		{
			var profile = Profile.CurrentProfile;

			var linkContent = new ShareLinkContent.Builder ()
				.SetContentTitle (Context.Resources.GetString (Resource.String.Gioca_CondividiTitolo) + " " + ListResult.Risultato)
				.SetContentDescription (ListResult.Descrizione)
				.SetImageUrl (Android.Net.Uri.Parse (ListResult.ImageUrl))
				.SetContentUrl (Android.Net.Uri.Parse ("http://www.torturemuseum.it/"))

				.JavaCast<ShareLinkContent.Builder> ()
				.Build ();

			if (canPresentShareDialog)
				shareDialog.Show (linkContent);
			else if (profile != null && HasPublishPermission ())
				ShareApi.Share (linkContent, shareCallback);
			else
				pendingAction = PendingAction.POST_STATUS_UPDATE;

		}

		bool HasPublishPermission ()
		{
			var accessToken = AccessToken.CurrentAccessToken;
			return accessToken != null;//&& accessToken.Permissions.Contains ("publish_actions");
		}

		void PerformPublish (PendingAction action, bool allowNoToken)
		{
			Console.WriteLine ("1");

			var accessToken = AccessToken.CurrentAccessToken;
			Console.WriteLine ("2");
			if (accessToken != null) {
				pendingAction = action;
				Console.WriteLine (HasPublishPermission());
				if (HasPublishPermission ()) {
					HandlePendingAction ();
					return;
				} else {
					LoginManager.Instance.LogInWithPublishPermissions(this, null);//PERMISSIONS);
					return;
				}
			}
			Console.WriteLine ("3");
			if (allowNoToken) {
				pendingAction = action;
				HandlePendingAction ();
			}
		}

	}
}

