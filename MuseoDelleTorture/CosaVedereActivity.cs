﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Json;
using System.IO;
using System.Net;

using Android.Content;
using Android.App;
using Android.Content.PM;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Locations;
using Android.Graphics;
using Android.Views.Animations;
using Android.Text;

using Android.Support.V4.View;
using Android.Support.V4.Widget;
using Fragment = Android.Support.V4.App.Fragment;
using FragmentManager = Android.Support.V4.App.FragmentManager;
using Android.Gms.Maps;
using Android.Gms.Maps.Model;
using RestSharp;
using Android;

namespace MuseoDelleTorture
{
	[Activity(Label = "CosaVedereActivity", Theme = "@style/AppTheme", ScreenOrientation = ScreenOrientation.Portrait)]
	public class CosaVedereActivity : BaseActivity, ILocationListener,IOnMapReadyCallback
	{
		private DrawerLayout mDrawerLayout;
		private ListView mDrawerList;

		//List<DrawerItem> dataList;
		DrawerAdapterMaps adapter;
		LocationManager lcm;

		public ImageView menuIcon;
		public ImageView backIcon;
		bool PositionFound = false;

		bool permOK = false;
		bool locationOK = false;

		MapFragment mapFrag;
		GoogleMap map;
		LatLng TRUEPOS;

		public Dictionary<string, MarkerInfo> MarkerDictionary = new Dictionary<string, MarkerInfo>();
		Dictionary<int, MarkerInfo> MarkerDictionaryComplete = new Dictionary<int, MarkerInfo>();
		List<Marker> MarkerList = new List<Marker>();

		int porta = 82;
		string maps = "";

		LatLng mypos;
		LatLng LuccaPos = new LatLng(43.843060, 10.503589);
		LatLng SienaPos = new LatLng(43.318579, 11.330521);
		LatLng SanMarinoPos = new LatLng(43.933983, 12.446931);
		//LatLng VolterraPos = new LatLng(43.401545, 10.860111);
		LatLng MontePulcianoPos = new LatLng(43.097799, 11.785759);
		LatLng SanGiminianoPos = new LatLng(43.467471, 11.043171);


		private readonly string[] Permissions =
		{
			//Manifest.Permission.AccessCoarseLocation,
			Manifest.Permission.AccessFineLocation
		};

		private void CheckPermissions()
		{
			bool minimumPermissionsGranted = true;
			Console.WriteLine(1);
			foreach (string permission in Permissions)
			{
				if (Android.Support.V4.Content.ContextCompat.CheckSelfPermission(this, permission) != Permission.Granted) minimumPermissionsGranted = false;
			}
			Console.WriteLine("2:"+minimumPermissionsGranted);
			// If one of the minimum permissions aren't granted, we request them from the user
			if (!minimumPermissionsGranted) Android.Support.V4.App.ActivityCompat.RequestPermissions(this, Permissions, 0);
			else
			{
				RunOnUiThread(() =>
				{
					permOK = true;
					lcm = GetSystemService(Context.LocationService) as LocationManager;

					string provider = LocationManager.GpsProvider;

					if (lcm.IsProviderEnabled(provider))
					{
						lcm.RequestLocationUpdates(provider, 1, 0.001f, this);
						Console.WriteLine("1");
					}
					else
					{
						AlertDialog.Builder alert = new AlertDialog.Builder(this);
						alert.SetTitle("Errore");
						alert.SetMessage(provider + " non è disponibile.\nControllare se il cellulare ha la localizzazione attiva?");
						alert.SetPositiveButton("Ok", (senderAlert, args) =>
						{

						});
						RunOnUiThread(() =>
						{
							alert.Show();
						});
					}
				
				});
			
			}
			Console.WriteLine(3);
		}
	
		public override void OnRequestPermissionsResult(int requestCode, string[] permissions, Permission[] grantResults)
		{
			
			if (requestCode == 0) {

				if (grantResults.Length > 0
					&& grantResults[0] == Permission.Granted)
				{

					// permission was granted, yay! Do the
					// contacts-related task you need to do.true
					RunOnUiThread(() =>
					{
						permOK = true;
						lcm = GetSystemService(Context.LocationService) as LocationManager;

						string provider = LocationManager.GpsProvider;

						if (lcm.IsProviderEnabled(provider))
						{
							lcm.RequestLocationUpdates(provider, 1, 0.001f, this);
							Console.WriteLine("1");
						}
						else
						{
							AlertDialog.Builder alert = new AlertDialog.Builder(this);
							alert.SetTitle("Errore");
							alert.SetMessage(provider + " non è disponibile.\nControllare se il cellulare ha la localizzazione attiva?");
							alert.SetPositiveButton("Ok", (senderAlert, args) =>
							{

							});
							RunOnUiThread(() =>
							{
								alert.Show();
							});
						}

					});
				
				
	            } else {

					Finish();

					if (lcm != null) { 
						lcm.RemoveUpdates(this);
					}
	            }
	            return;
			
			}
		}

		protected override void OnCreate (Bundle bundle)
		{
			base.OnCreate (bundle);

			CosaVedereActivity.Self = this;

			new System.Threading.Thread(new System.Threading.ThreadStart(() =>
			{
				CheckPermissions();

			})).Start();
			//lcm = GetSystemService (Context.LocationService) as LocationManager;

			Typeface tfG = Typeface.CreateFromAsset (Assets, "fonts/daunpenh.ttf");

			FindViewById<TextView> (Resource.Id.textView1).Typeface = tfG;

			Animation rotateAboutCenterAnimation = AnimationUtils.LoadAnimation(this,Resource.Animation.RotateAnimation);
			FindViewById<ImageView> (Resource.Id.rotateImage).StartAnimation(rotateAboutCenterAnimation);

			var client = new RestClient("http://api.netwintec.com:" + porta + "/");
			//client.Authenticator = new HttpBasicAuthenticator(username, password);

			var requestN4U = new RestRequest("play/maps", Method.GET);
			requestN4U.AddHeader("content-type", "application/json");
			requestN4U.AddHeader("Net4U-Company", "museotortura");
			requestN4U.AddHeader("Net4U-Token", MainActivity.Instance.prefs.GetString("TokenN4U", ""));

			client.ExecuteAsync(requestN4U, (s, e) => {

				var response = s;

				Console.WriteLine("Result:" + response.StatusCode + "|" + response.Content);

				if (response.StatusCode == System.Net.HttpStatusCode.OK)
				{

					maps = response.Content;
					if (PositionFound && permOK)
					{
						FindViewById<RelativeLayout>(Resource.Id.BlackScreen).Visibility = ViewStates.Gone;
		                StartMap();
						lcm.RemoveUpdates(this);
					}
				}
				else
				{

					this.Finish();
					if (lcm != null) { 
						lcm.RemoveUpdates(this);
					}
				}
			});

		}

		protected override void OnResume ()
		{
			base.OnResume ();
			Console.WriteLine ("Resume");

		}

		public void OnLocationChanged (Android.Locations.Location location)
		{
			mypos = new LatLng (location.Latitude,location.Longitude);
			PositionFound = true;
			this.RunOnUiThread(() => Toast.MakeText(this, "Gps pronto", ToastLength.Short).Show());

			if (maps != "" && permOK)
			{
				FindViewById<RelativeLayout>(Resource.Id.BlackScreen).Visibility = ViewStates.Gone;
				StartMap();
				lcm.RemoveUpdates(this);
			}
		}

		public void OnProviderDisabled (string provider)
		{

		}
		public void OnProviderEnabled (string provider)
		{

		}
		public void OnStatusChanged (string provider, Availability status, Bundle extras)
		{

		}

		public void StartMap(){
			setActionBarIcon(Resource.Drawable.lente_ingrandimento_menu,Resource.Drawable.back_icon);

			mDrawerLayout =  FindViewById<DrawerLayout>(Resource.Id.drawer_layout);
			mDrawerList =  FindViewById<ListView>(Resource.Id.left_drawer);

			var lang = Resources.Configuration.Locale;
			string position = "";
			LatLng truePos = null;

			List <LatLng> listPos = new List<LatLng> ();
			listPos.Add (LuccaPos);
			listPos.Add (SienaPos);
			listPos.Add (SanMarinoPos);
			listPos.Add (MontePulcianoPos);
			listPos.Add (SanGiminianoPos);
			//listPos.Add (VolterraPos);

			double distance = Double.MaxValue;

			Android.Locations.Location myposApp = new Android.Locations.Location (LocationManager.GpsProvider);
			myposApp.Latitude = mypos.Latitude;
			myposApp.Longitude = mypos.Longitude;

			Console.WriteLine ("Mypos:" + mypos.Latitude + "," + mypos.Longitude);
			for (int ii = 0; ii < listPos.Count; ii++) {

				Android.Locations.Location app = new Android.Locations.Location (LocationManager.GpsProvider);
				app.Latitude = listPos [ii].Latitude;
				app.Longitude = listPos [ii].Longitude;

				double Finalvalue = myposApp.DistanceTo (app);
				Console.WriteLine (ii + "|" + Finalvalue);
					
				/*double diffLat = mypos.Latitude - listPos [ii].Latitude;
				double diffLatPow = Math.Pow (diffLat,2);
				double diffLon = mypos.Longitude - listPos [ii].Longitude;
				double diffLonPow = Math.Pow (diffLon,2);
				double Finalvalue = Math.Sqrt (diffLatPow+diffLonPow);*/

				//Console.WriteLine (ii + "|" + diffLat+ "|" + diffLatPow+ "|" + diffLon+ "|" + diffLonPow+ "|" + Finalvalue);

				if(Finalvalue<distance){

					distance = Finalvalue;
					switch(ii){
					case 0:
						position = "lucca";
						break;
					case 1:
						position="siena";
						break;
					case 2:
						position="san_marino";
						break;
					case 3:
						position="montepulciano";
						break;
					case 4:
						position="san_giminiano";
						break;
					case 5:
						position="volterra";
						break;

					}
					truePos = listPos [ii];
					Console.WriteLine (position);
				}
			}   

			Console.WriteLine ("position:"+position+"|distance:"+distance);

			JsonUtility ju = new JsonUtility ();
			//StreamReader strm = new StreamReader (Assets.Open ("Marker.json"),Encoding.UTF8);
			ju.SpacchettamentoJsonMarker (position, MarkerDictionary, MarkerDictionaryComplete, maps,lang.Country);

			Console.WriteLine (MarkerDictionary.Count + "    " + MarkerDictionaryComplete.Count);

			TRUEPOS = truePos;

			//SpacchettamentoJson("lucca");

			//mDrawerList.Adapter = new ArrayAdapter<string> (this, Resource.Layout.item_menu, Section);
			adapter = new DrawerAdapterMaps(this,MarkerDictionaryComplete);

			mDrawerList.SetAdapter(adapter);


			menuIcon = (ImageView) getToolbar().FindViewById(Resource.Id.home_icon);
			backIcon = (ImageView) getToolbar().FindViewById(Resource.Id.back_icon);

			menuIcon.Click += delegate {
				if (mDrawerLayout.IsDrawerOpen(GravityCompat.Start)) {
					mDrawerLayout.CloseDrawer(GravityCompat.Start);
				}else{
					mDrawerLayout.OpenDrawer(GravityCompat.Start);
				}
			};

			backIcon.Click += delegate {
				Console.WriteLine("INFO"+ (SupportFragmentManager.BackStackEntryCount).ToString());
				this.Finish();
				if (lcm != null) { 
						lcm.RemoveUpdates(this);
					}
			};

			mDrawerList.ItemClick+=DrawerListItemClick;

			Console.WriteLine (MarkerDictionary.ElementAt (0).Value.titolo);

			mapFrag =  FragmentManager.FindFragmentById<MapFragment>(Resource.Id.map);
			mapFrag.GetMapAsync((Android.Gms.Maps.IOnMapReadyCallback)(this));

		}

		public static CosaVedereActivity Self {get;private set;}

		protected override int getLayoutResource() {
			return Resource.Layout.CosaVedereLayout;
		}

		public override bool OnOptionsItemSelected (IMenuItem item)
		{
			switch (item.ItemId) {
			case Resource.Id.home:
				mDrawerLayout.OpenDrawer(GravityCompat.Start);
				return true;
			}

			return base.OnOptionsItemSelected (item);
		}

		public override void OnBackPressed ()
		{
			Console.WriteLine("INFO"+ (SupportFragmentManager.BackStackEntryCount).ToString());
			if (PositionFound) {
				if (mDrawerLayout.IsDrawerOpen (GravityCompat.Start)) {
					mDrawerLayout.CloseDrawer (GravityCompat.Start);
				} else {
					base.OnBackPressed ();
				}
			} else {
				Finish ();
				if (lcm != null) { 
						lcm.RemoveUpdates(this);
					}
			}
		}


		public void DrawerListItemClick ( object sender , AdapterView.ItemClickEventArgs item ){
			selectItem(item.Position);
		}

		public void selectItem(int position) {

			MarkerInfo mi = MarkerDictionaryComplete.ElementAt(position).Value;
			if (mi.isHeader == false) {
				LatLng location = new LatLng ((double)mi.lat,(double)mi.lon);
				CameraPosition.Builder builder = CameraPosition.InvokeBuilder ();
				builder.Target (location);
				builder.Zoom (16);
				CameraPosition cameraPosition = builder.Build ();
				CameraUpdate cameraUpdate = CameraUpdateFactory.NewCameraPosition (cameraPosition);
				map.MoveCamera (cameraUpdate);

				foreach (Marker mark in MarkerList) {
					//Console.WriteLine (mark.Title + "|" + (mark.Title == mi.titolo));
					if (mark.Title == mi.titolo) {
						mark.ShowInfoWindow();
					} 
				}
			}

			mDrawerList.SetItemChecked(position,false);
			mDrawerLayout.CloseDrawer(mDrawerList);
		}



		private void MapOnInfoWindowClick (object sender, GoogleMap.InfoWindowClickEventArgs e)
		{
			Marker myMarker = e.Marker;
			Console.WriteLine ("\""+myMarker.Id+"\"   "+myMarker.Id.Equals("m1"));
			MarkerInfo miCl = MarkerDictionary [myMarker.Id]; 
			if (miCl.nome == "PARCHEGGI") { 
				Console.WriteLine("geo:"+miCl.lat+","+miCl.lon);
				string lat = miCl.lat.ToString().Replace(",",".");
				string lon = miCl.lon.ToString().Replace(",",".");
				Console.WriteLine("geo:"+lat+","+lon+"?q="+WebUtility.UrlEncode(miCl.via));
				var geoUri = Android.Net.Uri.Parse ("geo:"+miCl.lat+","+miCl.lon+"?q="+WebUtility.UrlEncode(miCl.via));
				var mapIntent = new Intent (Intent.ActionView, geoUri);
				StartActivity (mapIntent);
			} else {
				var intent = new Intent (this, typeof(CosaVedereInfoActivity));
				intent.PutExtra ("key", myMarker.Id);
				StartActivity (intent);
			}
		}

		public void OnMapReady(GoogleMap googleMap)
		{
			LatLng location = TRUEPOS;
			CameraPosition.Builder builder = CameraPosition.InvokeBuilder();
			builder.Target(location);
			builder.Zoom(14);
			CameraPosition cameraPosition = builder.Build();
			CameraUpdate cameraUpdate = CameraUpdateFactory.NewCameraPosition(cameraPosition);
		
			map = googleMap;

			if (map != null)
			{
				map.MapType = GoogleMap.MapTypeNormal;
				map.UiSettings.ZoomControlsEnabled = true;
				map.UiSettings.CompassEnabled = true;
				map.UiSettings.MyLocationButtonEnabled = false;
				map.MyLocationEnabled = false;
				map.UiSettings.MapToolbarEnabled = false;
				map.MoveCamera(cameraUpdate);
				//BitmapDescriptor descriptor = BitmapDescriptorFactory.FromResource (Resource.Id.icon);

				for (int c = 0; c < MarkerDictionary.Count; c++)
				{
					//BitmapDescriptor descriptor = BitmapDescriptorFactory.FromResource (Resource.Id.icon);
					MarkerInfo mi = MarkerDictionary.ElementAt(c).Value;
					MarkerOptions markerOpt = new MarkerOptions();
					markerOpt.SetPosition(new LatLng((double)mi.lat, (double)mi.lon));
					markerOpt.SetTitle(mi.titolo);
					var lbyte = System.Text.Encoding.UTF8.GetBytes(mi.descrizione);
					var lResult = System.Text.Encoding.UTF8.GetString(lbyte);
					markerOpt.SetSnippet(mi.descrizione);
					//markerOpt.SetIcon (BitmapDescriptorFactory.FromResource (Resource.Id.icon));
					if (mi.nome == "PARCHEGGI")
						markerOpt.SetIcon(BitmapDescriptorFactory.FromResource(Resource.Drawable.marker_mappaP));
					else
						markerOpt.SetIcon(BitmapDescriptorFactory.FromResource(Resource.Drawable.marker_mappa));
					var m = map.AddMarker(markerOpt);
					MarkerList.Add(m);

				}

				map.SetInfoWindowAdapter(new CustomMarkerPopupAdapter(LayoutInflater));
				//markerOpt.SetIcon (BitmapDescriptorFactory.FromResource (Resource.Drawable.marker_san_gimignano));
				map.InfoWindowClick += MapOnInfoWindowClick;
			}
		
		}
	}

	public class CustomMarkerPopupAdapter : Java.Lang.Object,GoogleMap.IInfoWindowAdapter
	{
		private LayoutInflater _layoutInflater = null;

		public IntPtr IJHandle { get { return IntPtr.Zero; } }

		public CustomMarkerPopupAdapter(LayoutInflater inflater)
		{
			//This constructor does hit a breakpoint and executes
			_layoutInflater = inflater;
		}

		public View GetInfoWindow(Marker marker)
		{
			//This never executes or hits a break point
			return null;
		}

		public View GetInfoContents(Marker marker)
		{

			Typeface tf = Typeface.CreateFromAsset (MainActivity.Instance.Assets, "fonts/Myriad_Pro.ttf");
			Typeface tfB = Typeface.CreateFromAsset (MainActivity.Instance.Assets, "fonts/Myriad_Pro_Bold.ttf");
			//This never executes or hits a break point
			var customPopup = _layoutInflater.Inflate(Resource.Layout.CustomMapInfoWindows, null);

			var titleTextView = customPopup.FindViewById<TextView>(Resource.Id.CustomMarkerTitle);
			if (titleTextView != null)
			{
				titleTextView.Typeface = tfB;
				titleTextView.Text = marker.Title;
			}

			var snippetTextView = customPopup.FindViewById<TextView>(Resource.Id.CustomMarkerDescr);
			if (snippetTextView != null)
			{
				snippetTextView.Typeface = tf;
				snippetTextView.TextFormatted = Html.FromHtml(marker.Snippet);
			}

			return customPopup;
		}

		public void Dispose()
		{

		}
	}

}

