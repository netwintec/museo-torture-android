﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content.PM;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Support.V4.View;
using Android.Support.V4.Widget;
using Android.Views;
using Fragment = Android.Support.V4.App.Fragment;
using FragmentManager = Android.Support.V4.App.FragmentManager;
using Xamarin.Facebook;
using Xamarin.Facebook.Login;
using Xamarin.Facebook.Login.Widget;


namespace MuseoDelleTorture
{
	[Activity (Label = "HomePage",Theme = "@style/AppTheme",ScreenOrientation = ScreenOrientation.Portrait)]			
	public class HomePage : BaseActivity 
	{

		private DrawerLayout mDrawerLayout;
		private ListView mDrawerList;

		//List<DrawerItem> dataList;
		DrawerAdapter adapter;

		public ImageView menuIcon;
		public ImageView backIcon;

		public FragmentManager.IOnBackStackChangedListener backStackListener;

		ISharedPreferences prefs;

		protected override void OnCreate (Bundle bundle)
		{
			base.OnCreate (bundle);

			setActionBarIcon(Resource.Drawable.menu_icon,Resource.Drawable.back_icon);

			//Set default settings
			//PreferenceManager.setDefaultValues(this, R.xml.preferences, false);

			prefs = MainActivity.Instance.prefs;

			// Set Navigation Drawer
			//dataList = new ArrayList<>();
			mDrawerLayout =  FindViewById<DrawerLayout>(Resource.Id.drawer_layout);
			mDrawerList =  FindViewById<ListView>(Resource.Id.left_drawer);

			//mDrawerList.Adapter = new ArrayAdapter<string> (this, Resource.Layout.item_menu, Section);
			adapter = new DrawerAdapter(this);

			mDrawerList.SetAdapter(adapter);

			menuIcon = (ImageView) getToolbar().FindViewById(Resource.Id.home_icon);
			backIcon = (ImageView) getToolbar().FindViewById(Resource.Id.back_icon);
			backIcon.Visibility = ViewStates.Invisible;

			menuIcon.Click += delegate {
				if (mDrawerLayout.IsDrawerOpen(GravityCompat.Start)) {
					mDrawerLayout.CloseDrawer(GravityCompat.Start);
				}else{
					mDrawerLayout.OpenDrawer(GravityCompat.Start);
				}
			};
			//mDrawerLayout.SetDrawerShadow(R.drawable.drawer_shadow,GravityCompat.START);
			
			backIcon.Click += delegate {
				Console.WriteLine("INFO"+ (SupportFragmentManager.BackStackEntryCount).ToString());
				if(SupportFragmentManager.BackStackEntryCount == 0) {
					
				} else {
					//int i=SupportFragmentManager.BackStackEntryCount;
					//while(i>0)
						SupportFragmentManager.PopBackStack();
					if(FragmentBeaconImage.player !=null){
						FragmentBeaconImage.player.Release();
					}
				}
			};

			mDrawerList.ItemClick+=DrawerListItemClick;

			
			//Initialize fragment
			Fragment fragment;
			//Bundle args = new Bundle();
			fragment = new FragmentHome();
			//args.putString(FragmentHome.ITEM_NAME, dataList.get(1).getItemName());
			//fragment.setArguments(args);
			FragmentManager frgManager = SupportFragmentManager;
			frgManager.BeginTransaction().Replace(Resource.Id.content_frame, fragment)
				.Commit();

			SupportFragmentManager.BackStackChanged += delegate {
				Console.WriteLine("INFO"+ (SupportFragmentManager.BackStackEntryCount).ToString());
				if(SupportFragmentManager.BackStackEntryCount == 0) {
					backIcon.Visibility = ViewStates.Invisible;
				} else {
					backIcon.Visibility = ViewStates.Visible;
					//backIcon.SetImageResource(Resource.Drawable.back_icon);
				}
			};


		}

		protected override int getLayoutResource() {
			return Resource.Layout.HomePageLayout;
		}

		public override bool OnOptionsItemSelected (IMenuItem item)
		{
			switch (item.ItemId) {
			case Resource.Id.home:
					mDrawerLayout.OpenDrawer(GravityCompat.Start);
				return true;
			}

			return base.OnOptionsItemSelected (item);
		}

		public override void OnBackPressed ()
		{
			Console.WriteLine("INFO"+ (SupportFragmentManager.BackStackEntryCount).ToString());
			if (mDrawerLayout.IsDrawerOpen(GravityCompat.Start)) {
				mDrawerLayout.CloseDrawer(GravityCompat.Start);
			} else {
				if (SupportFragmentManager.BackStackEntryCount == 0) {
					LogOut ();
				}
			}
		}
	
		public void LogOut(){
			//Alert vuoi sloggarti?
			AlertDialog.Builder alert = new AlertDialog.Builder (this);
			alert.SetTitle ("Sicuro di voler effettuare il logout?");
			alert.SetPositiveButton ("Logout", (senderAlert, args) => {
				//chiamata per sloggarsi
				LoginManager.Instance.LogOut();

				var prefEditor = prefs.Edit();
				prefEditor.PutString("TokenN4U","");
				prefEditor.PutString("TokenFB","");
				prefEditor.Commit();

				Finish ();
			} );
			alert.SetNegativeButton ("Annulla", (senderAlert, args) => {
				//volendo fa qualcosa
			} );
			//fa partire l'alert su di un trhead
			RunOnUiThread (() => {
				alert.Show();
			} );
		}

		public void DrawerListItemClick ( object sender , AdapterView.ItemClickEventArgs item ){
			selectItem(item.Position);
		}

		public void selectItem(int position) {
			Fragment fragment = null;
			Bundle args = new Bundle();
			bool isFragment = false;

			switch (position) {
			case 1:
				if (SupportFragmentManager.BackStackEntryCount == 1) {
					SupportFragmentManager.PopBackStack ();
				}
				if (SupportFragmentManager.BackStackEntryCount ==2 ) {
					SupportFragmentManager.PopBackStackImmediate();
					SupportFragmentManager.PopBackStackImmediate();
				}
				break;
			case 2:
				fragment = new FragmentMostra();
				isFragment = true;
				break;
			case 3:
				fragment = new FragmentMusei();
				isFragment = true;
				break;
			case 4:
				fragment = new FragmentMostreTemp();
				isFragment = true;
				break;
			case 5:
				fragment = new FragmentBeaconHomePage();
				isFragment = true;
				break;
			case 6:
				int i = SupportFragmentManager.BackStackEntryCount;
				while(i>0)
				{
					SupportFragmentManager.PopBackStackImmediate();
					i = SupportFragmentManager.BackStackEntryCount;
				}
				var intent = new Intent(this, typeof(CosaVedereActivity));
				StartActivity(intent);
				break;

			default:
				break;
			}
				

			if(isFragment) {
				int i = SupportFragmentManager.BackStackEntryCount;
				while(i>0)
				{
					SupportFragmentManager.PopBackStackImmediate();
					i = SupportFragmentManager.BackStackEntryCount;
				}

				FragmentManager frgManager = SupportFragmentManager;
					frgManager.BeginTransaction()
					.Replace(Resource.Id.content_frame, fragment)
					.AddToBackStack(null)
					.Commit();


			}
			mDrawerList.SetItemChecked(position,false);
			mDrawerLayout.CloseDrawer(mDrawerList);
		}
	}

}

