﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.Util;
using Android.Content;
using Android.App;
using Android.Content.PM;
using Android.OS;
using Android.Graphics;
using Android.Widget;


namespace MuseoDelleTorture
{		
	public class MyHorizontalLinearLayout : LinearLayout
	{
			Context myContext;
			List<String> itemList = new List<String>();

		public MyHorizontalLinearLayout(Context context) : base(context){
				myContext = context;
			}

		public MyHorizontalLinearLayout(Context context, IAttributeSet attrs) : base(context,attrs){
				myContext = context;
			}

		public MyHorizontalLinearLayout(Context context, IAttributeSet attrs,int defStyle) : base(context, attrs, defStyle){
				myContext = context;
			}

		public void add(String path){
			int newIdx = itemList.Count;
			itemList.Add(path);
			AddView(getImageView(newIdx));
			}

		ImageView getImageView(int i){
			Bitmap bm = null;
			//if (i < itemList.Count()){
		//		bm = decodeSampledBitmapFromUri(itemList.get(i), 220, 220);
		//		}

			ImageView imageView = new ImageView(myContext);
			imageView.LayoutParameters=new LayoutParams(220, 220);
				//imageView.SetScaleType(ImageView.ScaleType.CENTER_CROP);
			imageView.SetImageResource(Resource.Id.GiocaButton);

			return imageView;
		}

	}
}

