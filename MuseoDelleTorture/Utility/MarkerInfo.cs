﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace MuseoDelleTorture
{		
	public class MarkerInfo
	{
		public string nome,nomeE,via,titolo,descrizione,descrizioneC;
		public float lat,lon;
		public List<string> imgurl = new List<String>();
		public bool isHeader;
	}
}

