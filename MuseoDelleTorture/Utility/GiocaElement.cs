﻿using System;

namespace MuseoDelleTorture
{
	public class GiocaElement
	{

		public string Domanda, Risposta1, Risposta2, Risposta3;
		public GiocaElement (string d,string r1,string r2,string r3)
		{

			Domanda = d;
			Risposta1 = r1;
			Risposta2 = r2;
			Risposta3 = r3;

		}
	}
}

