﻿using System;

namespace MuseoDelleTorture
{
	public class GiocaResult
	{

		public string Luogo,Risultato, Descrizione, ImageUrl;
		public int IndiceActive;
		public GiocaResult (string l,int iA,string r,string d,string iU)
		{

			Luogo = l;
			IndiceActive = iA;
			Risultato = r;
			Descrizione = d;
			ImageUrl = iU;

		}
	}
}

