﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Util;
using Android.Views;
using Android.Widget;
using Android.Graphics;

using Fragment = Android.Support.V4.App.Fragment;
using FragmentManager = Android.Support.V4.App.FragmentManager;

namespace MuseoDelleTorture
{
	public class FragmentMostra : Fragment
	{

		LinearLayout stack1,stack2,stack3,stack4;
		TextView PrimaTextClick,PrimoText,SecondoTextClick,SecondoText,TerzaTextClick,TerzaText,QuartaTextClick,QuartaText;
		ImageView PrimaFreccia,SecondaFreccia,TerzaFreccia,QuartaFreccia;
		Button MostreTemp;

		int indice1=0,indice2=0,indice3=0,indice4=0;

		public override void OnCreate (Bundle savedInstanceState)
		{
			base.OnCreate (savedInstanceState);

			// Create your fragment here
		}

		public override View OnCreateView (LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
		{
			// Use this to return your custom view for this Fragment
			// return inflater.Inflate(Resource.Layout.YourFragment, container, false);
			View view = inflater.Inflate(Resource.Layout.FragmentMostraLayout,container,false);

			ImageView prova = view.FindViewById<ImageView>(Resource.Id.imageView1);

			Typeface tf = Typeface.CreateFromAsset(Activity.Assets,"fonts/Myriad_Pro.ttf");
			Typeface tfB = Typeface.CreateFromAsset(Activity.Assets,"fonts/Myriad_Pro_Bold.ttf");
			Typeface tfG = Typeface.CreateFromAsset (Activity.Assets, "fonts/daunpenh.ttf");

			TextView text = view.FindViewById<TextView>(Resource.Id.textView1);
			text.Typeface = tfG;
				
			//Prima Label
			stack1 =  view.FindViewById<LinearLayout>(Resource.Id.textComparsa1);
			PrimaTextClick = view.FindViewById<TextView>(Resource.Id.PrimaTextClick);
			PrimaTextClick.Typeface = tfG;
			PrimaTextClick.SetTextColor (Color.White);
			PrimaFreccia = view.FindViewById<ImageView> (Resource.Id.primaFreccia);
			PrimoText = new TextView(this.Activity);
			PrimoText.Text = Context.Resources.GetString(Resource.String.Mostra_1_1);
			PrimoText.SetTextColor (Android.Graphics.Color.White);
			PrimoText.Typeface = tf;
			PrimoText.TextSize = 14;

			PrimaTextClick.Click += delegate {
				if (indice1 % 2 == 0) {
					stack1.AddView(PrimoText);
					PrimaFreccia.SetImageResource(Resource.Drawable.freccia_su);
					PrimaTextClick.SetTextColor (Color.Rgb(152,20,27));
				} else {
					stack1.RemoveAllViews();
					PrimaFreccia.SetImageResource(Resource.Drawable.freccia_giu);
					PrimaTextClick.SetTextColor (Color.White);

				}
				indice1++;
			};
			PrimaFreccia.Click += delegate {
				if (indice1 % 2 == 0) {
					stack1.AddView(PrimoText);
					PrimaFreccia.SetImageResource(Resource.Drawable.freccia_su);
					PrimaTextClick.SetTextColor (Color.Rgb(152,20,27));
				} else {
					stack1.RemoveAllViews();
					PrimaFreccia.SetImageResource(Resource.Drawable.freccia_giu);
					PrimaTextClick.SetTextColor (Color.White);
				}
				indice1++;
			}; 

			//Seconda Label

			stack2 =  view.FindViewById<LinearLayout>(Resource.Id.textComparsa2);
			SecondoTextClick = view.FindViewById<TextView>(Resource.Id.SecondoTextClick);
			SecondoTextClick.Typeface = tfG;
			SecondoTextClick.SetTextColor (Color.White);
			SecondaFreccia = view.FindViewById<ImageView> (Resource.Id.secondaFreccia);
			SecondoText = new TextView(this.Activity);
			SecondoText.Text =Context.Resources.GetString( Resource.String.Mostra_2_1) + "\n\n" + Context.Resources.GetString(Resource.String.Mostra_2_2) + "\n\n" + Context.Resources.GetString(Resource.String.Mostra_2_3) + "\n\n" + Context.Resources.GetString(Resource.String.Mostra_2_4);
			SecondoText.SetTextColor (Android.Graphics.Color.White);
			SecondoText.Typeface = tf;
			SecondoText.TextSize = 14;

			SecondoTextClick.Click += delegate {
				if (indice2 % 2 == 0) {
					stack2.AddView(SecondoText);
					SecondaFreccia.SetImageResource(Resource.Drawable.freccia_su);
					SecondoTextClick.SetTextColor (Color.Rgb(152,20,27));
				} else {
					stack2.RemoveAllViews();
					SecondaFreccia.SetImageResource(Resource.Drawable.freccia_giu);
					SecondoTextClick.SetTextColor (Color.White);

				}
				indice2++;
			};
			SecondaFreccia.Click += delegate {
				if (indice2 % 2 == 0) {
					stack2.AddView(SecondoText);
					SecondaFreccia.SetImageResource(Resource.Drawable.freccia_su);
					SecondoTextClick.SetTextColor (Color.Rgb(152,20,27));
				} else {
					stack2.RemoveAllViews();
					SecondaFreccia.SetImageResource(Resource.Drawable.freccia_giu);
					SecondoTextClick.SetTextColor (Color.White);

				}
				indice2++;
			};

			stack3 =  view.FindViewById<LinearLayout>(Resource.Id.textComparsa3);
			TerzaTextClick = view.FindViewById<TextView>(Resource.Id.TerzaTextClick);
			TerzaTextClick.Typeface = tfG;
			TerzaTextClick.SetTextColor (Color.White);
			TerzaFreccia = view.FindViewById<ImageView> (Resource.Id.terzaFreccia);
			TerzaText = new TextView(this.Activity);
			TerzaText.Text = Context.Resources.GetString(Resource.String.Mostra_3_1) + "\n\n" + Context.Resources.GetString(Resource.String.Mostra_3_2) + "\n\n" + Context.Resources.GetString(Resource.String.Mostra_3_3) ;
			TerzaText.SetTextColor (Android.Graphics.Color.White);
			TerzaText.Typeface = tf;
			TerzaText.TextSize = 14;

			TerzaTextClick.Click += delegate {
				if (indice3 % 2 == 0) {
					stack3.AddView(TerzaText);
					TerzaFreccia.SetImageResource(Resource.Drawable.freccia_su);
					TerzaTextClick.SetTextColor (Color.Rgb(152,20,27));
				} else {
					stack3.RemoveAllViews();
					TerzaFreccia.SetImageResource(Resource.Drawable.freccia_giu);
					TerzaTextClick.SetTextColor (Color.White);

				}
				indice3++;
			};
			TerzaFreccia.Click += delegate {
				if (indice3 % 2 == 0) {
					stack3.AddView(TerzaText);
					TerzaFreccia.SetImageResource(Resource.Drawable.freccia_su);
					TerzaTextClick.SetTextColor (Color.Rgb(152,20,27));
				} else {
					stack3.RemoveAllViews();
					TerzaFreccia.SetImageResource(Resource.Drawable.freccia_giu);
					TerzaTextClick.SetTextColor (Color.White);

				}
				indice3++;
			};

			stack4 =  view.FindViewById<LinearLayout>(Resource.Id.textComparsa4);
			QuartaTextClick = view.FindViewById<TextView>(Resource.Id.QuartaTextClick);
			QuartaTextClick.Typeface = tfG;
			QuartaTextClick.SetTextColor (Color.White);
			QuartaFreccia = view.FindViewById<ImageView> (Resource.Id.quartaFreccia);
			QuartaText = new TextView(this.Activity);
			QuartaText.Text = Context.Resources.GetString(Resource.String.Mostra_4_1) + "\n\n" + Context.Resources.GetString(Resource.String.Mostra_4_2) + "\n\n" + Context.Resources.GetString(Resource.String.Mostra_4_3) ;
			QuartaText.SetTextColor (Android.Graphics.Color.White);
			QuartaText.Typeface = tf;
			QuartaText.TextSize = 14;

			QuartaTextClick.Click += delegate {
				if (indice4 % 2 == 0) {
					stack4.AddView(QuartaText);
					QuartaFreccia.SetImageResource(Resource.Drawable.freccia_su);
					QuartaTextClick.SetTextColor (Color.Rgb(152,20,27));
				} else {
					stack4.RemoveAllViews();
					QuartaFreccia.SetImageResource(Resource.Drawable.freccia_giu);
					QuartaTextClick.SetTextColor (Color.White);

				}
				indice4++;
			};
			QuartaFreccia.Click += delegate {
				if (indice4 % 2 == 0) {
					stack4.AddView(QuartaText);
					QuartaFreccia.SetImageResource(Resource.Drawable.freccia_su);
					QuartaTextClick.SetTextColor (Color.Rgb(152,20,27));
				} else {
					stack4.RemoveAllViews();
					QuartaFreccia.SetImageResource(Resource.Drawable.freccia_giu);
					QuartaTextClick.SetTextColor (Color.White);

				}
				indice4++;
			};

			MostreTemp = view.FindViewById<Button> (Resource.Id.MostreTempButton);
			MostreTemp.Typeface = tfG;
			MostreTemp.TextSize = 19;
			MostreTemp.Click += delegate {

				Fragment fragment = new FragmentMostreTemp();
				FragmentManager frgManager = FragmentManager;
				frgManager.BeginTransaction()
					.Replace(Resource.Id.content_frame, fragment)
					.AddToBackStack(null)
					.Commit();

			};

			//** MOSTRE DISATTIVATE **
			MostreTemp.Visibility = ViewStates.Gone;
			//************************

			return view;
		} 
	}
}

