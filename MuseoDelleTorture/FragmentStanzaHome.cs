﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Graphics;

using Fragment = Android.Support.V4.App.Fragment;
using FragmentManager = Android.Support.V4.App.FragmentManager;
using Android.Graphics.Drawables;
using Android.Content.Res;
using Square.Picasso;

namespace MuseoDelleTorture
{
    public class FragmentStanzaHome : Fragment, ViewTreeObserver.IOnPreDrawListener
    {
        public Dictionary<int, Stanza> StanzeDictionary = new Dictionary<int, Stanza>();


        ImageView iv;
        int resID;


        public static FragmentStanzaHome Self { get; private set; }

        public bool shouldPopBack = false;

        public override void OnResume()
        {
            base.OnResume();

            if (shouldPopBack)
            {
                try
                {
                    HomePage.Instance.FragmentManager.PopBackStackImmediate();
                    if (FragmentBeaconImage.player != null)
                    {
                        FragmentBeaconImage.player.Release();
                    }
                }
                catch (Exception e)
                {

                }
                shouldPopBack = false;
            }

            new System.Threading.Thread(new System.Threading.ThreadStart(() =>
           {
               //label1.Text = "updated in thread"; // should NOT reference UILabel on background thread!
               HomePage.Instance.RunOnUiThread(() =>
              {
                  System.Threading.Thread.Sleep(1000);

                  if (HomePage.Instance.ExitBeacon)
                  {

                      HomePage.Instance.ExitBeacon = false;
                      HomePage.Instance.OnBackPressed();
                  }
              });

           })).Start();
        }

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            // Use this to return your custom view for this Fragment
            View view = inflater.Inflate(Resource.Layout.FragmentStanzaHome, container, false);

            Typeface tf = Typeface.CreateFromAsset(Activity.Assets, "fonts/daunpenh.ttf");

            HomePage.Instance.FromBeacon = false;
            HomePage.Instance.isStanza = true;

            LinearLayout.LayoutParams layoutParamsRel = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MatchParent, LinearLayout.LayoutParams.WrapContent);


            RelativeLayout.LayoutParams layoutParamsImm = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MatchParent, RelativeLayout.LayoutParams.WrapContent);
            layoutParamsImm.AddRule(LayoutRules.CenterHorizontal, 0);

            RelativeLayout.LayoutParams layoutParamsSf = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MatchParent, RelativeLayout.LayoutParams.WrapContent);

            layoutParamsSf.AddRule(LayoutRules.AlignParentBottom, 1);

            RelativeLayout.LayoutParams layoutParamsText = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WrapContent, RelativeLayout.LayoutParams.WrapContent);
            layoutParamsText.AddRule(LayoutRules.AlignParentBottom, 2);
            layoutParamsText.AddRule(LayoutRules.CenterHorizontal, 1);
            layoutParamsText.SetMargins(0, 0, 0, ConvertDpToPixel(10));

            MainActivity.Instance.SetCorrectStanzaDictionary(HomePage.Instance.MuseoID);
            StanzeDictionary = MainActivity.Instance.StanzeDictionary;

            LinearLayout content = view.FindViewById<LinearLayout>(Resource.Id.Content);

            int i = 0;
            foreach (var val in StanzeDictionary.OrderBy((arg) => arg.Key))
            {
                Stanza st = val.Value;

                RelativeLayout rel = new RelativeLayout(Activity.ApplicationContext);
                rel.LayoutParameters = layoutParamsRel;

                //Console.WriteLine("AAAAAAAAAAAAAAA");
                ImageView im = new ImageView(Activity.ApplicationContext);
                im.SetAdjustViewBounds(true);
                //im.SetScaleType (ImageView.ScaleType.CenterCrop);
                im.LayoutParameters = layoutParamsImm;

                ImageView sf = new ImageView(Activity.ApplicationContext);
                sf.SetAdjustViewBounds(true);
                sf.SetImageResource(Resource.Drawable.Sfocatura);
                sf.LayoutParameters = layoutParamsSf;

                TextView tx = new TextView(Activity.ApplicationContext);
                tx.Text = Context.Resources.GetString(Resource.String.StanzaH) + " " + st.stanza;
                tx.LayoutParameters = layoutParamsText;
                tx.Typeface = tf;
                tx.TextSize = 30;
                tx.SetTextColor(Color.White);

                if (st.img_file == "Lucca1")
                    im.SetImageResource(Resource.Drawable.stanzaLucca1);
                if (st.img_file == "Lucca2")
                    im.SetImageResource(Resource.Drawable.stanzaLucca2);
                if (st.img_file == "Lucca3")
                    im.SetImageResource(Resource.Drawable.stanzaLucca3);
                if (st.img_file == "Lucca4")
                    im.SetImageResource(Resource.Drawable.stanzaLucca4);
                if (st.img_file == "Lucca5")
                    im.SetImageResource(Resource.Drawable.stanzaLucca5);

                if (st.img_file == "SanMarino1")
                    im.SetImageResource(Resource.Drawable.stanzaSanMarino2);
                if (st.img_file == "SanMarino2")
                    im.SetImageResource(Resource.Drawable.stanzaSanMarino3);
                if (st.img_file == "SanMarino3")
                    im.SetImageResource(Resource.Drawable.stanzaSanMarino5);
                if (st.img_file == "SanMarino4")
                    im.SetImageResource(Resource.Drawable.stanzaSanMarino4);

                if (st.img_file == "Siena1")
                {
                    //Square.Picasso.Picasso.With(Activity.ApplicationContext).
                    //	  Load(Resource.Drawable.stanzaSiena1).
                    //	  Into(im);
                    //setScaledImage(im, Resource.Drawable.stanzaSiena1);
                    im.SetImageResource(Resource.Drawable.stanzaSiena1);
                }
                if (st.img_file == "Siena2")
                {
                    //Square.Picasso.Picasso.With(Activity.ApplicationContext)
                    //	  .Load(Resource.Drawable.stanzaSiena2)
                    //	  .Into(im);
                    //setScaledImage(im, Resource.Drawable.stanzaSiena2);
                    im.SetImageResource(Resource.Drawable.stanzaSiena2);
                }
                if (st.img_file == "Siena3")
                {
                    //Square.Picasso.Picasso.With(Activity.ApplicationContext).
                    //	  Load(Resource.Drawable.stanzaSiena8).
                    //	  Into(im);
                    //setScaledImage(im, Resource.Drawable.stanzaSiena8);
                    im.SetImageResource(Resource.Drawable.stanzaSiena8);
                }
                if (st.img_file == "Siena4")
                {
                    //Square.Picasso.Picasso.With(Activity.ApplicationContext).
                    //	  Load(Resource.Drawable.stanzaSiena9).
                    //	  Into(im);
                    //setScaledImage(im, Resource.Drawable.stanzaSiena9);
                    im.SetImageResource(Resource.Drawable.stanzaSiena9);
                }
                if (st.img_file == "Siena5")
                {
                    //Square.Picasso.Picasso.With(Activity.ApplicationContext).
                    //	  Load(Resource.Drawable.stanzaSiena3).
                    //	  Into(im);
                    //setScaledImage(im, Resource.Drawable.stanzaSiena3);
                    im.SetImageResource(Resource.Drawable.stanzaSiena3);
                }
                if (st.img_file == "Siena6")
                {
                    //Square.Picasso.Picasso.With(Activity.ApplicationContext).
                    //	  Load(Resource.Drawable.stanzaSiena6).
                    //	  Into(im);
                    //setScaledImage(im, Resource.Drawable.stanzaSiena6);
                    im.SetImageResource(Resource.Drawable.stanzaSiena6);
                }
                if (st.img_file == "Siena7")
                {
                    //Square.Picasso.Picasso.With(Activity.ApplicationContext).
                    //	  Load(Resource.Drawable.stanzaSiena4).
                    //	  Into(im);
                    //setScaledImage(im, Resource.Drawable.stanzaSiena4);
                    im.SetImageResource(Resource.Drawable.stanzaSiena4);
                }
                if (st.img_file == "Siena8")
                {
                    //Square.Picasso.Picasso.With(Activity.ApplicationContext).
                    //	  Load(Resource.Drawable.stanzaSiena7).
                    //	  Into(im);
                    //setScaledImage(im, Resource.Drawable.stanzaSiena7);
                    im.SetImageResource(Resource.Drawable.stanzaSiena7);
                }

                if (st.img_file == "SanGimignanoStr1")
                {
                    im.SetImageResource(Resource.Drawable.SGStrege1);
                }
                if (st.img_file == "SanGimignanoStr2")
                {
                    im.SetImageResource(Resource.Drawable.SGStrege2);
                }
                if (st.img_file == "SanGimignanoStr3")
                {
                    im.SetImageResource(Resource.Drawable.SGStrege3);
                }
                if (st.img_file == "SanGimignanoStr4")
                {
                    im.SetImageResource(Resource.Drawable.SGStrege4);
                }

                if (st.img_file == "SanGimignanoPM1")
                {
                    im.SetImageResource(Resource.Drawable.SGMorte1);
                }
                if (st.img_file == "SanGimignanoPM2")
                {
                    im.SetImageResource(Resource.Drawable.SGMorte2);
                }
                if (st.img_file == "SanGimignanoPM3")
                {
                    im.SetImageResource(Resource.Drawable.SGMorte3);
                }
                if (st.img_file == "SanGimignanoPM4")
                {
                    im.SetImageResource(Resource.Drawable.SGMorte4);
                }
                if (st.img_file == "SanGimignanoPM5")
                {
                    im.SetImageResource(Resource.Drawable.SGMorte5);
                }
                if (st.img_file == "SanGimignanoPM6")
                {
                    im.SetImageResource(Resource.Drawable.SGMorte6);
                }

                if (st.img_file == "Volterra1")
                {
                    im.SetImageResource(Resource.Drawable.Volterra1);
                }
                if (st.img_file == "Volterra2")
                {
                    im.SetImageResource(Resource.Drawable.Volterra2);
                }
                if (st.img_file == "Volterra3")
                {
                    im.SetImageResource(Resource.Drawable.Volterra3);
                }
                if (st.img_file == "Volterra4")
                {
                    im.SetImageResource(Resource.Drawable.Volterra4);
                }
                if (st.img_file == "Volterra5")
                {
                    im.SetImageResource(Resource.Drawable.Volterra5);
                }

                if (st.img_file == "Montepulciano1")
                {
                    im.SetImageResource(Resource.Drawable.Montepulciano1);
                }
                if (st.img_file == "Montepulciano2")
                {
                    im.SetImageResource(Resource.Drawable.Montepulciano2);
                }
                if (st.img_file == "Montepulciano3")
                {
                    im.SetImageResource(Resource.Drawable.Montepulciano3);
                }
                if (st.img_file == "Montepulciano4")
                {
                    im.SetImageResource(Resource.Drawable.Montepulciano4);
                }
                if (st.img_file == "Montepulciano5")
                {
                    im.SetImageResource(Resource.Drawable.Montepulciano5);
                }
                int indice = i;
                im.Click += delegate
                {

                    Console.WriteLine("PROVA:" + (i + 1) + "|" + (indice + 1));
                    MainActivity.Instance.StanzaFind = indice + 1;
                    var intent2 = new Intent(Context, typeof(BeaconPage));
                    StartActivity(intent2);

                    System.GC.Collect();
                };

                rel.AddView(im);
                rel.AddView(sf);
                rel.AddView(tx);

                content.AddView(rel);

                i++;
            }

            return view;
        }


        private int ConvertDpToPixel(float dp)
        {
            var pixel = (int)(dp * Resources.DisplayMetrics.Density);
            return pixel;
        }


        private void setScaledImage(ImageView imageView, int resId)
        {
            resID = resId;

            iv = imageView;
            ViewTreeObserver viewTreeObserver = iv.ViewTreeObserver;
            viewTreeObserver.AddOnPreDrawListener(this);

        }


        public bool OnPreDraw()
        {
            iv.ViewTreeObserver.RemoveOnPreDrawListener(this);
            int imageViewHeight = iv.MeasuredHeight;
            int imageViewWidth = iv.MeasuredWidth;
            Bitmap b = decodeSampledBitmapFromResource(Resources,
                            resID, imageViewWidth, imageViewHeight);
            iv.SetImageBitmap(b);

            //b.Recycle();

            return true;
        }


        private static Bitmap decodeSampledBitmapFromResource(Resources res, int resId,
                                                             int reqWidth, int reqHeight)
        {

            // First decode with inJustDecodeBounds = true to check dimensions
            BitmapFactory.Options options = new BitmapFactory.Options();
            options.InJustDecodeBounds = true;
            BitmapFactory.DecodeResource(res, resId, options);

            // Calculate inSampleSize
            options.InSampleSize = calculateInSampleSize(options, reqWidth, reqHeight);

            // Decode bitmap with inSampleSize set
            options.InJustDecodeBounds = false;
            Bitmap b = BitmapFactory.DecodeResource(res, resId, options);

            //b.Recycle();

            return b;
        }

        private static int calculateInSampleSize(
                BitmapFactory.Options options, int reqWidth, int reqHeight)
        {

            // Raw height and width of image
            int height = options.OutHeight;
            int width = options.OutWidth;
            int inSampleSize = 1;

            if (height > reqHeight || width > reqWidth)
            {

                int halfHeight = height / 2;
                int halfWidth = width / 2;

                // Calculate the largest inSampleSize value that is a power of 2 and keeps both
                // height and width larger than the requested height and width.
                while ((halfHeight / inSampleSize) > reqHeight
                        && (halfWidth / inSampleSize) > reqWidth)
                {
                    inSampleSize *= 2;
                }
            }

            return inSampleSize;
        }

    }


    public class CropSquareTransformation : Java.Lang.Object, ITransformation
    {
        public Bitmap Transform(Bitmap source)
        {
            int size = Math.Min(source.Width, source.Height);
            int x = (source.Width - size);
            int y = (source.Height - size);
            Bitmap result = Bitmap.CreateBitmap(source, x, y, size, size);
            if (result != source)
            {
                source.Recycle();
            }
            return result;
        }

        public string Key
        {
            get { return "square()"; }
        }
    }
}

