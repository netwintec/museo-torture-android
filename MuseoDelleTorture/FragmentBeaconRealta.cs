﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Graphics;
using Android.Media;
using Android.Views;
using Android.Widget;
using Android.Text;

using Fragment = Android.Support.V4.App.Fragment;
using FragmentManager = Android.Support.V4.App.FragmentManager;

using System.Net;
using System.Threading.Tasks;
using Android.Graphics;
using Android;
using Android.Content.PM;
using Square.Picasso;

namespace MuseoDelleTorture
{

	public class FragmentBeaconRealta : Fragment
	{

		string titolo, urlImage, descrizione, url_audio;
		int indice;
		ImageView image, play, pause, stop;
		WebClient webClient;
		public static MediaPlayer player;
		bool flagFirst = true, flagStop = false;

		public override void OnCreate(Bundle savedInstanceState)
		{
			base.OnCreate(savedInstanceState);

			// Create your fragment here
		}

		private readonly string[] Permissions =
		{
			Manifest.Permission.Camera
		};

		private void CheckPermissions()
		{
			bool minimumPermissionsGranted = true;

			foreach (string permission in Permissions)
			{
				if (Android.Support.V4.Content.ContextCompat.CheckSelfPermission(Activity.ApplicationContext, permission) != Permission.Granted) minimumPermissionsGranted = false;
			}

			// If one of the minimum permissions aren't granted, we request them from the user
			if (!minimumPermissionsGranted) Android.Support.V4.App.ActivityCompat.RequestPermissions(MainActivity.Instance, Permissions, 0);
		}
	
		public override View OnCreateView (LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
		{
			// Use this to return your custom view for this Fragment
			View view = inflater.Inflate(Resource.Layout.FragmentRealtaBeaconLayout, container, false);

			Typeface tf = Typeface.CreateFromAsset(Activity.Assets,"fonts/Myriad_Pro.ttf");
			Typeface tfB = Typeface.CreateFromAsset(Activity.Assets,"fonts/Myriad_Pro_Bold.ttf");
			Typeface tfG = Typeface.CreateFromAsset(Activity.Assets,"fonts/daunpenh.ttf");

			CheckPermissions();

			Bundle bundle = this.Arguments;
			if (bundle != null) {
				indice = int.Parse(bundle.GetString("indice"));
				titolo = FragmentBeaconHomePage.Self.BeaconDictionary [indice].titolo;
				urlImage = FragmentBeaconHomePage.Self.BeaconDictionary [indice].urlImage;
				descrizione =FragmentBeaconHomePage.Self.BeaconDictionary [indice].descrizione;
				url_audio = FragmentBeaconHomePage.Self.BeaconDictionary[indice].urlaudio;
			}

			//String text = String.Format(titolo,);
			view.FindViewById<TextView> (Resource.Id.Titolo).Text=titolo;
			view.FindViewById<TextView> (Resource.Id.Titolo).Typeface=tfG;
			view.FindViewById<TextView> (Resource.Id.Descrizione).TextFormatted=Html.FromHtml(descrizione);
			view.FindViewById<TextView> (Resource.Id.Descrizione).Typeface=tf;

			image = view.FindViewById<ImageView> (Resource.Id.Image);
			//caricaImmagineAsync (urlImage, image);

            try
            {
                Picasso.With(MainActivity.Instance.ApplicationContext)
                       .Load(urlImage)
                       .Placeholder(Resource.Drawable.placeholder)
                       .Into(image);
            }
            catch (Exception e)
            {
                Picasso.With(MainActivity.Instance.ApplicationContext)
                       .Load(Resource.Drawable.placeholder)
                       .Placeholder(Resource.Drawable.placeholder)
                       .Into(image);
            }

			AudioManager a = (AudioManager)Activity.GetSystemService ("audio");
			Console.WriteLine ("PLUG:"+a.WiredHeadsetOn);

			ImageView RAButton = view.FindViewById<ImageView> (Resource.Id.RAButton);

			RAButton.Click += delegate {

				var intent = new Intent (Activity.ApplicationContext, typeof(RABeaconActivity));
				intent.PutExtra ("folder", FragmentBeaconHomePage.Self.BeaconDictionary [indice].folder3D);
				StartActivity (intent);

			};
				
			play = view.FindViewById<ImageView> (Resource.Id.playButton);
			pause = view.FindViewById<ImageView> (Resource.Id.pauseButton);
			stop = view.FindViewById<ImageView> (Resource.Id.stopButton);

			player = new MediaPlayer();

			play.Click += delegate {

				try
				{
					//if (a.WiredHeadsetOn)
					//{
						Console.WriteLine("PLUG");
						if (flagFirst)
						{
							player.Reset();
							player.SetDataSource(url_audio);
							player.Prepare();
							player.Start();

							play.SetImageResource(Resource.Drawable.play_attivo);
							pause.SetImageResource(Resource.Drawable.pause);

							flagFirst = false;
							play.Clickable = false;
							pause.Clickable = true;
							stop.Clickable = true;
						}
						else
						{
							if (flagStop)
							{
								player.Reset();
								player.SetDataSource(url_audio);
								player.Prepare();
								player.Start();

								play.SetImageResource(Resource.Drawable.play_attivo);
								pause.SetImageResource(Resource.Drawable.pause);

								flagStop = false;
								play.Clickable = false;
								pause.Clickable = true;
								stop.Clickable = true;
							}
							else
							{
								player.Start();

								play.SetImageResource(Resource.Drawable.play_attivo);
								pause.SetImageResource(Resource.Drawable.pause);

								play.Clickable = false;
								pause.Clickable = true;
								stop.Clickable = true;
							}
						}
					/*}
					else
					{
						Console.WriteLine("UNPLUG");
						AlertDialog.Builder alert = new AlertDialog.Builder(Activity);
						alert.SetTitle("Informazione");
						alert.SetMessage("Perfavore usare un paio di cuffie per riprodurre l'audio");
						alert.SetPositiveButton("OK", (senderAlert, args) => { });
						alert.Show();
					}*/
				}
				catch (Java.IO.IOException e)
				{

					Console.WriteLine("UNPLUG");
					AlertDialog.Builder alert = new AlertDialog.Builder(Activity);
					alert.SetTitle("Errore");
					alert.SetMessage("Impossibile caricare l'audio guida!");
					alert.SetPositiveButton("OK", (senderAlert, args) => { });
					alert.Show();
				}
			};

			pause.Click += delegate {

				player.Pause();

				play.SetImageResource(Resource.Drawable.play);
				pause.SetImageResource(Resource.Drawable.pause_attivo);

				play.Clickable = true;
				pause.Clickable = false;
				stop.Clickable = true;

			};

			stop.Click += delegate {

				player.Stop();

				play.SetImageResource(Resource.Drawable.play);
				pause.SetImageResource(Resource.Drawable.pause);

				flagStop=true;
				play.Clickable = true;
				pause.Clickable = false;
				stop.Clickable = false;

			};

			pause.Clickable = false;
			stop.Clickable = false;

			return view;
		}




		async void caricaImmagineAsync(String uri,ImageView immagine_view){
			webClient = new WebClient ();
			byte[] bytes = null;
			try{
				bytes = await webClient.DownloadDataTaskAsync(uri);
			}
			catch(TaskCanceledException){
				Console.WriteLine ("Task Canceled!**************************************");
				return;
			}
			catch(Exception e){
				Console.WriteLine (e.ToString());
				return;
			}

			//if (immagine_view.IsInLayout) {

			Bitmap immagine = await BitmapFactory.DecodeByteArrayAsync (bytes, 0, bytes.Length);

			immagine_view.SetImageBitmap (immagine);

			Console.WriteLine ("Immagine caricata!");
			/*} else {
				Console.WriteLine ("Immagine contatto non più esistente per cui non la carico!");
			}*/
		}
	}
}
