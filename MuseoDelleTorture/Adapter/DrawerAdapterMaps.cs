﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Graphics;

namespace MuseoDelleTorture
{		
	public class DrawerAdapterMaps : BaseAdapter
	{
		Activity context;

		Dictionary <int,MarkerInfo> MarkerDictionary = new Dictionary <int,MarkerInfo>();

		public DrawerAdapterMaps(Activity context , Dictionary <int,MarkerInfo> Dictionary) : base()
		{
			this.context = context;

			MarkerDictionary = Dictionary;

			//For demo purposes we hard code some data here
			/*this.items = new List<ListObject>() {
				new ListObject() { First = true},
				new ListObject() { Text = "Prova",Image = Resource.Drawable.Icon },
				new ListObject() { Text = "LA MOSTRA", Image = Resource.Drawable.mostra_icon_list },
				new ListObject() { Text = "I MUSEI", Image = Resource.Drawable.museo_icon_list },
				new ListObject() { Text = "MOSTRE TEMPORANEE", Image = Resource.Drawable.mostretemp_icon_list },
				new ListObject() { Text = "GIOCA", Image = Resource.Drawable.gioca_icon_list },
				new ListObject() { Text = "COSA VEDERE IN CITTA", Image = Resource.Drawable.citta_icon_list },
			};*/
		}

		public override int Count
		{
			get { return MarkerDictionary.Count; }
		}

		public override Java.Lang.Object GetItem(int position)
		{
			return position;
		}

		public override long GetItemId(int position)
		{
			return position;
		}

		public override View GetView(int position, View convertView, ViewGroup parent)
		{
			//Get our object for this position
			//var item = items[position];  

			View view = convertView;

			Typeface tf = Typeface.CreateFromAsset(this.context.Assets,"fonts/Myriad_Pro.ttf");
			Typeface tfB = Typeface.CreateFromAsset(this.context.Assets,"fonts/Myriad_Pro_Bold.ttf");
			Typeface tfG = Typeface.CreateFromAsset(this.context.Assets,"fonts/daunpenh.ttf");

			MarkerInfo item = MarkerDictionary.ElementAt (position).Value;

			if(item.isHeader){
				LayoutInflater inflater = ((Activity) context).LayoutInflater;

				view = inflater.Inflate(Resource.Layout.drawer_map_header, parent, false);
				view.FindViewById<TextView>(Resource.Id.HeaderText).Text=item.nome;
				view.FindViewById<TextView>(Resource.Id.HeaderText).Typeface=tfG;

				//*** CHIESE , PIAZZE E MONUMENTI , PALAZZI , PARCHEGGI , SERVIZI PUBBLICI
				if(item.nome.Equals("PIAZZE E MONUMENTI"))
					view.FindViewById<ImageView>(Resource.Id.HeaderImage).SetImageResource(Resource.Drawable.PM_list_icon);
				if(item.nome.Equals("PALAZZI"))
					view.FindViewById<ImageView>(Resource.Id.HeaderImage).SetImageResource(Resource.Drawable.palazzi_list_icon);
				if(item.nome.Equals("PARCHEGGI"))
					view.FindViewById<ImageView>(Resource.Id.HeaderImage).SetImageResource(Resource.Drawable.parcheggi_list_icon);
				if(item.nome.Equals("CHIESE"))
					view.FindViewById<ImageView>(Resource.Id.HeaderImage).SetImageResource(Resource.Drawable.chiese_list_icon);

				return view;

			}else {

				LayoutInflater inflater = ((Activity) context).LayoutInflater;

				view = inflater.Inflate(Resource.Layout.drawer_map_row, parent, false);

				view.FindViewById<TextView>(Resource.Id.RowText).Text=item.nomeE;
				view.FindViewById<TextView>(Resource.Id.RowText).Typeface=tf;

				return view;
			}
			/*
			//Try to reuse convertView if it's not  null, otherwise inflate it from our item layout
			// This gives us some performance gains by not always inflating a new view
			// This will sound familiar to MonoTouch developers with UITableViewCell.DequeueReusableCell()
			var view = (convertView ??
				context.LayoutInflater.Inflate(
					Resource.layout.customlistitem,
					parent,
					false)) as LinearLayout;

			//Find references to each subview in the list item's view
			var imageItem = view.FindViewById(Resource.id.imageItem) as ImageView;
			var textTop = view.FindViewById(Resource.id.textTop) as TextView;
			var textBottom = view.FindViewById(Resource.id.textBottom) as TextView;

			//Assign this item's values to the various subviews
			imageItem.SetImageResource(item.Image);
			textTop.SetText(item.Name, TextView.BufferType.Normal);
			textBottom.SetText(item.Description, TextView.BufferType.Normal);

			//Finally return the view*/
			return view;
		}

		public MarkerInfo GetItemAtPosition(int position)
		{
			return MarkerDictionary.ElementAt(position).Value;
		}
	}
}

