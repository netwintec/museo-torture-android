﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Graphics;
using Android.Media;
using Android.Views;
using Android.Widget;

using Fragment= Android.Support.V4.App.Fragment;
using FragmentManager = Android.Support.V4.App.FragmentManager;

using System.Net;
using System.Threading.Tasks;
using Android.Graphics;
using Square.Picasso;

namespace MuseoDelleTorture
{
	public class FragmentBeaconImages : Fragment
	{

		string titolo,descrizione,url_audio;
		List <string> ListImage = new List<string>();
		int indice;
		LinearLayout myGallery;
		ImageView play,pause,stop;
		WebClient webClient;
		public static MediaPlayer player;
		bool flagFirst = true, flagStop = false;

		public override void OnCreate (Bundle savedInstanceState)
		{
			base.OnCreate (savedInstanceState);

			// Create your fragment here
		}

		public override View OnCreateView (LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
		{
			// Use this to return your custom view for this Fragment
			View view = inflater.Inflate(Resource.Layout.FragmentImagesBeaconLayout, container, false);

			Typeface tf = Typeface.CreateFromAsset(Activity.Assets,"fonts/Myriad_Pro.ttf");
			Typeface tfB = Typeface.CreateFromAsset(Activity.Assets,"fonts/Myriad_Pro_Bold.ttf");
			Typeface tfG = Typeface.CreateFromAsset(Activity.Assets,"fonts/daunpenh.ttf");

			Bundle bundle = this.Arguments;
			if (bundle != null) {
				indice = int.Parse(bundle.GetString("indice"));
				titolo = FragmentBeaconHomePage.Self.BeaconDictionary [indice].titolo;
				ListImage = FragmentBeaconHomePage.Self.BeaconDictionary [indice].ListUrlImage;
				descrizione = FragmentBeaconHomePage.Self.BeaconDictionary [indice].descrizione;
				url_audio = FragmentBeaconHomePage.Self.BeaconDictionary[indice].urlaudio;
			}
				

			view.FindViewById<TextView> (Resource.Id.Titolo).Text=titolo;
			view.FindViewById<TextView> (Resource.Id.Titolo).Typeface=tfG;
			view.FindViewById<TextView> (Resource.Id.Descrizione).Text=descrizione;
			view.FindViewById<TextView> (Resource.Id.Descrizione).Typeface=tf;
			Console.WriteLine ("B1");

			myGallery =view.FindViewById<LinearLayout>(Resource.Id.Gallery);

			Console.WriteLine ("B2"+ListImage.Count);
			for (int i = 0; i < ListImage.Count; i++) {
				myGallery.AddView(insertPhoto(ListImage[i]));
			}
			Console.WriteLine ("B3");
			AudioManager a = (AudioManager)Activity.GetSystemService ("audio");
			Console.WriteLine ("PLUG:"+a.WiredHeadsetOn);

			play = view.FindViewById<ImageView> (Resource.Id.playButton);
			pause = view.FindViewById<ImageView> (Resource.Id.pauseButton);
			stop = view.FindViewById<ImageView> (Resource.Id.stopButton);

			player = new MediaPlayer();

			play.Click += delegate {

				try
				{
					//if (a.WiredHeadsetOn)
					//{
						Console.WriteLine("PLUG");
						if (flagFirst)
						{
							player.Reset();
							player.SetDataSource(url_audio);
							player.Prepare();
							player.Start();

							play.SetImageResource(Resource.Drawable.play_attivo);
							pause.SetImageResource(Resource.Drawable.pause);

							flagFirst = false;
							play.Clickable = false;
							pause.Clickable = true;
							stop.Clickable = true;
						}
						else
						{
							if (flagStop)
							{
								player.Reset();
								player.SetDataSource(url_audio);
								player.Prepare();
								player.Start();

								play.SetImageResource(Resource.Drawable.play_attivo);
								pause.SetImageResource(Resource.Drawable.pause);

								flagStop = false;
								play.Clickable = false;
								pause.Clickable = true;
								stop.Clickable = true;
							}
							else
							{
								player.Start();

								play.SetImageResource(Resource.Drawable.play_attivo);
								pause.SetImageResource(Resource.Drawable.pause);

								play.Clickable = false;
								pause.Clickable = true;
								stop.Clickable = true;
							}
						}
					/*}
					else
					{
						Console.WriteLine("UNPLUG");
						AlertDialog.Builder alert = new AlertDialog.Builder(Activity);
						alert.SetTitle("Informazione");
						alert.SetMessage("Perfavore usare un paio di cuffie per riprodurre l'audio");
						alert.SetPositiveButton("OK", (senderAlert, args) => { });
						alert.Show();
					}*/
				}
				catch (Java.IO.IOException e)
				{

					Console.WriteLine("UNPLUG");
					AlertDialog.Builder alert = new AlertDialog.Builder(Activity);
					alert.SetTitle("Errore");
					alert.SetMessage("Impossibile caricare l'audio guida!");
					alert.SetPositiveButton("OK", (senderAlert, args) => { });
					alert.Show();
				}

			};

			pause.Click += delegate {

				player.Pause();

				play.SetImageResource(Resource.Drawable.play);
				pause.SetImageResource(Resource.Drawable.pause_attivo);

				play.Clickable = true;
				pause.Clickable = false;
				stop.Clickable = true;

			};

			stop.Click += delegate {

				player.Stop();

				play.SetImageResource(Resource.Drawable.play);
				pause.SetImageResource(Resource.Drawable.pause);

				flagStop=true;
				play.Clickable = true;
				pause.Clickable = false;
				stop.Clickable = false;

			};

			pause.Clickable = false;
			stop.Clickable = false;

			return view;
		}

		private int ConvertDpToPixel(float dp)
		{
			var pixel = (int)(dp*Resources.DisplayMetrics.Density);
			return pixel;
		}


		public View insertPhoto(String path){
			//Bitmap bm = decodeSampledBitmapFromUri(path, 220, 220);

			LinearLayout layout = new LinearLayout(Application.Context);
			layout.LayoutParameters = new ViewGroup.LayoutParams (ViewGroup.LayoutParams.WrapContent, ViewGroup.LayoutParams.MatchParent);
			layout.SetGravity (GravityFlags.Center);

			ImageView imageView = new ImageView(Application.Context);
			LinearLayout.LayoutParams imgLayout = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WrapContent, LinearLayout.LayoutParams.MatchParent);
			imgLayout.SetMargins (ConvertDpToPixel (5), ConvertDpToPixel (0), ConvertDpToPixel (5), ConvertDpToPixel (0));
			imageView.LayoutParameters = imgLayout;//new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WrapContent, LinearLayout.LayoutParams.MatchParent);
			imageView.SetImageResource (Resource.Drawable.placeholder);
			imageView.SetAdjustViewBounds (true);

            try
            {
                Picasso.With(Activity.ApplicationContext)
                       .Load(path)
                       .Placeholder(Resource.Drawable.placeholder)
                       .Into(imageView);
            }
            catch (Exception e)
            {
                Picasso.With(Activity.ApplicationContext)
                       .Load(Resource.Drawable.placeholder)
                       .Placeholder(Resource.Drawable.placeholder)
                       .Into(imageView);
            }

			//caricaImmagineAsync (path, imageView);
			layout.AddView(imageView);

			return layout;
		}

		async void caricaImmagineAsync(String uri,ImageView immagine_view){
			webClient = new WebClient ();
			byte[] bytes = null;
			try{
				bytes = await webClient.DownloadDataTaskAsync(uri);
			}
			catch(TaskCanceledException){
				Console.WriteLine ("Task Canceled!**************************************");
				return;
			}
			catch(Exception e){
				Console.WriteLine (e.ToString());
				return;
			}

			//if (immagine_view.IsInLayout) {

			Bitmap immagine = await BitmapFactory.DecodeByteArrayAsync (bytes, 0, bytes.Length);

			immagine_view.SetImageBitmap (immagine);

			Console.WriteLine ("Immagine caricata!");
			/*} else {
				Console.WriteLine ("Immagine contatto non più esistente per cui non la carico!");
			}*/
		}
	}
}


