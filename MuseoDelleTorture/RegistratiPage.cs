﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Graphics;
using Android.Views;
using Android.Widget;
using Xamarin.Facebook;
using Xamarin.Facebook.Login;
using Xamarin.Facebook.Login.Widget;
using Android.Support.V4.App;
using Android.Content.PM;
using Java.Security;
using Xamarin.Facebook.AppEvents;

namespace MuseoDelleTorture
{
	[Activity (Label = "@string/app_name",ScreenOrientation = ScreenOrientation.Portrait)]			
	public class RegistratiPage : Activity,View.IOnTouchListener,GestureDetector.IOnGestureListener
	{
		EditText nome,cognome,eMail,repeatPassword,password,paese,citta;
		Button Registrati;
		ICallbackManager callbackManager;
		LoginButton loginButton;
		ISharedPreferences prefs;
		int attivo = 1;
		ImageView pallino1,pallino2,pallino3,pallino4;
		ViewFlipper immagineSwipe;
		protected GestureDetector gestureScanner;
		private static int SWIPE_MIN_DISTANCE = 120;
		private static int SWIPE_MAX_OFF_PATH = 250;
		private static int SWIPE_THRESHOLD_VELOCITY = 200;
		bool isPopUp = true;

		protected override void OnCreate (Bundle bundle)
		{
			base.OnCreate (bundle);

			ActionBar.Hide ();

			callbackManager = CallbackManagerFactory.Create ();

			SetContentView (Resource.Layout.RegistratiPageLayout);

			Window.SetSoftInputMode (SoftInput.StateHidden);

			Typeface tf = Typeface.CreateFromAsset(Assets,"fonts/Myriad_Pro.ttf");

			nome = FindViewById <EditText> (Resource.Id.NomeText);
			nome.Typeface = tf;
			cognome = FindViewById <EditText> (Resource.Id.CognomeText);
			cognome.Typeface = tf;
			eMail = FindViewById <EditText> (Resource.Id.eMailText);
			eMail.Typeface = tf;
			password = FindViewById <EditText> (Resource.Id.PasswordText);
			password.Typeface = tf;
			repeatPassword = FindViewById <EditText> (Resource.Id.RepeatPasswordText);
			repeatPassword.Typeface = tf;
			paese = FindViewById <EditText> (Resource.Id.PaeseText);
			paese.Typeface = tf;
			citta = FindViewById <EditText> (Resource.Id.CittaText);
			citta.Typeface = tf;

			Registrati = FindViewById<Button> (Resource.Id.RegistratiButton);
			Registrati.Typeface = tf;

			Registrati.Click += delegate {

				if(password.Text.CompareTo(repeatPassword.Text)==0){

					UtilityLoginManager loginManager = new UtilityLoginManager();
					List<string> value=loginManager.ValidationReg(nome.Text,cognome.Text,eMail.Text,password.Text,paese.Text,citta.Text);
					if(value[0] == "ERRORDATA"){
						int errore=0;
						if(value.Contains("N"))
							errore++;
						if(value.Contains("C"))
							errore++;
						if(value.Contains("P"))
							errore++;
						if(value.Contains("E")){
							errore++;
							eMail.Text="";
						}
						if(errore == 1){
							AlertDialog.Builder alert = new AlertDialog.Builder (this);
							alert.SetTitle("Errore");
							alert.SetMessage ("Un campo da lei inserito contiene valori errati o non contiene nessun valore, perfavore controlli");
							alert.SetPositiveButton ("OK", (senderAlert, args) => {} );
							alert.Show();
						}
						if(errore >=2){
							AlertDialog.Builder alert = new AlertDialog.Builder (this);
							alert.SetTitle("Errore");
							alert.SetMessage ("Due o più campi da lei inseriti contengono valori errati o non contengono nessun valore, perfavore controlli");
							alert.SetPositiveButton ("OK", (senderAlert, args) => {} );
							alert.Show();
						}
					}
					if(value[0]=="ERROR"){

						AlertDialog.Builder alert = new AlertDialog.Builder (this);
						alert.SetTitle("Errore di rete");
						alert.SetMessage ("Registrazione non Riuscito riprovare più tardi");
						alert.SetPositiveButton ("OK", (senderAlert, args) => {} );
						alert.Show();

					}

					if(value[0]=="SUCCESS"){


						prefs=MainActivity.Instance.prefs;
						var prefEditor = prefs.Edit();
						prefEditor.PutString("TokenN4U",value[1]);
						prefEditor.Commit();


						var intent = new Intent (this, typeof(HomePage));
						StartActivity (intent);
						Finish();
						//******* SALVA TOKEN *********

					}
					
				}
				else{

					AlertDialog.Builder alert = new AlertDialog.Builder (this);
					alert.SetTitle("Errore");
					alert.SetMessage ("Le password non corrispondono riprovare");
					alert.SetPositiveButton ("OK", (senderAlert, args) => {} );
					alert.Show();
					password.Text="";
					repeatPassword.Text="";


				}
			};

			var loginCallback = new FacebookCallback<LoginResult> {

				HandleSuccess = loginResult => {
					//login
					Console.WriteLine ("Loggato con successo");
					Console.WriteLine(AccessToken.CurrentAccessToken.Token);

					UtilityLoginManager loginManagerFB=new UtilityLoginManager();
					List<string> Response = loginManagerFB.LoginFB(AccessToken.CurrentAccessToken.Token);

					if(Response[0] =="Errore"){
						LoginManager.Instance.LogOut();
						AlertDialog.Builder alert = new AlertDialog.Builder (this);
						alert.SetTitle("Errore di rete");
						alert.SetMessage ("Login non Riuscito riprovare più tardi");
						alert.SetPositiveButton ("OK", (senderAlert, args) => {} );
						alert.Show();
					}

					if(Response[0] =="SUCCESS"){
						Console.WriteLine("SUCCESS");

						prefs=MainActivity.Instance.prefs;
						var prefEditor = prefs.Edit();
						prefEditor.PutString("TokenN4U",Response[1]);
						prefEditor.Commit();

						loginButton.ClearPermissions();
						loginButton.SetPublishPermissions("publish_actions");

						var intent = new Intent (this, typeof(HomePage));
						StartActivity (intent);
						Finish();
						//****** SALVA TOKEN ******

					}
				},
				HandleCancel = () => {
					Console.WriteLine ("Condizioni non accettate");
				},
				HandleError = loginError => {
					Console.WriteLine ("Log in fallito");
				}
			};

			LoginManager.Instance.RegisterCallback (callbackManager, loginCallback);

			loginButton = (LoginButton) FindViewById<Button> (Resource.Id.LoginFb);
			loginButton.SetReadPermissions("public_profile","email");

			TextView text = FindViewById<TextView> (Resource.Id.textView1);
			text.Typeface = tf;
			TextView text1 = FindViewById<TextView> (Resource.Id.textView2);
			text1.Typeface = tf;
			TextView text2 = FindViewById<TextView> (Resource.Id.textView3);
			text2.Typeface = tf;
			TextView text3 = FindViewById<TextView> (Resource.Id.textView4);
			text3.Typeface = tf;

			immagineSwipe = FindViewById<ViewFlipper> (Resource.Id.immagineSwipe);
			immagineSwipe.SetOnTouchListener (this);

			pallino1 = FindViewById<ImageView> (Resource.Id.pallino1);
			pallino2 = FindViewById<ImageView> (Resource.Id.pallino2);
			pallino3 = FindViewById<ImageView> (Resource.Id.pallino3);
			pallino4 = FindViewById<ImageView> (Resource.Id.pallino4);

			gestureScanner = new GestureDetector(this);

			ImageView escImage = FindViewById<ImageView> (Resource.Id.EscImage);

			escImage.Click += delegate {
				isPopUp = false;
				FindViewById<RelativeLayout>(Resource.Id.PopUpView).Visibility = ViewStates.Invisible;
			};

		}

		public override void OnBackPressed ()
		{

			if (isPopUp) {
				isPopUp = false;
				FindViewById<RelativeLayout>(Resource.Id.PopUpView).Visibility = ViewStates.Invisible;
			} else {
				Finish ();
			}

		}

		protected override void OnResume ()
		{
			base.OnResume ();

			AppEventsLogger.ActivateApp (this);

		}

		protected override void OnSaveInstanceState (Bundle outState)
		{
			base.OnSaveInstanceState (outState);
		}

		protected override void OnActivityResult (int requestCode, Result resultCode, Intent data)
		{
			base.OnActivityResult (requestCode, resultCode, data);

			callbackManager.OnActivityResult (requestCode, (int)resultCode, data);
		}

		protected override void OnPause ()
		{
			base.OnPause ();

			AppEventsLogger.DeactivateApp (this);
		}

		protected override void OnDestroy ()
		{
			base.OnDestroy ();
		}

		public bool OnTouch (View v, MotionEvent e)
		{
			return gestureScanner.OnTouchEvent(e);
		}

		public bool OnDown (MotionEvent e)
		{
			return true;
		}

		public bool OnFling (MotionEvent e1, MotionEvent e2, float velocityX, float velocityY)
		{
			Console.WriteLine(e1.GetX()+"  "+e2.GetX()+"  "+ Math.Abs(e1.GetX() - e2.GetX())+"  "+velocityX+"  "+Math.Abs(velocityX));
			try {
				if(e1.GetX() > e2.GetX() && Math.Abs(e1.GetX() - e2.GetX()) > SWIPE_MIN_DISTANCE && Math.Abs(velocityX) > SWIPE_THRESHOLD_VELOCITY) {
					Console.WriteLine("L");
					//Toast.makeText(this.getApplicationContext(), "Left", Toast.LENGTH_SHORT).show();
					attivo++;
					if(attivo!=5){
						immagineSwipe.ShowNext();
						cambiaPallino(attivo);
					}
					else{
						attivo--;
					}
				}else if (e1.GetX() < e2.GetX() && e2.GetX() - e1.GetX() > SWIPE_MIN_DISTANCE && Math.Abs(velocityX) > SWIPE_THRESHOLD_VELOCITY) {
					Console.WriteLine("R");
					//Toast.makeText(this.getApplicationContext(), "Right", Toast.LENGTH_SHORT).show();
					attivo--;
					if(attivo!=0){
						immagineSwipe.ShowPrevious();
						cambiaPallino(attivo);
					}
					else{
						attivo++;
					}
				}
			} catch (Exception e) {
				// nothing
			}
			return true;

		}

		public void OnLongPress (MotionEvent e)
		{
		}

		public bool OnScroll (MotionEvent e1, MotionEvent e2, float distanceX, float distanceY)
		{
			return true;
		}

		public void OnShowPress (MotionEvent e)
		{
		}

		public bool OnSingleTapUp (MotionEvent e)
		{
			return true;
		}

		public void cambiaPallino(int posizione){

			if (posizione == 1) {
				pallino1.SetImageResource (Resource.Drawable.pallinorosso);
				pallino2.SetImageResource (Resource.Drawable.pallinobianco);
				pallino3.SetImageResource (Resource.Drawable.pallinobianco);
				pallino4.SetImageResource (Resource.Drawable.pallinobianco);
			}
			if (posizione == 2) {
				pallino1.SetImageResource (Resource.Drawable.pallinobianco);
				pallino2.SetImageResource (Resource.Drawable.pallinorosso);
				pallino3.SetImageResource (Resource.Drawable.pallinobianco);
				pallino4.SetImageResource (Resource.Drawable.pallinobianco);
			}
			if (posizione == 3) {
				pallino1.SetImageResource (Resource.Drawable.pallinobianco);
				pallino2.SetImageResource (Resource.Drawable.pallinobianco);
				pallino3.SetImageResource (Resource.Drawable.pallinorosso);
				pallino4.SetImageResource (Resource.Drawable.pallinobianco);
			}
			if (posizione == 4) {
				pallino1.SetImageResource (Resource.Drawable.pallinobianco);
				pallino2.SetImageResource (Resource.Drawable.pallinobianco);
				pallino3.SetImageResource (Resource.Drawable.pallinobianco);
				pallino4.SetImageResource (Resource.Drawable.pallinorosso);
			}
		}

	}

	/*class FacebookCallback<TResult> : Java.Lang.Object, IFacebookCallback where TResult : Java.Lang.Object
	{
		public Action HandleCancel { get; set; }
		public Action<FacebookException> HandleError { get; set; }
		public Action<TResult> HandleSuccess { get; set; }

		public void OnCancel ()
		{
			var c = HandleCancel;
			if (c != null)
				c ();
		}

		public void OnError (FacebookException error)
		{
			var c = HandleError;
			if (c != null)
				c (error);
		}

		public void OnSuccess (Java.Lang.Object result)
		{
			var c = HandleSuccess;
			if (c != null)
				c (result.JavaCast<TResult> ());
		}
	}*/
}

