﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Graphics;
using Android.Media;
using Android.Views;
using Android.Widget;

using Fragment= Android.Support.V4.App.Fragment;
using FragmentManager = Android.Support.V4.App.FragmentManager;

using System.Net;
using System.Threading.Tasks;
using Android.Graphics;

namespace MuseoDelleTorture
{
	public class FragmentBeaconVideo : Fragment
	{

		string titolo,urlVideo,descrizione;
		int indice;
		VideoView Video;
		ImageView Placeholder;
		WebClient webClient;
		public static MediaPlayer player;
		bool flagFirst = true, flagStop = false;
		RelativeLayout VideoContainer;

		public override void OnCreate (Bundle savedInstanceState)
		{
			base.OnCreate (savedInstanceState);

			// Create your fragment here
		}

		public override View OnCreateView (LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
		{
			// Use this to return your custom view for this Fragment
			View view = inflater.Inflate(Resource.Layout.FragmentVideoBeaconLayout, container, false);

			Typeface tf = Typeface.CreateFromAsset(Activity.Assets,"fonts/Myriad_Pro.ttf");
			Typeface tfB = Typeface.CreateFromAsset(Activity.Assets,"fonts/Myriad_Pro_Bold.ttf");
			Typeface tfG = Typeface.CreateFromAsset(Activity.Assets,"fonts/daunpenh.ttf");

			Bundle bundle = this.Arguments;
			if (bundle != null) {
				indice = int.Parse(bundle.GetString("indice"));
				titolo = FragmentBeaconHomePage.Self.BeaconDictionary [indice].titolo;
				urlVideo = FragmentBeaconHomePage.Self.BeaconDictionary [indice].videoUrl;
				descrizione = FragmentBeaconHomePage.Self.BeaconDictionary [indice].descrizione;
			}
			Console.WriteLine ("VIDEO:"+titolo + "|" + urlVideo + "|" + descrizione);
			view.FindViewById<TextView> (Resource.Id.Titolo).Text=titolo;
			view.FindViewById<TextView> (Resource.Id.Titolo).Typeface=tfG;
			view.FindViewById<TextView> (Resource.Id.Descrizione).Text=descrizione;
			view.FindViewById<TextView> (Resource.Id.Descrizione).Typeface=tf;

			Placeholder = view.FindViewById<ImageView> (Resource.Id.placeholder);
			VideoContainer = view.FindViewById<RelativeLayout> (Resource.Id.relativeLayout2);
			Console.WriteLine ("C:" + VideoContainer.Width + "|" + VideoContainer.Height);

			Video = view.FindViewById<VideoView> (Resource.Id.Video);
			Video.SetVideoURI (Android.Net.Uri.Parse (urlVideo));
			Video.SetMediaController(new MediaController(Context));
			Video.RequestFocus ();

			//Video.Start ();

			Video.Prepared += (sender, e) => {

				Console.WriteLine("PRONTO");
				Console.WriteLine ("C:" + VideoContainer.Width + "|" + VideoContainer.Height);
				Placeholder.Visibility = ViewStates.Invisible;
				Video.Visibility = ViewStates.Visible;
				int width = (int)((VideoContainer.Height * 540)/900);
				Video.Measure(width,VideoContainer.Height);
				Video.Start();
			};

			return view;
		}
	}
}

