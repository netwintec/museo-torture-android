﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.Content.PM;
using Android.OS; 
using Android.Runtime;
using Android.Views;
using Android.Widget;
using System.Net;
using System.Threading.Tasks;
using Android.Graphics;
using Android.Text;
using Square.Picasso;

namespace MuseoDelleTorture
{
	[Activity (Label = "CosaVedereInfoActivity",Theme = "@style/AppTheme",ScreenOrientation = ScreenOrientation.Portrait)]			
	class CosaVedereInfoActivity : BaseActivity
	{

		public ImageView backIcon,menuIcon;
		LinearLayout myGallery,navigaVerso;

		MarkerInfo mi;
		string key;

		TextView Titolo,Descrizione;

		WebClient webClient;

		View layer;
		RelativeLayout popupWindow;
		ImageView immagineGrande,escImage;

		Dictionary<int,Bitmap> immaginiBit = new Dictionary<int,Bitmap>();

		bool isPopUp = false;

		protected override void OnCreate (Bundle bundle)
		{
			base.OnCreate (bundle);

			setActionBarIcon(Resource.Drawable.lente_icon,Resource.Drawable.back_icon);

			key=Intent.GetStringExtra ("key") ?? "Data not available";

			menuIcon = (ImageView) getToolbar().FindViewById(Resource.Id.home_icon);
			backIcon = (ImageView) getToolbar().FindViewById(Resource.Id.back_icon);
			menuIcon.Visibility = ViewStates.Invisible;

			backIcon.Click += delegate {
				Console.WriteLine("INFO"+ (SupportFragmentManager.BackStackEntryCount).ToString());
				this.Finish();
			};


			Typeface tf = Typeface.CreateFromAsset(Assets,"fonts/Myriad_Pro.ttf");
			Typeface tfB = Typeface.CreateFromAsset(Assets,"fonts/Myriad_Pro_Bold.ttf");
			Typeface tfG = Typeface.CreateFromAsset(Assets,"fonts/daunpenh.ttf");


			myGallery =FindViewById<LinearLayout>(Resource.Id.mygallery);

			mi = CosaVedereActivity.Self.MarkerDictionary [key];

			for (int i = 0; i < mi.imgurl.Count; i++) {
				myGallery.AddView(insertPhoto(mi.imgurl[i],i));
			}
			/*myGallery.AddView(insertPhoto("1"));
			myGallery.AddView(insertPhoto("2"));
			myGallery.AddView(insertPhoto("3"));
			myGallery.AddView(insertPhoto("4"));
			myGallery.AddView(insertPhoto("5"));*/
			  
			FindViewById<TextView> (Resource.Id.textView1).Typeface=tfG;
			FindViewById<TextView> (Resource.Id.textView2).Typeface=tf;

			Titolo = FindViewById<TextView> (Resource.Id.titoloText);
			Titolo.Typeface=tf;
			Descrizione = FindViewById<TextView> (Resource.Id.descrizioneText);
			Descrizione.Typeface=tf;
			navigaVerso = FindViewById<LinearLayout> (Resource.Id.NavigaVersoButton);
			Titolo.Text = mi.titolo;
			Descrizione.Text= mi.descrizioneC;
			navigaVerso.Click += delegate {
				Console.WriteLine("geo:"+mi.lat+","+mi.lon);
				string lat = mi.lat.ToString().Replace(",",".");
				string lon = mi.lon.ToString().Replace(",",".");
				Console.WriteLine("geo:"+lat+","+lon+"?q="+WebUtility.UrlEncode(mi.via));
				var geoUri = Android.Net.Uri.Parse ("geo:"+mi.lat+","+mi.lon+"?q="+WebUtility.UrlEncode(mi.via));
				var mapIntent = new Intent (Intent.ActionView, geoUri);
				StartActivity (mapIntent);
			};

			layer = FindViewById<View> (Resource.Id.LayerOscuratore);
			layer.Visibility = ViewStates.Invisible;
			popupWindow = FindViewById <RelativeLayout> (Resource.Id.PopUpImage);
			popupWindow.Visibility = ViewStates.Invisible;
			immagineGrande = FindViewById<ImageView> (Resource.Id.ImmagineGrande);
			escImage = FindViewById<ImageView> (Resource.Id.EscImage);

			escImage.Click += delegate {
				ChiudiPopUp();
			};

		}

		protected override int getLayoutResource() {
			return Resource.Layout.CosaVedereInfoLayout;
		}
			

		public override void OnBackPressed ()
		{
			if (isPopUp) {
				ChiudiPopUp ();
			} else {
				Finish ();
			}
		}

		public View insertPhoto(String path,int indice){
			//Bitmap bm = decodeSampledBitmapFromUri(path, 220, 220);

			LinearLayout layout = new LinearLayout(Application.Context);
			layout.LayoutParameters = new ViewGroup.LayoutParams (490, 450);
			layout.SetGravity (GravityFlags.Center);

			ImageView imageView = new ImageView(Application.Context);
			imageView.LayoutParameters =new ViewGroup.LayoutParams(450, 450);
			imageView.SetImageResource (Resource.Drawable.placeholder);
			imageView.SetAdjustViewBounds (true);

			imageView.Click += delegate {
				if(!isPopUp){
					Console.WriteLine ("ImageView n°"+indice + " "+ imageView.Id);
					AppariPopUp(path,indice);
				}
			};
				
			//caricaImmagineAsyncPopUp (path, imageView,indice);

            try
            {
                Picasso.With(MainActivity.Instance.ApplicationContext)
                       .Load(path)
                       .Placeholder(Resource.Drawable.placeholder)
                       .Into(imageView);
            }
            catch (Exception e)
            {
                Picasso.With(MainActivity.Instance.ApplicationContext)
                       .Load(Resource.Drawable.placeholder)
                       .Placeholder(Resource.Drawable.placeholder)
                       .Into(imageView);
            }

			layout.AddView(imageView);

			return layout;
		}

		private Bitmap GetImageBitmapFromUrl(string url)
		{
			Bitmap imageBitmap = null;

			using (var webClient = new WebClient())
			{
				var imageBytes = webClient.DownloadData(url);
				Console.WriteLine ("ProvaDownload:\""+imageBytes.ToString ()+"\" "+imageBytes == null+"  "+imageBytes.Length);
				if (imageBytes != null && imageBytes.Length > 0)
				{
					imageBitmap = BitmapFactory.DecodeByteArray(imageBytes, 0, imageBytes.Length);
				}
			}

			return imageBitmap;
		}

		async void caricaImmagineAsync(String uri,ImageView immagine_view,int indice){
			webClient = new WebClient ();
			byte[] bytes = null;
			try{
				bytes = await webClient.DownloadDataTaskAsync(uri);
			}
			catch(TaskCanceledException){
				Console.WriteLine ("Task Canceled!**************************************");
				return;
			}
			catch(Exception e){
				Console.WriteLine (e.ToString());
				return;
			}

			//if (immagine_view.IsInLayout) {

			Bitmap immagine = await BitmapFactory.DecodeByteArrayAsync (bytes, 0, bytes.Length);

			immagine_view.SetImageBitmap (immagine);
			immaginiBit.Add (indice,immagine);

			Console.WriteLine ("Immagine caricata!");
			/*} else {
				Console.WriteLine ("Immagine contatto non più esistente per cui non la carico!");
			}*/
		}

		async void caricaImmagineAsyncPopUp(String uri,ImageView immagine_view,int indice){
			webClient = new WebClient ();
			byte[] bytes = null;
			try{
				bytes = await webClient.DownloadDataTaskAsync(uri);
			}
			catch(TaskCanceledException){
				Console.WriteLine ("Task Canceled!**************************************");
				return;
			}
			catch(Exception e){
				Console.WriteLine (e.ToString());
				return;
			}

			//if (immagine_view.IsInLayout) {

			Bitmap immagine = await BitmapFactory.DecodeByteArrayAsync (bytes, 0, bytes.Length);

			immagine_view.SetImageBitmap (immagine);
			//immaginiBit.Add (indice,immagine);

			Console.WriteLine ("Immagine caricata!");
			/*} else {
				Console.WriteLine ("Immagine contatto non più esistente per cui non la carico!");
			}*/
		}

		public void AppariPopUp(string Path,int indice){
			isPopUp = true;
			layer.Visibility = ViewStates.Visible;
			popupWindow.Visibility = ViewStates.Visible;


			navigaVerso.Clickable = false;
			backIcon.Clickable = false;
			myGallery.Clickable = false;
			Bitmap immagine; 
			if (immaginiBit.ContainsKey (indice)) {
				immagine = immaginiBit [indice];
			} else {
				immagine = null;
			}

			if (immagine == null) {

                try
                {
                    Picasso.With(MainActivity.Instance.ApplicationContext)
                           .Load(Path)
                           .Placeholder(Resource.Drawable.placeholder)
                           .Into(immagineGrande);
                }
                catch (Exception e)
                {
                    Picasso.With(MainActivity.Instance.ApplicationContext)
                           .Load(Resource.Drawable.placeholder)
                           .Placeholder(Resource.Drawable.placeholder)
                           .Into(immagineGrande);
                }

				//caricaImmagineAsync (Path, immagineGrande,indice);
			} else {
				immagineGrande.SetImageBitmap (immagine);
			}

		}

		public void ChiudiPopUp(){
			isPopUp = false;
			layer.Visibility = ViewStates.Invisible;
			popupWindow.Visibility = ViewStates.Invisible;


			navigaVerso.Clickable = true;
			backIcon.Clickable = true;
			myGallery.Clickable = true;

			immagineGrande.SetImageResource (Resource.Drawable.placeholder);

		}
	}
}

