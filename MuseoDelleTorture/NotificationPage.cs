﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;
using System.Threading.Tasks;

using Android.App;
using Android.Content.PM;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Views;
using Android.Graphics;
using Square.Picasso;

namespace MuseoDelleTorture
{
	[Activity (Label = "NotificationPage",Theme = "@style/AppTheme",ScreenOrientation = ScreenOrientation.Portrait)]			
	public class NotificationPage : BaseActivity
	{

		public ImageView menuIcon;
		public ImageView backIcon;

		string image,descrizione;

		ImageView Image;
		TextView Descrizione;

		protected override void OnCreate (Bundle savedInstanceState)
		{
			base.OnCreate (savedInstanceState);

			setActionBarIcon(Resource.Drawable.menu_icon,Resource.Drawable.back_icon);

			menuIcon = (ImageView) getToolbar().FindViewById(Resource.Id.home_icon);
			backIcon = (ImageView) getToolbar().FindViewById(Resource.Id.back_icon);
			menuIcon.Visibility = ViewStates.Invisible;

			//mDrawerLayout.SetDrawerShadow(R.drawable.drawer_shadow,GravityCompat.START);

			backIcon.Click += delegate {
				Finish();
			};
				
			descrizione = Intent.GetStringExtra ("descrizione");
			image = Intent.GetStringExtra ("image");

			Image = FindViewById<ImageView> (Resource.Id.Image);
			Descrizione = FindViewById<TextView> (Resource.Id.descrizione);

			Typeface tf = Typeface.CreateFromAsset(Assets,"fonts/Myriad_Pro.ttf");

			Descrizione.Typeface = tf;
			Descrizione.Text = descrizione;

			if (image == "null") {
				Image.Visibility = ViewStates.Gone;
			} else {
				//caricaImmagineAsync (image,Image);
                try
                {
                    Picasso.With(MainActivity.Instance.ApplicationContext)
                           .Load(image)
                           .Placeholder(Resource.Drawable.placeholder)
                           .Into(Image);
                }
                catch (Exception e)
                {
                    Picasso.With(MainActivity.Instance.ApplicationContext)
                           .Load(Resource.Drawable.placeholder)
                           .Placeholder(Resource.Drawable.placeholder)
                           .Into(Image);
                }
			}


			// Create your application here
		}

		protected override int getLayoutResource() {
			return Resource.Layout.NotificationLayout;
		}

		async void caricaImmagineAsync(String uri,ImageView immagine_view){
			WebClient webClient = new WebClient ();
			byte[] bytes = null;
			try{
				bytes = await webClient.DownloadDataTaskAsync(uri);
			}
			catch(TaskCanceledException){
				Console.WriteLine ("Task Canceled!**************************************");
				return;
			}
			catch(Exception e){
				Console.WriteLine (e.ToString());
				return;
			}

			//if (immagine_view.IsInLayout) {

			Bitmap immagine = await BitmapFactory.DecodeByteArrayAsync (bytes, 0, bytes.Length);

			immagine_view.SetImageBitmap (immagine);

			Console.WriteLine ("Immagine caricata!");
			/*} else {
				Console.WriteLine ("Immagine contatto non più esistente per cui non la carico!");
			}*/
		}

	}
}

