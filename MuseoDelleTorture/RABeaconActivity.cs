﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content.PM;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using ZXing.Mobile;
using Android.Media;
using Android.Graphics;
using Android;

namespace MuseoDelleTorture
{
    [Activity(Label = "RABeaconActivity", ScreenOrientation = ScreenOrientation.Landscape)]
    public class RABeaconActivity : Activity, MediaPlayer.IOnPreparedListener, ISurfaceHolderCallback
    {


        private const string SAMPLE_WORLD_URL = "samples/6_Video_1_SimpleVideo/index.html";

        private const string KEY = "18/vUE0vfSURFLoURPF0bTo1sfpJEOkgKzqEZnE0vloS18Nbcz04BPyk+eEOY1Y3wipBd+M3RWy6iZgSQcPFKf8fLQWeSlXqg9ylrt14UZ5QtUzEXMQoGzeKgaAZxZ6EVFnVx7vlyXbhq7357yJtGTJNeNXLNPVWhV/mQ9YOf+VTYWx0ZWRfX9cuWHznY8yPDh86njYNt7lZGbkOiakYQutoLx+Kq2ch/sEXpQCcpB3Gi4bjHrMTZN3JwkxnMk0/2RBzONffO2RChJfy5N98srq4MwKeAQAfowfG83tzme5/PpbL6Gdb77u/G0c3H6lXZ8HascL474TIIGHTMNMjjmg00g7tlJ54hsvZAVS04hrzUoPu+0KXpFdUiI9M+U4NZfQatv7MB1DqLGQNwPgOragYv5GnsK4VFilK4PxSD60Qqgpj76HfTGf+O1Q+mawEpKX7oXmRKUiLa+6Q6xlSbxfR9aOd+p0rSTZhC/0y9paPUfp9LflBnKbv+QkR9ZlYCGY4N+InPgabASBles0qYF4Y+uOGQal2AiQoK27rgCi07X9laeqZ/SOAMwPAF+dWmZa1M8fKJL9f6/5rcpoc98xp9lWOWyzGaBTOGKotoBqPzaYgItr0a63PyGsbtPJkvVuLIgs8JQyT4t6tlG1R3uaf7gGO6GJK6yGMxZZrrWo=";

        private const string TITLE = "";

        ImageView menuIcon, backIcon;

        string folder;

        MobileBarcodeScanner scanner;

        bool scanOpen = false;
        bool scanFineOpen = false;

        MediaPlayer mp;
        VideoView videoView;

        bool videoplaying = false;

        private readonly string[] Permissions =
        {
            Manifest.Permission.Camera,
        };

        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);

            ActionBar.Hide();

            //folder = "RA/Banco/index.html";
            folder = Intent.GetStringExtra("folder");

            SetContentView(Resource.Layout.RABeaconActivityLayout);

            backIcon = (ImageView)FindViewById<ImageView>(Resource.Id.imageView1);

            backIcon.Click += delegate
            {
                Console.WriteLine("BAKKKKK");

                if (videoplaying)
                {
                    mp.Stop();
                    mp.Dispose();
                }
                Finish();
            };


            videoView = FindViewById<VideoView>(Resource.Id.VideoView);
            videoView.Visibility = ViewStates.Gone;

            ISurfaceHolder holder = videoView.Holder;
            holder.SetType(SurfaceType.PushBuffers);
            holder.AddCallback(this);

            var split = folder.Split('/');

            var descriptor = Assets.OpenFd(split[0] + "/" + split[1] + "/" + "video.mp4");
            mp = new MediaPlayer();
            mp.SetDataSource(descriptor.FileDescriptor, descriptor.StartOffset, descriptor.Length);
            mp.Prepare();
            mp.Completion += (sender, e) =>
            {

                //Console.WriteLine("Finish");
                //RequestedOrientation = Android.Content.PM.ScreenOrientation.Portrait;
                //videoView.Visibility = ViewStates.Gone;
                videoplaying = false;
                if (videoplaying)
                {
                    mp.Stop();
                    mp.Dispose();
                }

                Finish();
            };
            mp.Prepared += (sender, e) =>
            {

                int videoWidth = ((MediaPlayer)sender).VideoWidth;
                int videoHeight = ((MediaPlayer)sender).VideoHeight;
                float videoProportion = (float)videoWidth / (float)videoHeight;
                int screenWidth = WindowManager.DefaultDisplay.Width;// getWindowManager().getDefaultDisplay().getWidth();
                int screenHeight = WindowManager.DefaultDisplay.Height;//getWindowManager().getDefaultDisplay().getHeight();
                float screenProportion = (float)screenWidth / (float)screenHeight;
                var lp = videoView.LayoutParameters;



                if (videoProportion > screenProportion)
                {
                    lp.Width = screenWidth;
                    lp.Height = (int)((float)screenWidth / videoProportion);
                }
                else
                {
                    lp.Width = (int)(videoProportion * (float)screenHeight);
                    lp.Height = screenHeight;
                }

                Console.WriteLine(videoWidth + "|" + videoHeight + "|" + videoProportion + "\n" +
                                  screenWidth + "|" + screenHeight + "|" + screenProportion + "\n" +
                                  lp.Width + "|" + lp.Height);

                videoView.LayoutParameters = lp;

            };


            if (Android.Support.V4.Content.ContextCompat.CheckSelfPermission(this, Manifest.Permission.Camera) != Permission.Granted)
            {
                Android.Support.V4.App.ActivityCompat.RequestPermissions(this, Permissions, 0);
            }
            else
            {
                try
                {
                    releaseCameraAndPreview();
                    setCameraDisplayOrientation(this, 1, Android.Hardware.Camera.Open());
                    DisplayCamera();
                }
                catch (Exception e)
                {
                    Finish();
                }
            }


        }


        private void releaseCameraAndPreview()
        {
            Android.Hardware.Camera.Open().Release();
        }

        public async void DisplayCamera()
        {
            var options = new ZXing.Mobile.MobileBarcodeScanningOptions();
            options.PossibleFormats = new List<ZXing.BarcodeFormat>() {
                ZXing.BarcodeFormat.QR_CODE
                };
            options.AutoRotate = false;
            options.TryHarder = true;

            scanOpen = true;

            scanner = new ZXing.Mobile.MobileBarcodeScanner();
            var result = await scanner.Scan(options);

            if (result != null)

                HandleScanResult(result, 1);
            else
            {
                scanOpen = false;

                if (videoplaying)
                {
                    mp.Stop();
                    mp.Dispose();
                }

                Finish();
            }
        }


        void HandleScanResult(ZXing.Result result, int Indice)
        {

            scanOpen = false;

            string msg = "";

            if (result != null && !string.IsNullOrEmpty(result.Text))
            {
                string id = result.Text;
                RunOnUiThread(() =>
                {
                    if (id == "MuseoDelleTorture")
                    {
                        Console.WriteLine("TXT:" + id);
                        StartVideo();
                    }
                    else
                    {
                        DisplayCamera();
                    }
                });
            }
            else
            {

                if (videoplaying)
                {
                    mp.Stop();
                    mp.Dispose();
                }
                Finish();
                //RunOnUiThread(() => Toast.MakeText(this, msg, ToastLength.Short).Show());
            }

        }

        public static void setCameraDisplayOrientation(Activity activity, int cameraId, Android.Hardware.Camera camera)
        {

            if (camera == null)
            {
                Console.WriteLine("setCameraDisplayOrientation - camera null");
                return;
            }

            Android.Hardware.Camera.CameraInfo info = new Android.Hardware.Camera.CameraInfo();
            Android.Hardware.Camera.GetCameraInfo(cameraId, info);
            SurfaceOrientation rotation = activity.WindowManager.DefaultDisplay.Rotation;
            int degrees = 0;

            Console.WriteLine("Rotation:" + rotation);

            switch (rotation)
            {
                case SurfaceOrientation.Rotation0: degrees = 0; break;
                case SurfaceOrientation.Rotation90: degrees = 90; break;
                case SurfaceOrientation.Rotation180: degrees = 180; break;
                case SurfaceOrientation.Rotation270: degrees = 270; break;
            }

            int result;
            if (info.Facing == Android.Hardware.CameraFacing.Front)
            {
                Console.WriteLine("JJHJHHJ");
                result = (info.Orientation + degrees) % 360;
                result = (360 - result) % 360;  // compensate the mirror
            }
            else
            {  // back-facing
                result = (info.Orientation - degrees + 360) % 360;
            }

            Console.WriteLine(result + "|" + info.Orientation + "|" + degrees);
            camera.SetDisplayOrientation(90);
        }

        public override void OnBackPressed()
        {
            //Console.WriteLine("INFO"+ (SupportFragmentManager.BackStackEntryCount).ToString());
            if (videoplaying)
            {
                mp.Stop();
                mp.Dispose();
            }
            Finish();
        }

        protected override void OnResume()
        {
            base.OnResume();

            //if (architectView != null)
            //	architectView.OnResume ();
        }

        public override bool DispatchTouchEvent(MotionEvent ev)
        {
            //if (architectView != null)
            //return architectView.DispatchTouchEvent(ev);

            return base.DispatchTouchEvent(ev);

        }
        protected override void OnPause()
        {
            base.OnPause();

            //if (architectView != null)
            //	architectView.OnPause ();
        }

        protected override void OnStop()
        {
            base.OnStop();
        }

        protected override void OnDestroy()
        {
            base.OnDestroy();
            /*
			if (architectView != null)
			{
				architectView.OnDestroy ();

			}*/

        }

        public override void OnLowMemory()
        {
            base.OnLowMemory();

            //if (architectView != null)
            //	architectView.OnLowMemory ();
        }

        protected override void OnPostCreate(Bundle savedInstanceState)
        {
            base.OnPostCreate(savedInstanceState);
            /*
			if (architectView != null) {
				architectView.OnPostCreate ();

				try {
					architectView.Load (folder);
				} catch (Exception ex) {
					Console.WriteLine ("WIKITUDE_SAMPLE"+ ex.ToString ());
				}
			}
			*/
        }

        public void StartVideo()
        {


            //RequestedOrientation = Android.Content.PM.ScreenOrientation.Landscape;

            videoView.Visibility = ViewStates.Visible;
            videoplaying = true;
            mp.Start();

        }

        public void SurfaceCreated(ISurfaceHolder holder)
        {
            Console.WriteLine("SurfaceCreated");
            mp.SetDisplay(holder);
        }
        public void SurfaceDestroyed(ISurfaceHolder holder)
        {
            Console.WriteLine("SurfaceDestroyed");
        }
        public void SurfaceChanged(ISurfaceHolder holder, Android.Graphics.Format format, int w, int h)
        {
            Console.WriteLine("SurfaceChanged");
        }
        public void OnPrepared(MediaPlayer player)
        {
        }

    }
}

