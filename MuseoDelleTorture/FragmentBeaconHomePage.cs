﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Net;
using System.Threading.Tasks;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Graphics;
using Android.Util;
using Android.Views;
using Android.Widget;

using Fragment = Android.Support.V4.App.Fragment;
using FragmentManager = Android.Support.V4.App.FragmentManager;
using Square.Picasso;

namespace MuseoDelleTorture
{
	public class FragmentBeaconHomePage : Fragment
	{

		public Dictionary <int,BeaconInfo> BeaconDictionary = new Dictionary <int,BeaconInfo>();


		public override void OnCreate (Bundle savedInstanceState)
		{
			base.OnCreate (savedInstanceState);

			// Create your fragment here
		}

		public static FragmentBeaconHomePage Self {get;private set;}

		public override View OnCreateView (LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
		{
			// Use this to return your custom view for this Fragment
			View view = inflater.Inflate(Resource.Layout.FragmentBeaconHomeLayout, container, false);

			FragmentBeaconHomePage.Self = this;

			LinearLayout Content = view.FindViewById<LinearLayout>(Resource.Id.Content);
			TextView label = view.FindViewById<TextView> (Resource.Id.textView2);
			Typeface tf = Typeface.CreateFromAsset(Activity.Assets,"fonts/daunpenh.ttf"); 
			label.Typeface = tf;

			LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams (2000,LinearLayout.LayoutParams.WrapContent);
			//layoutParams.SetMargins (ConvertDpToPixel(40), ConvertDpToPixel(40), ConvertDpToPixel(20), 0);
			layoutParams.Weight=(float)0.5;

			LinearLayout.LayoutParams layoutParams2 = new LinearLayout.LayoutParams (2000,LinearLayout.LayoutParams.WrapContent);
			//layoutParams.SetMargins (ConvertDpToPixel(40), ConvertDpToPixel(40), ConvertDpToPixel(20), 0);
			layoutParams2.Weight=(float)0.5;

			LinearLayout.LayoutParams layoutParams3 = new LinearLayout.LayoutParams (LinearLayout.LayoutParams.MatchParent,LinearLayout.LayoutParams.WrapContent);

			LinearLayout.LayoutParams imgLayout = new LinearLayout.LayoutParams (LinearLayout.LayoutParams.MatchParent,LinearLayout.LayoutParams.WrapContent);
			imgLayout.SetMargins (ConvertDpToPixel(40), ConvertDpToPixel(10), ConvertDpToPixel(20), ConvertDpToPixel(10));

			LinearLayout.LayoutParams imgLayout2 = new LinearLayout.LayoutParams (LinearLayout.LayoutParams.MatchParent,LinearLayout.LayoutParams.WrapContent);
			imgLayout2.SetMargins (ConvertDpToPixel(20), ConvertDpToPixel(10), ConvertDpToPixel(40), ConvertDpToPixel(10));



            try
            {
                BeaconDictionary = MainActivity.Instance.StanzeDictionary[MainActivity.Instance.StanzaFind].BeaconDictionary;
            }catch(Exception)
            {
                FragmentManager.PopBackStack();
                return view;
            }
			

			TextView titolo = view.FindViewById<TextView> (Resource.Id.titolo);
			titolo.Typeface = tf;
			Console.WriteLine(1);
			if (BeaconDictionary.Count > 1){
				Console.WriteLine(1.1);
				titolo.Text = Context.Resources.GetString(Resource.String.BeaconH_Stanza) +" "+ BeaconDictionary [0].stanza;
			}else{
				Console.WriteLine(1.2);
				titolo.Text= Context.Resources.GetString(Resource.String.BeaconH_StanzaB)+" "+BeaconDictionary[0].stanza;
			}
			Console.WriteLine(2);
			int i = 0;
			int count = BeaconDictionary.Count;
			LinearLayout LayoutContainer=null;
			LinearLayout ImageContainer=null;
			LinearLayout ImageContainer2=null;
			if (count % 2 == 1)
				count++;
			for (i = 0; i < count; i++) {
				Console.WriteLine("for"+i);
				if (i % 2 == 0) {
					LayoutContainer = new LinearLayout (Activity.ApplicationContext);
					LayoutContainer.Orientation = Orientation.Horizontal;
					LayoutContainer.LayoutParameters = layoutParams3;
					ImageContainer = new LinearLayout (Activity.ApplicationContext);
					ImageContainer.LayoutParameters = layoutParams;
					ImageContainer2 = new LinearLayout (Activity.ApplicationContext);
					ImageContainer2.LayoutParameters = layoutParams2;

					LayoutContainer.AddView (ImageContainer);
					LayoutContainer.AddView (ImageContainer2);

				}

				if (BeaconDictionary.ContainsKey (i)) {

					BeaconInfo bi = BeaconDictionary [i];
					int indice = i;
					ImageView im = new ImageView (Activity.ApplicationContext);
					im.SetAdjustViewBounds (true);

					if (bi.tipo == "immagine") {
						im.SetImageResource (Resource.Drawable.placeholder);
                        //im.SetImageResource (Resource.Drawable.lamostra_icon);
                        try
                        {
                            Picasso.With(Activity.ApplicationContext)
                                   .Load(bi.url_thumbnail)
                                   .Placeholder(Resource.Drawable.placeholder)
                                   .Into(im);
                        }
                        catch(Exception e)
                        {
                            Picasso.With(Activity.ApplicationContext)
                                   .Load(Resource.Drawable.placeholder)
                                   .Placeholder(Resource.Drawable.placeholder)
                                   .Into(im); 
                        }

						im.Click += delegate {
							Console.WriteLine(i+"|"+indice);
							Bundle args =new Bundle();
							args.PutString("indice",indice.ToString());
							Fragment fragment = new FragmentBeaconImage();
							fragment.Arguments=args;
							FragmentManager frgManager = FragmentManager;
							frgManager.BeginTransaction()
								.Replace(Resource.Id.content_frame, fragment)
								.AddToBackStack(null)
								.Commit();
						};
					}
                    if (bi.tipo == "immagini")
                    {
                        im.SetImageResource(Resource.Drawable.placeholder);
                        //im.SetImageResource (Resource.Drawable.lamostra_icon);
                        try
                        {

                            Picasso.With(Activity.ApplicationContext)
                                   .Load(bi.url_thumbnail)
                                   .Placeholder(Resource.Drawable.placeholder)
                                   .Into(im);
                        }
                        catch (Exception e)
                        {
                            Picasso.With(Activity.ApplicationContext)
                                   .Load(Resource.Drawable.placeholder)
                                   .Placeholder(Resource.Drawable.placeholder)
                                   .Into(im);
                        }
                        im.Click += delegate
                        {
                            Console.WriteLine(i + "|" + indice);
                            Bundle args = new Bundle();
                            args.PutString("indice", indice.ToString());
                            Fragment fragment = new FragmentBeaconImages();
                            fragment.Arguments = args;
                            FragmentManager frgManager = FragmentManager;
                            frgManager.BeginTransaction()
                                .Replace(Resource.Id.content_frame, fragment)
                                .AddToBackStack(null)
                                .Commit();
                        };
                    }
                    if (bi.tipo == "realta")
                    {
                        im.SetImageResource(Resource.Drawable.placeholder);
                        //im.SetImageResource (Resource.Drawable.lamostra_icon);
                        try
                        {
                            Picasso.With(Activity.ApplicationContext)
                                   .Load(bi.url_thumbnail)
                                   .Placeholder(Resource.Drawable.placeholder)
                                   .Into(im);
                        }
                        catch (Exception e)
                        {
                            Picasso.With(Activity.ApplicationContext)
                                   .Load(Resource.Drawable.placeholder)
                                   .Placeholder(Resource.Drawable.placeholder)
                                   .Into(im);
                        }

                        im.Click += delegate
                        {
                            Console.WriteLine(i + "|" + indice);
                            Bundle args = new Bundle();
                            args.PutString("indice", indice.ToString());
                            Fragment fragment = new FragmentBeaconRealta();
                            fragment.Arguments = args;
                            FragmentManager frgManager = FragmentManager;
                            frgManager.BeginTransaction()
                                .Replace(Resource.Id.content_frame, fragment)
                                .AddToBackStack(null)
                                .Commit();
                            /*var intent = new Intent (Activity.ApplicationContext, typeof(RABeaconActivity));
							intent.PutExtra ("folder", bi.folder3D);
							StartActivity (intent);*/
                        };
                    }
					if (bi.tipo == "video") {
						im.SetImageResource (Resource.Drawable.placeholder);
                        //im.SetImageResource (Resource.Drawable.lamostra_icon);

                        try
                        {
                            Picasso.With(Activity.ApplicationContext)
                                   .Load(bi.url_thumbnail)
                                   .Placeholder(Resource.Drawable.placeholder)
                                   .Into(im);
                        }
                        catch (Exception e)
                        {
                            Picasso.With(Activity.ApplicationContext)
                                   .Load(Resource.Drawable.placeholder)
                                   .Placeholder(Resource.Drawable.placeholder)
                                   .Into(im);
                        }

						im.Click += delegate {
							Console.WriteLine(i+"|"+indice);
							Bundle args =new Bundle();
							args.PutString("indice",indice.ToString());
							Fragment fragment = new FragmentBeaconVideo();
							fragment.Arguments=args;
							FragmentManager frgManager = FragmentManager;
							frgManager.BeginTransaction()
								.Replace(Resource.Id.content_frame, fragment)
								.AddToBackStack(null)
								.Commit();
						};
					}
					if (i % 2 == 0) {
						im.LayoutParameters = imgLayout;
						ImageContainer.AddView (im);
					}else {
						im.LayoutParameters = imgLayout2;
						ImageContainer2.AddView (im);
					}
						
				} else {
					//ImageContainer2.LayoutParameters = layoutParams2;
				}

				if (i % 2 == 1) {
					Content.AddView(LayoutContainer);
				}
			}

			return view;
		}

		private int ConvertDpToPixel(float dp)
		{
			var pixel = (int)(dp*Resources.DisplayMetrics.Density);
			return pixel;
		}

		async void caricaImmagineAsync(String uri,ImageView immagine_view,BeaconInfo b){
			WebClient webClient = new WebClient ();
			byte[] bytes = null;
			try{
				bytes = await webClient.DownloadDataTaskAsync(uri);
			}
			catch(TaskCanceledException){
				Console.WriteLine ("Task Canceled!**************************************");
				return;
			}
			catch(Exception e){
				Console.WriteLine (e.ToString());
				return;
			}

			//if (immagine_view.IsInLayout) {

			Bitmap immagine = await BitmapFactory.DecodeByteArrayAsync (bytes, 0, bytes.Length);

			b.Thumbnail = immagine;

			immagine_view.SetImageBitmap (immagine);

			Console.WriteLine ("Immagine caricata!");
			/*} else {
				Console.WriteLine ("Immagine contatto non più esistente per cui non la carico!");
			}*/
		}

	}
}

